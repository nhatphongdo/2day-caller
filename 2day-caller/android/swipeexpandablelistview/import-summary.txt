ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Risky Project Location:
-----------------------
The tools *should* handle project locations in any directory. However,
due to bugs, placing projects in directories containing spaces in the
path, or characters like ", ' and &, have had issues. We're working to
eliminate these bugs, but to save yourself headaches you may want to
move your project to a location where this is not a problem.
C:\Users\nhatp\Documents\Visual Studio 2015\Projects\2CALL_source_Android\swipeexpandablelistview
                               -      -                                                          

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .idea\
* .idea\compiler.xml
* .idea\copyright\
* .idea\copyright\profiles_settings.xml
* .idea\encodings.xml
* .idea\misc.xml
* .idea\workspace.xml
* app\
* app\build.gradle
* app\lint.xml
* app\src\
* app\src\main\
* app\src\main\AndroidManifest.xml
* app\src\main\java\
* app\src\main\java\com\
* app\src\main\java\com\allen\
* app\src\main\java\com\allen\expandablelistview\
* app\src\main\java\com\allen\expandablelistview\BaseSwipeMenuExpandableListAdapter.java
* app\src\main\java\com\allen\expandablelistview\Swipable.java
* app\src\main\java\com\allen\expandablelistview\SwipeMenuExpandableCreator.java
* app\src\main\java\com\allen\expandablelistview\SwipeMenuExpandableListAdapter.java
* app\src\main\java\com\allen\expandablelistview\SwipeMenuExpandableListView.java
* app\src\main\java\com\allen\expandablelistview\SwipeMenuViewForExpandable.java
* app\src\main\java\com\baoyz\
* app\src\main\java\com\baoyz\swipemenulistview\
* app\src\main\java\com\baoyz\swipemenulistview\BaseSwipeListAdapter.java
* app\src\main\java\com\baoyz\swipemenulistview\ContentViewWrapper.java
* app\src\main\java\com\baoyz\swipemenulistview\SwipeMenu.java
* app\src\main\java\com\baoyz\swipemenulistview\SwipeMenuAdapter.java
* app\src\main\java\com\baoyz\swipemenulistview\SwipeMenuCreator.java
* app\src\main\java\com\baoyz\swipemenulistview\SwipeMenuItem.java
* app\src\main\java\com\baoyz\swipemenulistview\SwipeMenuLayout.java
* app\src\main\java\com\baoyz\swipemenulistview\SwipeMenuListView.java
* app\src\main\java\com\baoyz\swipemenulistview\SwipeMenuView.java
* app\src\main\res\
* app\src\main\res\drawable-hdpi\
* app\src\main\res\drawable-hdpi\ic_launcher.png
* app\src\main\res\drawable-mdpi\
* app\src\main\res\drawable-mdpi\ic_launcher.png
* app\src\main\res\drawable-xhdpi\
* app\src\main\res\drawable-xhdpi\ic_launcher.png
* app\src\main\res\values-v11\
* app\src\main\res\values-v11\styles.xml
* app\src\main\res\values-v14\
* app\src\main\res\values-v14\styles.xml
* app\src\main\res\values\
* app\src\main\res\values\strings.xml
* app\src\main\res\values\styles.xml
* build.gradle
* gradle\
* gradle\wrapper\
* gradle\wrapper\gradle-wrapper.jar
* gradle\wrapper\gradle-wrapper.properties
* gradlew
* gradlew.bat
* proguard-project.txt
* settings.gradle

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:18.0.0

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app\src\main\AndroidManifest.xml
* lint.xml => app\lint.xml
* res\ => app\src\main\res\
* src\ => app\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
