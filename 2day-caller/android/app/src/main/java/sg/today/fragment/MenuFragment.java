package sg.today.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

import java.util.ArrayList;

import sg.R;
import sg.today.model.Profile;
import sg.today.service.ContextHelper;


public class MenuFragment extends Fragment {

    private OnMenuClickedListener mListener;
    private GridLayout grid;
    private View callModule;
    private View voiceModule;
    private View videoModule;
    private View messagesModule;
    private View smsModule;
    private View meetingModule;
    private TextView greetingView;

    public MenuFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        greetingView = (TextView) view.findViewById(R.id.greeting_message);
        updateGreeting();

        grid = (GridLayout) view.findViewById(R.id.grid_layout);
        callModule = view.findViewById(R.id.call_module);
        voiceModule = view.findViewById(R.id.voice_module);
        videoModule = view.findViewById(R.id.video_module);
        messagesModule = view.findViewById(R.id.messages_module);
        smsModule = view.findViewById(R.id.sms_module);
        meetingModule = view.findViewById(R.id.meeting_module);

        Button callButton = (Button) view.findViewById(R.id.call_button);
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onMenuClicked(0);
                }
            }
        });
        Button voiceConferenceButton = (Button) view.findViewById(R.id.voice_conference_button);
        voiceConferenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onMenuClicked(1);
                }
            }
        });
        Button videoConferenceButton = (Button) view.findViewById(R.id.video_conference_button);
        videoConferenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onMenuClicked(2);
                }
            }
        });
        Button messagesButton = (Button) view.findViewById(R.id.messages_button);
        messagesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onMenuClicked(3);
                }
            }
        });
        Button meetingButton = (Button) view.findViewById(R.id.meeting_button);
        meetingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onMenuClicked(4);
                }
            }
        });

        reloadModules();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMenuClickedListener) {
            mListener = (OnMenuClickedListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnMenuClickedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        reloadModules();
        updateGreeting();
    }

    public void updateGreeting() {
        Profile profile = ContextHelper.getProfile(getContext());
        greetingView.setText("Hello " + (profile.firstName + " " + profile.lastName) + ".\nWhat would you like to do today?");
    }

    public interface OnMenuClickedListener {
        void onMenuClicked(int index);
    }

    private void reloadModules() {
        if (callModule == null || voiceModule == null || videoModule == null || messagesModule == null || smsModule == null || meetingModule == null) {
            return;
        }

        ArrayList<String> orderedModules = new ArrayList<>();
        ArrayList<String> modules = ContextHelper.getModules(getContext());

        if (modules.contains("Call")) {
            callModule.setVisibility(View.VISIBLE);
            orderedModules.add("Call");
        } else {
            callModule.setVisibility(View.GONE);
        }
        if (modules.contains("Voice")) {
            voiceModule.setVisibility(View.VISIBLE);
            orderedModules.add("Voice");
        } else {
            voiceModule.setVisibility(View.GONE);
        }
        if (modules.contains("Video")) {
            videoModule.setVisibility(View.VISIBLE);
            orderedModules.add("Video");
        } else {
            videoModule.setVisibility(View.GONE);
        }
        if (modules.contains("Messages")) {
            messagesModule.setVisibility(View.VISIBLE);
            orderedModules.add("Messages");
        } else {
            messagesModule.setVisibility(View.GONE);
        }
        if (modules.contains("Sms")) {
            smsModule.setVisibility(View.VISIBLE);
            orderedModules.add("Sms");
        } else {
            smsModule.setVisibility(View.GONE);
        }
        if (modules.contains("Meet")) {
            meetingModule.setVisibility(View.VISIBLE);
            orderedModules.add("Meet");
        } else {
            meetingModule.setVisibility(View.GONE);
        }

        if (!orderedModules.contains("Call")) {
            orderedModules.add("Call");
        }
        if (!orderedModules.contains("Voice")) {
            orderedModules.add("Voice");
        }
        if (!orderedModules.contains("Video")) {
            orderedModules.add("Video");
        }
        if (!orderedModules.contains("Messages")) {
            orderedModules.add("Messages");
        }
        if (!orderedModules.contains("Meet")) {
            orderedModules.add("Meet");
        }
        if (!orderedModules.contains("Sms")) {
            orderedModules.add("Sms");
        }

        grid.removeAllViews();
        for (int i = 0; i < orderedModules.size(); i++) {
            View v = null;
            if (orderedModules.get(i) == "Call") {
                v = callModule;
            }
            else if (orderedModules.get(i) == "Voice") {
                v = voiceModule;
            }
            else if (orderedModules.get(i) == "Video") {
                v = videoModule;
            }
            else if (orderedModules.get(i) == "Messages") {
                v = messagesModule;
            }
            else if (orderedModules.get(i) == "Meet") {
                v = meetingModule;
            }
            else if (orderedModules.get(i) == "Sms") {
                v = smsModule;
            }

            if (v != null) {
                GridLayout.Spec columnSpec = GridLayout.spec(i % 2, 1, GridLayout.FILL, 1);
                GridLayout.Spec rowSpec = GridLayout.spec(i / 2, 1, GridLayout.CENTER, 1);
                GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                grid.addView(v, layoutParams);
            }
        }
    }

}
