package sg.today.service;

/**
 * Created by Phong Do on 11/17/2016.
 */

public interface OnBackClickedListener {
    void onBackClicked();
}
