package sg.today.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.baoyz.swipemenulistview.BaseSwipeListAdapter;
import com.baoyz.swipemenulistview.ContentViewWrapper;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import sg.R;
import sg.today.model.Contact;
import sg.today.service.ContextHelper;

/**
 * Created by Phong Do on 11/23/2016.
 */

public class FavoriteListAdapter extends BaseSwipeListAdapter implements Filterable {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Contact> contactsList = new ArrayList<Contact>();
    private ArrayList<Contact> filterList = new ArrayList<>();

    public FavoriteListAdapter(Context mContext, ArrayList<Contact> contactsList) {
        this.mContext = mContext;
        this.contactsList = contactsList;
        this.filterList = contactsList;
        this.mInflater = LayoutInflater.from(mContext);
    }

    public void setContactsList(ArrayList<Contact> contacts) {
        this.contactsList = contacts;
        this.filterList = contacts;
    }

    @Override
    public ContentViewWrapper getViewAndReusable(int position, View view, ViewGroup parent) {
        boolean reUsable = true;
        final Contact contact = getItem(position);

        if (view == null) {
            view = mInflater.inflate(R.layout.view_item_contact, null);
            reUsable = false;
        }

        CircularImageView avatar = (CircularImageView) view.findViewById(R.id.avatar);
        TextView contactName = (TextView) view.findViewById(R.id.contact_name);
        TextView contactNumber = (TextView) view.findViewById(R.id.contact_number);
        ImageView sipAccount = (ImageView) view.findViewById(R.id.contact_2day);

        contactName.setText(contact.Name);
        contactNumber.setText(contact.Phone);
        if (TextUtils.isEmpty(contact.SipAccount)) {
            sipAccount.setVisibility(View.INVISIBLE);
        } else {
            if (contact.Phone == null || TextUtils.isEmpty(contact.Phone)) {
            }
            contactNumber.setText(contact.SipAccount);
            sipAccount.setVisibility(View.VISIBLE);
        }

        ContextHelper.loadAvatar(mContext, contact, contact.Phone, avatar);

        return new ContentViewWrapper(view, reUsable);
    }

    @Override
    public int getCount() {
        return filterList.size();
    }

    @Override
    public Contact getItem(int i) {
        return filterList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return filterList.get(i).Id;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    results.values = contactsList;
                    results.count = contactsList.size();
                } else {
                    ArrayList<Contact> list = new ArrayList<Contact>();
                    for (int i = 0; i < contactsList.size(); i++) {
                        if (contactsList.get(i).Name.toLowerCase().indexOf(constraint.toString().toLowerCase()) >= 0) {
                            list.add(contactsList.get(i));
                        }
                    }
                    results.values = list;
                    results.count = list.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterList = (ArrayList<Contact>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
