package sg.today.service;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;

import sg.today.model.Contact;
import sg.today.model.ContactNumber;

/**
 * Created by Phong Do on 11/20/2016.
 */

public class ContactService extends ContentObserver {

    private Handler mHandler;

    public ContactService(Handler handler) {
        super(handler);
        mHandler = handler;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Message message = mHandler.obtainMessage(0);
        message.sendToTarget();
    }

    public static ArrayList<Contact> readContacts(Context context) {
        ArrayList<Contact> contactsList = new ArrayList<>();

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            return contactsList;
        }

        ContentResolver cr = context.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                Contact contact = new Contact();
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                contact.ContactId = id;
                contact.Name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (contact.Name == null || contact.Name.length() == 0) {
                    contact.Name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY));
                }
                contact.Avatar = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
//                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
//                }

                Cursor pCur = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                while (pCur.moveToNext()) {
                    ContactNumber number = new ContactNumber();
                    number.Number = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    number.Number = number.Number.replace(" ", "");
                    number.Number = number.Number.replace("(", "");
                    number.Number = number.Number.replace(")", "");
                    number.Number = number.Number.replace("-", "");
                    number.Number = number.Number.replaceAll("\\D+", "");
                    boolean found = false;
                    for (int i = 0; i < contact.Numbers.size(); i++) {
                        if (contact.Numbers.get(i).Number.equalsIgnoreCase(number.Number)) {
                            found = true;
                        }
                    }
                    if (!found) {
                        contact.Numbers.add(number);
                    }
                }

                if (contact.Numbers.size() > 0) {
                    contact.Phone = contact.Numbers.get(0).Number;
                }
                pCur.close();

                Cursor emailCur = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                while (emailCur.moveToNext()) {
                    contact.Email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    break;
                }
                emailCur.close();

                contact.IsLiked = false;
                contact.IsDeleted = false;

                contactsList.add(contact);
            }
            cur.close();
        }

        return contactsList;
    }

}
