package sg.today.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import sg.today.model.CallLog;
import sg.today.model.Contact;
import sg.today.model.ContactNumber;

/**
 * Created by Phong Do on 11/20/2016.
 */

public class DatabaseAdapter {

    private static final String DATABASE_NAME = "DB_FONELINE";
    private static final String DATABASE_TABLE_CONTACTS = "contacts";
    private static final String DATABASE_TABLE_CONTACT_NUMBER = "contact_numbers";
    private static final String DATABASE_TABLE_CALL_LOGS = "call_logs";
    private static final String DATABASE_TABLE_SETTINGS = "settings";
    private static final int DATABASE_VERSION = 2;

    public static final String KEY_ID = "_id";
    public static final String KEY_CONTACT_NAME = "contact_name";
    public static final String KEY_CONTACT_ID = "contact_id";
    public static final String KEY_CONTACT_EMAIL = "contact_email";
    public static final String KEY_CONTACT_PHONE = "contact_phone";
    public static final String KEY_CONTACT_SIP_ACCOUNT = "contact_sip_account";
    public static final String KEY_CONTACT_IS_LIKED = "is_liked";
    public static final String KEY_CONTACT_IS_DELETED = "is_deleted";
    public static final String KEY_CONTACT_URI = "contact_uri";
    public static final String KEY_CALL_STATUS = "call_status";
    public static final String KEY_TIMER = "timer";

    private static final String CREATE_TABLE_SETTINGS = String.format(
            "create table %s ( name text unique not null, value text null);",
            DATABASE_TABLE_SETTINGS);

    private static final String CREATE_TABLE_CONTACTS = String.format(
            "create table %s ( " +
                    "%s integer primary key autoincrement, " +
                    "%s text null, " +
                    "%s text null, " +
                    "%s text null, " +
                    "%s text null, " +
                    "%s text null, " +
                    "%s text null, " +
                    "%s integer null, " +
                    "%s integer null );",
            DATABASE_TABLE_CONTACTS,
            KEY_ID,
            KEY_CONTACT_ID,
            KEY_CONTACT_NAME,
            KEY_CONTACT_PHONE,
            KEY_CONTACT_EMAIL,
            KEY_CONTACT_URI,
            KEY_CONTACT_SIP_ACCOUNT,
            KEY_CONTACT_IS_LIKED,
            KEY_CONTACT_IS_DELETED);

    private static final String CREATE_TABLE_CONTACT_NUMBERS = String.format(
            "create table %s ( " +
                    "%s integer primary key autoincrement, " +
                    "%s integer null, " +
                    "%s text null );",
            DATABASE_TABLE_CONTACT_NUMBER,
            KEY_ID,
            KEY_CONTACT_ID,
            KEY_CONTACT_PHONE);

    private static final String CREATE_TABLE_CALL_LOGS = String.format(
            "create table %s ( " +
                    "%s integer primary key autoincrement, " +
                    "%s integer null, " +
                    "%s text null, " +
                    "%s text null, " +
                    "%s text null, " +
                    "%s text null, " +
                    "%s text null );",
            DATABASE_TABLE_CALL_LOGS,
            KEY_ID,
            KEY_CONTACT_ID,
            KEY_CONTACT_PHONE,
            KEY_CONTACT_NAME,
            KEY_CONTACT_URI,
            KEY_TIMER,
            KEY_CALL_STATUS);

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context _ctx) {
            super(_ctx, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_TABLE_SETTINGS);
                db.execSQL(CREATE_TABLE_CONTACTS);
                db.execSQL(CREATE_TABLE_CONTACT_NUMBERS);
                db.execSQL(CREATE_TABLE_CALL_LOGS);
            } catch (SQLiteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_SETTINGS);
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_CALL_LOGS);
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_CONTACT_NUMBER);
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_CONTACTS);

            onCreate(db);
        }
    }

    private Context mContext;
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;

    public DatabaseAdapter(Context context) {
        mContext = context;
        dbHelper = new DatabaseHelper(context);
    }

    public DatabaseAdapter open() throws SQLException {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public long insertContact(Contact contact) {
        long result = 0;
        ContentValues initValues = new ContentValues();
        initValues.put(KEY_CONTACT_ID, contact.ContactId);
        initValues.put(KEY_CONTACT_NAME, contact.Name);
        initValues.put(KEY_CONTACT_PHONE, contact.Phone);
        initValues.put(KEY_CONTACT_EMAIL, contact.Email);
        initValues.put(KEY_CONTACT_IS_LIKED, contact.IsLiked);
        initValues.put(KEY_CONTACT_URI, contact.Avatar);
        initValues.put(KEY_CONTACT_IS_DELETED, contact.IsDeleted);
        initValues.put(KEY_CONTACT_SIP_ACCOUNT, contact.SipAccount);

        result = db.insert(DATABASE_TABLE_CONTACTS, null, initValues);

        if (contact.Numbers != null && contact.Numbers.size() > 0) {
            for (int i = 0; i < contact.Numbers.size(); i++) {
                insertContactNumber(result, contact.Numbers.get(i));
            }
        }

        return result;
    }

    public void deleteContact(long id) {
        ContentValues initValues = new ContentValues();
        initValues.put(KEY_CONTACT_IS_DELETED, 1);
        db.update(DATABASE_TABLE_CONTACTS, initValues, KEY_ID + "=\'" + id + "\'", null);
    }

    public void clearContacts() {
        db.delete(DATABASE_TABLE_CONTACTS, null, null);
    }

    public ArrayList<Contact> getAllContacts() {
        ArrayList<Contact> contactsList = new ArrayList<Contact>();
        Cursor c = db.query(true, DATABASE_TABLE_CONTACTS, null,
                null, null,
                KEY_ID, null, KEY_CONTACT_NAME + " ASC", null);
        if (c.moveToFirst()) {
            do {
                Contact contact = new Contact(c);
                contact.Numbers = getContactNumbers(contact.Id);
                contactsList.add(contact);
            } while (c.moveToNext());
        }

        c.close();
        return contactsList;
    }

    public ArrayList<Contact> getAllSipContacts() {
        ArrayList<Contact> contactsList = new ArrayList<Contact>();
        Cursor c = db.query(true, DATABASE_TABLE_CONTACTS, null,
                KEY_CONTACT_SIP_ACCOUNT + " is not null and " + KEY_CONTACT_SIP_ACCOUNT + "!=''", null,
                KEY_ID, null, KEY_CONTACT_NAME + " ASC", null);
        if (c.moveToFirst()) {
            do {
                Contact contact = new Contact(c);
                contact.Numbers = getContactNumbers(contact.Id);
                contactsList.add(contact);
            } while (c.moveToNext());
        }

        c.close();
        return contactsList;
    }

    public Contact getContact(long id) {
        Contact contact = null;
        Cursor c = db.query(true, DATABASE_TABLE_CONTACTS, null,
                KEY_ID + "=\'" + id + "\'", null,
                KEY_ID, null, KEY_CONTACT_NAME + " ASC", null);
        if (c.moveToFirst()) {
            contact = new Contact(c);
            contact.Numbers = getContactNumbers(contact.Id);
        }
        c.close();
        return contact;
    }

    public Contact getContactByContactId(String id) {
        Contact contact = null;
        Cursor c = db.query(true, DATABASE_TABLE_CONTACTS, null,
                KEY_CONTACT_ID + "=\'" + id + "\'", null,
                KEY_ID, null, KEY_CONTACT_NAME + " ASC", null);
        if (c.moveToFirst()) {
            contact = new Contact(c);
            contact.Numbers = getContactNumbers(contact.Id);
        }
        c.close();
        return contact;
    }

    public ArrayList<Contact> getFavoriteContacts() {
        ArrayList<Contact> contactsList = new ArrayList<Contact>();
        Cursor c = db.query(true, DATABASE_TABLE_CONTACTS, null,
                KEY_CONTACT_IS_LIKED + "=\'1\'", null, KEY_ID, null,
                KEY_CONTACT_NAME + " ASC", null);
        if (c.moveToFirst()) {
            do {
                Contact contact = new Contact(c);
                contact.Numbers = getContactNumbers(contact.Id);
                contactsList.add(contact);
            } while (c.moveToNext());
        }

        c.close();
        return contactsList;
    }

    public int updateContact(long ID, Contact contact, boolean removeLiked) {
        ContentValues initValues = new ContentValues();
        initValues.put(KEY_CONTACT_ID, contact.ContactId);
        initValues.put(KEY_CONTACT_NAME, contact.Name);
        initValues.put(KEY_CONTACT_PHONE, contact.Phone);
        initValues.put(KEY_CONTACT_EMAIL, contact.Email);
        if (removeLiked) {
            initValues.put(KEY_CONTACT_IS_LIKED, contact.IsLiked);
        }
        initValues.put(KEY_CONTACT_URI, contact.Avatar);
        initValues.put(KEY_CONTACT_IS_DELETED, contact.IsDeleted);
        initValues.put(KEY_CONTACT_SIP_ACCOUNT, contact.SipAccount);

        if (contact.Numbers != null && contact.Numbers.size() > 0) {
            // Delete old ones
            ArrayList<ContactNumber> oldNumbers = getContactNumbers(ID);
            for (int i = 0; i < oldNumbers.size(); i++) {
                deleteContactNumber(oldNumbers.get(i).Id);
            }
            // Insert new one
            for (int i = 0; i < contact.Numbers.size(); i++) {
                insertContactNumber(ID, contact.Numbers.get(i));
            }
        }

        return db.update(DATABASE_TABLE_CONTACTS, initValues, KEY_ID + "=\'" + ID + "\'", null);
    }

    public void updateContactAvatar(int ID, String image) {
        ContentValues initValues = new ContentValues();
        initValues.put(KEY_CONTACT_URI, image);
        db.update(DATABASE_TABLE_CONTACTS, initValues, KEY_ID + "=\'" + ID + "\'", null);
    }

    public void updateLikedContact(long id, boolean flag) {
        ContentValues args = new ContentValues();
        args.put(KEY_CONTACT_IS_LIKED, (flag ? "1" : "0"));
        db.update(DATABASE_TABLE_CONTACTS, args, KEY_ID + "=\'" + id + "\'", null);
    }

    public ArrayList<Contact> searchContacts(String term) {
        ArrayList<Contact> contactsList = new ArrayList<Contact>();
        Cursor c = db.query(true, DATABASE_TABLE_CONTACTS, null,
                KEY_CONTACT_NAME + "=\'" + term + "\' or " + KEY_CONTACT_PHONE + "=\'" + term + "\' or " + KEY_CONTACT_SIP_ACCOUNT + "=\'" + term + "\'", null,
                KEY_ID, null, null, null);
        if (c.moveToFirst()) {
            do {
                Contact contact = new Contact(c);
                contact.Numbers = getContactNumbers(contact.Id);
                contactsList.add(contact);
            } while (c.moveToNext());
        }

        c.close();
        return contactsList;
    }

    public long insertContactNumber(long contactId, ContactNumber number) {
        long result = 0;
        ContentValues initValues = new ContentValues();
        initValues.put(KEY_CONTACT_ID, contactId);
        initValues.put(KEY_CONTACT_PHONE, number.Number);

        result = db.insert(DATABASE_TABLE_CONTACT_NUMBER, null, initValues);

        return result;
    }

    public void deleteContactNumber(long id) {
        db.delete(DATABASE_TABLE_CONTACT_NUMBER, KEY_ID + "=\'" + id + "\'", null);
    }

    public ArrayList<ContactNumber> getContactNumbers(long contactId) {
        ArrayList<ContactNumber> contactNumbers = new ArrayList<ContactNumber>();
        Cursor c = db.query(true, DATABASE_TABLE_CONTACT_NUMBER, null,
                KEY_CONTACT_ID + "=\'" + contactId + "\'", null, KEY_ID, null,
                KEY_ID + " ASC", null);
        if (c.moveToFirst()) {
            do {
                ContactNumber number = new ContactNumber(c);
                contactNumbers.add(number);
            } while (c.moveToNext());
        }

        c.close();
        return contactNumbers;
    }

    public long insertCallLog(CallLog log) {
        long result = 0;
        ContentValues initValues = new ContentValues();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        initValues.put(KEY_CONTACT_ID, log.ContactId);
        initValues.put(KEY_CONTACT_NAME, log.Name);
        initValues.put(KEY_CONTACT_PHONE, log.Phone);
        initValues.put(KEY_CONTACT_URI, log.Avatar);
        initValues.put(KEY_TIMER, formatter.format(log.Timer));
        initValues.put(KEY_CALL_STATUS, log.Status);

        result = db.insert(DATABASE_TABLE_CALL_LOGS, null, initValues);

        return result;
    }

    public ArrayList<CallLog> getAllCallLogs() {
        ArrayList<CallLog> logList = new ArrayList<CallLog>();
        Cursor c = db.query(true, DATABASE_TABLE_CALL_LOGS, null,
                null, null,
                KEY_ID, null, KEY_TIMER + " DESC", null);
        if (c.moveToFirst()) {
            do {
                CallLog log = new CallLog(c);
                logList.add(log);
            } while (c.moveToNext());
        }

        c.close();
        return logList;
    }

    public ArrayList<CallLog> getCallLogsByStatus(String status) {
        ArrayList<CallLog> logList = new ArrayList<CallLog>();
        Cursor c = db.query(true, DATABASE_TABLE_CALL_LOGS, null,
                KEY_CALL_STATUS + "=\'" + status + "\'", null,
                KEY_ID, null, KEY_TIMER + " DESC", null);
        if (c.moveToFirst()) {
            do {
                CallLog log = new CallLog(c);
                logList.add(log);
            } while (c.moveToNext());
        }

        c.close();
        return logList;
    }

    public void deleteCallLog(long id) {
        db.delete(DATABASE_TABLE_CALL_LOGS, KEY_ID + "=\'" + id + "\'", null);
    }

    public void clearCallLogs(String status) {
        if (TextUtils.isEmpty(status)) {
            db.delete(DATABASE_TABLE_CALL_LOGS, null, null);
        } else {
            db.delete(DATABASE_TABLE_CALL_LOGS, KEY_CALL_STATUS + "=\'" + status + "\'", null);
        }
    }

}
