package sg.today.service;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.telecom.TelecomManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.InputMethodManager;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.clover_studio.spikachatmodule.ChatActivity;
import com.clover_studio.spikachatmodule.models.Config;
import com.clover_studio.spikachatmodule.models.User;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.helper.StringUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import sg.R;
import sg.today.activity.IncomingActivity;
import sg.today.model.Contact;
import sg.today.model.Profile;
import sg.today.model.SipAccount;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Phong Do on 11/16/2016.
 */

public class ContextHelper {

    private static MediaPlayer mediaPlayer;
    private static Vibrator mVibrator;

    public static final String SHARE_PREFERENCES_NAME = "TWO_CALL_SHARED_PREFERENCES";
    public static final String REMEMBER_ME = "REMEMBER_ME";
    public static final String SAVED_USERNAME = "SAVED_USERNAME";
    public static final String SAVED_PASSWORD = "SAVED_PASSWORD";
    public static final String ACCOUNT_USERNAME = "ACCOUNT_USERNAME";
    public static final String ACCOUNT_EMAIL = "ACCOUNT_EMAIL";
    public static final String ACCOUNT_COUNTRY_CODE = "ACCOUNT_COUNTRY_CODE";
    public static final String ACCOUNT_CITY_CODE = "ACCOUNT_CITY_CODE";
    public static final String ACCOUNT_MOBILE_NUMBER = "ACCOUNT_MOBILE_NUMBER";
    public static final String ACCOUNT_FIRST_NAME = "ACCOUNT_FIRST_NAME";
    public static final String ACCOUNT_LAST_NAME = "ACCOUNT_LAST_NAME";
    public static final String ACCOUNT_MEET_PLAN = "ACCOUNT_MEET_PLAN";
    public static final String ACCOUNT_MEET_VALID_FROM = "ACCOUNT_MEET_VALID_FROM";
    public static final String ACCOUNT_MEET_VALID_TO = "ACCOUNT_MEET_VALID_TO";
    public static final String ACCOUNT_MOXTRA = "ACCOUNT_MOXTRA";
    public static final String ACCOUNT_TELEGRAM = "ACCOUNT_TELEGRAM";
    public static final String ACCOUNT_SIP_DISPLAY_NAME = "ACCOUNT_SIP_DISPLAY_NAME";
    public static final String ACCOUNT_SIP_RINGTONE = "ACCOUNT_SIP_RINGTONE";
    public static final String ACCOUNT_SIP_DID = "ACCOUNT_SIP_DID";
    public static final String ACCOUNT_SIP_CONNECT_TYPE = "ACCOUNT_SIP_CONNECT_TYPE";
    public static final String ACCOUNT_PROFILE_IMAGE = "ACCOUNT_PROFILE_IMAGE";
    public static final String ACCOUNT_SIP_ACCOUNTS = "ACCOUNT_SIP_ACCOUNTS";
    public static final String ACCOUNT_GCM_TOKEN = "ACCOUNT_GCM_TOKEN";
    public static final String ACCOUNT_SIP_BALANCE = "ACCOUNT_SIP_BALANCE";
    public static final String LAST_CALL_NUMBER = "LAST_CALL_NUMBER";
    public static final String COUNTRY_NAME = "COUNTRY_NAME";
    public static final String COUNTRY_CODE = "COUNTRY_CODE";
    public static final String USE_LOCAL_MODE = "USE_LOCAL_MODE";
    public static final String WORKING_MODULES = "WORKING_MODULES";

    public static void hideKeyboard(Activity context) {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showAlert(Context context, String title, String message) {
        if (context instanceof Activity) {
            if (((Activity) context).isFinishing()) {
                return;
            }
        }
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.action_close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public static void showInfo(Context context, String title, String message) {
        if (context instanceof Activity) {
            if (((Activity) context).isFinishing()) {
                return;
            }
        }
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(R.string.action_close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public static void showGlobalDialog(Context context, String title, String message,
                                        String strPositive, DialogInterface.OnClickListener positiveListener,
                                        String strNegative, DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        if (positiveListener != null) {
            builder.setPositiveButton(strPositive, positiveListener);
        }

        if (negativeListener != null) {
            builder.setNegativeButton(strNegative, negativeListener);
        }

        AlertDialog ad = builder.create();
        //ad.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        ad.setCanceledOnTouchOutside(false);
        ad.show();
    }

    public static void showNotification(Context context, Intent intent, String message) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        PendingIntent pi = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent answerIntent = new Intent(context, IncomingActivity.class);
        answerIntent.setAction("Answer");
        answerIntent.putExtra(IncomingActivity.CALLER_DISPLAY_NAME, intent.getStringExtra(IncomingActivity.CALLER_DISPLAY_NAME));
        answerIntent.putExtra(IncomingActivity.CALLER_NUMBER, intent.getStringExtra(IncomingActivity.CALLER_NUMBER));
        answerIntent.putExtra(IncomingActivity.CALL_IS_VIDEO, intent.getBooleanExtra(IncomingActivity.CALL_IS_VIDEO, false));
        answerIntent.putExtra(IncomingActivity.CALL_SESSION_ID, intent.getLongExtra(IncomingActivity.CALL_SESSION_ID, -1));
        PendingIntent answerPi = PendingIntent.getActivity(context, 0, answerIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent answerVideoIntent = new Intent(context, IncomingActivity.class);
        answerVideoIntent.setAction("Answer Video");
        answerVideoIntent.putExtra(IncomingActivity.CALLER_DISPLAY_NAME, intent.getStringExtra(IncomingActivity.CALLER_DISPLAY_NAME));
        answerVideoIntent.putExtra(IncomingActivity.CALLER_NUMBER, intent.getStringExtra(IncomingActivity.CALLER_NUMBER));
        answerVideoIntent.putExtra(IncomingActivity.CALL_IS_VIDEO, intent.getBooleanExtra(IncomingActivity.CALL_IS_VIDEO, false));
        answerVideoIntent.putExtra(IncomingActivity.CALL_SESSION_ID, intent.getLongExtra(IncomingActivity.CALL_SESSION_ID, -1));
        PendingIntent answerVideoPi = PendingIntent.getActivity(context, 0, answerVideoIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent rejectIntent = new Intent(context, IncomingActivity.class);
        rejectIntent.setAction("Reject");
        rejectIntent.putExtra(IncomingActivity.CALLER_DISPLAY_NAME, intent.getStringExtra(IncomingActivity.CALLER_DISPLAY_NAME));
        rejectIntent.putExtra(IncomingActivity.CALLER_NUMBER, intent.getStringExtra(IncomingActivity.CALLER_NUMBER));
        rejectIntent.putExtra(IncomingActivity.CALL_IS_VIDEO, intent.getBooleanExtra(IncomingActivity.CALL_IS_VIDEO, false));
        rejectIntent.putExtra(IncomingActivity.CALL_SESSION_ID, intent.getLongExtra(IncomingActivity.CALL_SESSION_ID, -1));
        PendingIntent rejectPi = PendingIntent.getActivity(context, 0, rejectIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.logo)
                .setSound(defaultSoundUri)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(false)
                .setFullScreenIntent(pi, true)
                .addAction(R.drawable.small_action_call, "Answer", answerPi)
                .addAction(R.drawable.small_action_video, "Answer Video", answerVideoPi)
                .addAction(R.drawable.end_call_icon, "Reject", rejectPi);
        mBuilder.setContentIntent(pi);
        // Sets an ID for the notification
        int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    public static void showChatNotification(Context context, Intent intent, String message) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        PendingIntent pi = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.logo)
                .setSound(defaultSoundUri)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(false);
        mBuilder.setContentIntent(pi);
        // Sets an ID for the notification
        int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    public static void showTipMessage(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static void bringToFront(Context context, Intent intent) {
        try {
            // FLAG_ACTIVITY_SINGLE_TOP will solve not sending Extras problem
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pi = PendingIntent.getActivity(context, 0, intent, 0);
            pi.send(context, 0, null);
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }
    }

    public static void startActivityAsFront(Context context, Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static void playRingtone(Context context, boolean speaker) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        }

        if (am.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
            if (!mediaPlayer.isPlaying()) {
                try {
                    mediaPlayer = new MediaPlayer();
                    AssetFileDescriptor afd = context.getResources().openRawResourceFd(R.raw.romans2);
                    mediaPlayer.setAudioSessionId(0);
                    mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    afd.close();
                    if (speaker) {
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
                    } else {
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
                    }
                    mediaPlayer.setLooping(true);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("RINGTONE", "RINGING...stop");
            }
        }
        boolean vibrate = false;
        if (am.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE) {
            vibrate = true;
        } else if (1 == Settings.System.getInt(context.getContentResolver(), "vibrate_when_ringing", 0)) //vibrate on
        {
            vibrate = true;
        }
        if (vibrate) {
            mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            mVibrator.vibrate(5000);
        }
    }

    public static void stopRingtone() {
        if (mediaPlayer == null) {
            return;
        }
        mediaPlayer.stop();
        if (mVibrator != null) {
            mVibrator.cancel();
        }
    }

    public static Drawable resizeDrawable(Drawable input, int width, int height) {
        input.setBounds(0, 0, (int) (input.getIntrinsicWidth() * 0.5), (int) (input.getIntrinsicHeight() * 0.5));
        ScaleDrawable sd = new ScaleDrawable(input, 0, width, height);
        return sd.getDrawable();
    }

    public static void startChat(Context context, String roomId, String userId, String userName, String avatarUrl) {
        User user = new User();
        user.roomID = roomId;
        user.userID = userId;
        user.name = userName;
        user.avatarURL = avatarUrl;
        Config config = new Config();
        config.apiBaseUrl = "http://chat.2day.sg/spika/v1/";
        config.socketUrl = "http://chat.2day.sg/spika";

        ChatActivity.startChatActivityWithConfig(context, user, config);
    }

    public static boolean retrieveCountryCodes(Context context) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);

        String bestProvider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        Location lastLocation = locationManager.getLastKnownLocation(bestProvider);

        String countryName = getCountryFromLocation(context, lastLocation);
        if (countryName == null) {
            //return false;
            countryName = "SG";
        }

        String countryCode = getCountryZipCode(context, countryName);

        setCountryCodes(context, countryName, countryCode);
        return true;
    }

    private static String getCountryZipCode(Context context, String code) {
        if (code == null) {
            return "";
        }

        String countryID = "";
        String countryZipCode = "";

        countryID = code.toUpperCase();
        String[] rl = context.getResources().getStringArray(R.array.country_codes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(countryID.trim())) {
                countryZipCode = g[0];
                break;
            }
        }

        return countryZipCode;
    }

    private static String getCountryFromLocation(Context context, Location location) {
        String country = null;
        if (location == null) {
            return country;
        }

        Geocoder geoCoder = new Geocoder(context);
        try {
            List<Address> addresses = geoCoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0) {
                country = addresses.get(0).getCountryCode();
            }
        } catch (IOException e) {
        }

        return country;
    }

    public static void setRemember(Context context, boolean isRemember) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(REMEMBER_ME, isRemember);
        editor.commit();
    }

    public static boolean getRemember(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sp.getBoolean(REMEMBER_ME, false);
    }

    public static void setAccount(Context context, String username, String password) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SAVED_USERNAME, username);
        editor.putString(SAVED_PASSWORD, password);
        editor.commit();
    }

    public static String[] getAccount(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String[] result = new String[2];
        result[0] = sp.getString(SAVED_USERNAME, "");
        result[1] = sp.getString(SAVED_PASSWORD, "");
        return result;
    }

    public static void clearAccount(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(SAVED_USERNAME);
        editor.remove(SAVED_PASSWORD);
        editor.commit();
    }

    public static void setProfile(Context context, Profile profile) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        editor.putString(ACCOUNT_USERNAME, profile.username);
        editor.putString(ACCOUNT_EMAIL, profile.email);
        editor.putString(ACCOUNT_COUNTRY_CODE, profile.countryCode);
        editor.putString(ACCOUNT_CITY_CODE, profile.cityCode);
        editor.putString(ACCOUNT_MOBILE_NUMBER, profile.mobileNumber);
        editor.putString(ACCOUNT_FIRST_NAME, profile.firstName);
        editor.putString(ACCOUNT_LAST_NAME, profile.lastName);
        editor.putInt(ACCOUNT_MEET_PLAN, profile.meetPlan);
        editor.putString(ACCOUNT_MEET_VALID_FROM, profile.meetValidFrom == null ? "" : dateFormatter.format(profile.meetValidFrom));
        editor.putString(ACCOUNT_MEET_VALID_TO, profile.meetValidTo == null ? "" : dateFormatter.format(profile.meetValidTo));
        editor.putInt(ACCOUNT_MOXTRA, profile.moxtra);
        editor.putInt(ACCOUNT_TELEGRAM, profile.telegram);
        editor.putString(ACCOUNT_SIP_DISPLAY_NAME, profile.sipDisplayName);
        editor.putString(ACCOUNT_SIP_RINGTONE, profile.sipRingtone);
        editor.putString(ACCOUNT_SIP_DID, profile.sipDid);
        editor.putString(ACCOUNT_SIP_CONNECT_TYPE, profile.sipConnectType);
        editor.putString(ACCOUNT_PROFILE_IMAGE, profile.profileImage);
        JSONArray jsonArray = new JSONArray();
        if (profile.sipAccounts != null) {
            try {
                for (int i = 0; i < profile.sipAccounts.length; i++) {
                    jsonArray.put(i, profile.sipAccounts[i].toJSONObject());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        editor.putString(ACCOUNT_SIP_ACCOUNTS, profile.sipAccounts == null ? "" : jsonArray.toString());
        editor.commit();
    }

    public static Profile getProfile(Context context) {
        Profile profile = new Profile();
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        profile.username = sp.getString(ACCOUNT_USERNAME, "");
        profile.email = sp.getString(ACCOUNT_EMAIL, "");
        profile.countryCode = sp.getString(ACCOUNT_COUNTRY_CODE, "");
        profile.cityCode = sp.getString(ACCOUNT_CITY_CODE, "");
        profile.mobileNumber = sp.getString(ACCOUNT_MOBILE_NUMBER, "");
        profile.firstName = sp.getString(ACCOUNT_FIRST_NAME, "");
        profile.lastName = sp.getString(ACCOUNT_LAST_NAME, "");
        profile.meetPlan = sp.getInt(ACCOUNT_MEET_PLAN, 0);
        try {
            profile.meetValidFrom = dateFormatter.parse(sp.getString(ACCOUNT_MEET_VALID_FROM, ""));
        } catch (ParseException e) {
            profile.meetValidFrom = null;
            e.printStackTrace();
        }
        try {
            profile.meetValidTo = dateFormatter.parse(sp.getString(ACCOUNT_MEET_VALID_TO, ""));
        } catch (ParseException e) {
            profile.meetValidTo = null;
            e.printStackTrace();
        }
        profile.moxtra = sp.getInt(ACCOUNT_MOXTRA, 0);
        profile.telegram = sp.getInt(ACCOUNT_TELEGRAM, 0);
        profile.sipDisplayName = sp.getString(ACCOUNT_SIP_DISPLAY_NAME, "");
        profile.sipDid = sp.getString(ACCOUNT_SIP_DID, "");
        profile.sipRingtone = sp.getString(ACCOUNT_SIP_RINGTONE, "");
        profile.sipConnectType = sp.getString(ACCOUNT_SIP_CONNECT_TYPE, "");
        profile.profileImage = sp.getString(ACCOUNT_PROFILE_IMAGE, "");
        if (!TextUtils.isEmpty(sp.getString(ACCOUNT_SIP_ACCOUNTS, ""))) {
            try {
                JSONArray jsonArray = new JSONArray(sp.getString(ACCOUNT_SIP_ACCOUNTS, ""));
                profile.sipAccounts = new SipAccount[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    profile.sipAccounts[i] = new SipAccount(jsonArray.getJSONObject(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return profile;
    }

    public static void setLastCallNumber(Context context, String number) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(LAST_CALL_NUMBER, number);
        editor.commit();
    }

    public static void setCountryCodes(Context context, String country, String code) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(COUNTRY_NAME, country);
        editor.putString(COUNTRY_CODE, code);
        editor.commit();
    }

    public static String[] getCountryCodes(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String[] result = new String[2];
        result[0] = sp.getString(COUNTRY_NAME, "");
        result[1] = sp.getString(COUNTRY_CODE, "");
        return result;
    }

    public static void setLocalMode(Context context, boolean local) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(USE_LOCAL_MODE, local);
        editor.commit();
    }

    public static boolean getLocalMode(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sp.getBoolean(USE_LOCAL_MODE, false);
    }

    public static void setGcmToken(Context context, String token) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(ACCOUNT_GCM_TOKEN, token);
        editor.commit();
    }

    public static String getGcmToken(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sp.getString(ACCOUNT_GCM_TOKEN, "");
    }

    public static void setSipBalance(Context context, float value) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putFloat(ACCOUNT_SIP_BALANCE, value);
        editor.commit();
    }

    public static float getSipBalance(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sp.getFloat(ACCOUNT_SIP_BALANCE, 0);
    }

    public static void startChat(final Activity context, ArrayList<Contact> contacts) {
        final String[] accounts = ContextHelper.getAccount(context);
        final Profile profile = ContextHelper.getProfile(context);
        final ArrayList<String> roomIdentifier = new ArrayList<>();
        roomIdentifier.add(accounts[0].toLowerCase());
        final ArrayList<String> accountIds = new ArrayList<>();
        for (int i = 0; i < contacts.size(); i++) {
            if (contacts.get(i).Email == null || contacts.get(i).Email.length() == 0) {
                continue;
            }
            accountIds.add(contacts.get(i).Email);
            roomIdentifier.add(contacts.get(i).Email.toLowerCase());
        }

        Collections.sort(roomIdentifier);

        final ProgressDialog progress = ProgressDialog.show(
                context,
                "Messages",
                context.getString(R.string.waiting), true, false);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final String roomId = ApiServices.md5(TextUtils.join(";", roomIdentifier)).toLowerCase();
                    ApiServices.getInstance(context).SendChatNotification(accounts[0], accounts[1], accountIds, roomId, "", new JsonObjectResultInterface() {
                        @Override
                        public void onResponse(JSONObject response) {
                            context.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.dismiss();
                                }
                            });
                            ContextHelper.startChat(context, roomId, accounts[0], profile.firstName + " " + profile.lastName, profile.profileImage);
                        }

                        @Override
                        public void onErrorResponse(String error) {
                            context.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.dismiss();
                                }
                            });
                            ContextHelper.showAlert(context, context.getString(R.string.chat), "Cannot start chat session right now.");
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progress.dismiss();
                        }
                    });
                }
            }
        }).start();
    }

    public static void loadSipBalance(final Context context, final TextView textView, final String format) {
        final String[] account = ContextHelper.getAccount(context);
        final Profile profile = ContextHelper.getProfile(context);
        try {
            ApiServices.getInstance(context).GetSipBalance(account[0], account[1], new JsonObjectResultInterface() {
                @Override
                public void onResponse(JSONObject response) {
                    final DecimalFormat formatter = new DecimalFormat(format);
                    if (response.optInt("result_code") == 1) {
                        ContextHelper.setSipBalance(context, (float) response.optDouble("balance", 0));
                        textView.setText(formatter.format(ContextHelper.getSipBalance(context)));
                    } else {
                        try {
                            ApiServices.getInstance(context).GetSipInfo(account[0], account[1], new JsonObjectResultInterface() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    JSONArray sips = response.optJSONArray("data");
                                    profile.sipAccounts = new SipAccount[sips.length()];
                                    for (int i = 0; i < sips.length(); i++) {
                                        profile.sipAccounts[i] = new SipAccount(sips.optJSONObject(i));
                                    }
                                    ContextHelper.setProfile(context, profile);
                                    ContextHelper.setSipBalance(context, profile.sipAccounts[0].sipBalance);
                                    textView.setText(formatter.format(ContextHelper.getSipBalance(context)));
                                }

                                @Override
                                public void onErrorResponse(String error) {
                                    textView.setText(formatter.format(ContextHelper.getSipBalance(context)));
                                }
                            });
                        } catch (JSONException e) {
                        }
                    }
                }

                @Override
                public void onErrorResponse(String error) {
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap convertToBitmap(Drawable drawable, int widthPixels, int heightPixels) {
        Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, widthPixels, heightPixels);
        drawable.draw(canvas);

        return mutableBitmap;
    }

    public static void loadAvatar(Context context, DatabaseAdapter database, String number, CircularImageView imageView) {
        ArrayList<Contact> contacts = database.searchContacts(number);
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(contacts.size() > 0 && !TextUtils.isEmpty(contacts.get(0).Name) ? contacts.get(0).Name : number);
        TextDrawable.IBuilder builder = TextDrawable.builder()
                .beginConfig()
                .bold()
                .toUpperCase()
                .endConfig()
                .round();
        String[] words = (contacts.size() > 0 && !TextUtils.isEmpty(contacts.get(0).Name) ? contacts.get(0).Name : number).split(" ");
        TextDrawable drawable = builder.build(words[words.length - 1].substring(0, 1), color);
        imageView.setImageBitmap(ContextHelper.convertToBitmap(drawable, ContextHelper.dpToPx(context, 150), ContextHelper.dpToPx(context, 150)));

        if (contacts.size() > 0 && !TextUtils.isEmpty(contacts.get(0).Avatar)) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(contacts.get(0).Avatar));
                imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void loadAvatar(Context context, Contact contact, String number, CircularImageView imageView) {
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(contact != null && !TextUtils.isEmpty(contact.Name) ? contact.Name : number);
        TextDrawable.IBuilder builder = TextDrawable.builder()
                .beginConfig()
                .bold()
                .toUpperCase()
                .endConfig()
                .round();
        String[] words = (contact != null && !TextUtils.isEmpty(contact.Name) ? contact.Name : number).split(" ");
        if (words.length == 0) {
            words = new String[]{"Unknown"};
        }
        TextDrawable drawable = builder.build(words[words.length - 1].substring(0, 1), color);
        imageView.setImageBitmap(ContextHelper.convertToBitmap(drawable, ContextHelper.dpToPx(context, 150), ContextHelper.dpToPx(context, 150)));

        if (contact != null && !TextUtils.isEmpty(contact.Avatar)) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(contact.Avatar));
                imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void setModules(Context context, ArrayList<String> modules) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        String value = StringUtil.join(modules, ";");
        editor.putString(WORKING_MODULES, value);
        editor.commit();
    }

    public static ArrayList<String> getModules(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String value = sp.getString(WORKING_MODULES, "Call;Voice;Video;Messages;Meet");
        ArrayList<String> result = new ArrayList<>();
        String[] values = value.split(";");
        for (int i = 0; i < values.length; i++) {
            result.add(values[i]);
        }
        return result;
    }

    public static void showPhoneAccountsSetting(Context context) {
        if (Build.MANUFACTURER.equalsIgnoreCase("Samsung")) {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.android.server.telecom", "com.android.server.telecom.settings.EnableAccountPreferenceActivity"));
            context.startActivity(intent);
        } else {
            context.startActivity(new Intent(TelecomManager.ACTION_CHANGE_PHONE_ACCOUNTS));
        }
    }

    public static boolean hasNavBar(Context context) {
        //Emulator
//        if (Build.FINGERPRINT.startsWith("generic"))
//            return true;
//
//        return true;

        boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        boolean hasHomeKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_HOME);

        if (!hasBackKey && !hasHomeKey && !hasMenuKey) {
            // 99% sure there's a navigation bar
            return true;
        } else {
            // no navigation bar, unless it is enabled in the settings
            return false;
        }
    }

    public static void checkSipAccounts(final Context context, final ArrayList<Contact> contacts, final DatabaseAdapter mDatabase, final ArrayList<String> mobiles, int startIndex, final JsonObjectResultInterface callback) {
        final int toIndex;
        if (startIndex + 99 < mobiles.size()) {
            toIndex = startIndex + 99;
        } else {
            toIndex = mobiles.size() - 1;
        }
        final List<String> checkList = mobiles.subList(startIndex, toIndex);

        String[] account = ContextHelper.getAccount(context);
        try {
            ApiServices.getInstance(context).CheckSipAccounts(account[0], account[1], checkList,
                    new JsonObjectResultInterface() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response.optInt("result_code") == 1) {
                                JSONArray data = response.optJSONArray("data");
                                for (int j = 0; j < contacts.size(); j++) {
                                    String phone = contacts.get(j).Phone;
                                    String email = contacts.get(j).Email;
                                    if ((phone == null || TextUtils.isEmpty(phone)) && (email == null || TextUtils.isEmpty(email))) {
                                        continue;
                                    }
                                    if (phone != null) {
                                        phone = phone.replace(" ", "").replace("+", "");
                                    }
                                    contacts.get(j).SipAccount = "";
                                    for (int i = 0; i < data.length(); i++) {
                                        if ((phone != null && phone.equals(data.optJSONObject(i).optString("id"))) ||
                                                (email != null && email.equals(data.optJSONObject(i).optString("id")))) {
                                            if (data.optJSONObject(i).optInt("exists", 0) == 1) {
                                                contacts.get(j).SipAccount = data.optJSONObject(i).optString("extension");
                                                contacts.get(j).Avatar = data.optJSONObject(i).optString("image");
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            // Save to DB
                            for (int i = 0; i < contacts.size(); i++) {
                                if ((contacts.get(i).Phone == null || TextUtils.isEmpty(contacts.get(i).Phone)) &&
                                        (contacts.get(i).SipAccount == null || TextUtils.isEmpty(contacts.get(i).SipAccount))) {
                                    continue;
                                }
                                Contact oldContact = mDatabase.getContactByContactId(contacts.get(i).ContactId);
                                if (oldContact == null) {
                                    mDatabase.insertContact(contacts.get(i));
                                } else {
                                    mDatabase.updateContact(oldContact.Id, contacts.get(i), false);
                                }
                            }

                            if (toIndex + 1 < mobiles.size()) {
                                checkSipAccounts(context, contacts, mDatabase, mobiles, toIndex + 1, callback);
                            } else if (callback != null) {
                                callback.onResponse(new JSONObject());
                            }
                        }

                        @Override
                        public void onErrorResponse(String error) {
                            // Save to DB
                            for (int i = 0; i < contacts.size(); i++) {
                                if ((contacts.get(i).Phone == null || TextUtils.isEmpty(contacts.get(i).Phone)) &&
                                        (contacts.get(i).SipAccount == null || TextUtils.isEmpty(contacts.get(i).SipAccount))) {
                                    continue;
                                }
                                Contact oldContact = mDatabase.getContactByContactId(contacts.get(i).ContactId);
                                if (oldContact == null) {
                                    mDatabase.insertContact(contacts.get(i));
                                } else {
                                    mDatabase.updateContact(oldContact.Id, contacts.get(i), false);
                                }
                            }

                            if (toIndex + 1 < mobiles.size()) {
                                checkSipAccounts(context, contacts, mDatabase, mobiles, toIndex + 1, callback);
                            } else if (callback != null) {
                                callback.onResponse(new JSONObject());
                            }
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
            if (callback != null) {
                callback.onErrorResponse(e.getMessage());
            }
        }
    }
}
