package sg.today.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import sg.R;
import sg.today.AndroidBug5497Workaround;
import sg.today.model.Profile;
import sg.today.service.ContextHelper;

public class MeetSelectorActivity extends BaseActivity {

    private TextView mDateTimeLabel;
    private TextView mCurrentRoomLabel;
    private EditText mMeetID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meet_selector);

        AndroidBug5497Workaround.assistActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.back_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        final Profile profile = ContextHelper.getProfile(getContext());
        String email = profile.email.replace('@', '_').replace('.', '_');
        if (profile.username != null && profile.username.length() > 0) {
            email = profile.username;
        }
        final String room = email;

        mDateTimeLabel = (TextView) findViewById(R.id.datetime);

        mCurrentRoomLabel = (TextView)findViewById(R.id.my_room_name);
        mCurrentRoomLabel.setText("My Meeting Room Name: " + room);

        mMeetID = (EditText) findViewById(R.id.meet_id);
        mMeetID.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    joinMeet();
                    return true;
                }
                return false;
            }
        });

        Button mStartMeet = (Button) findViewById(R.id.start_meet_button);
        mStartMeet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), MeetActivity.class);
                intent.putExtra("Room", room);
                getContext().startActivity(intent);
            }
        });

        Button mJoinMeet = (Button) findViewById(R.id.join_meet_button);
        mJoinMeet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                joinMeet();
            }
        });
    }

    public MeetSelectorActivity getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onResume() {
        super.onResume();

        timer = new Timer();
        timer.schedule(new CountTimerTask(), 0, 1000);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
    }

    private void joinMeet() {
        if (mMeetID.getText().length() == 0) {
            mMeetID.setError("Please enter Meeting Room Name");
            return;
        }

        Intent intent = new Intent(getContext(), MeetActivity.class);
        intent.putExtra("Room", mMeetID.getText().toString());
        getContext().startActivity(intent);
    }

    final Handler timerHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (mDateTimeLabel != null) {
                mDateTimeLabel.setText(dateFormat.format(new Date()));
            }
            return false;
        }
    });

    private class CountTimerTask extends TimerTask {
        @Override
        public void run() {
            timerHandler.sendEmptyMessage(0);
        }
    }

    private Timer timer;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy hh:mm:ss aa");
}
