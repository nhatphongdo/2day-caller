package sg.today.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Set;

import sg.R;
import sg.today.AndroidBug5497Workaround;
import sg.today.service.ContextHelper;

public class CustomizeLayoutActivity extends BaseActivity {

    private View callModule;
    private View voiceGroupModule;
    private View videoGroupModule;
    private View messagesModule;
    private View meetModule;
    private View callModuleInactive;
    private View voiceGroupModuleInactive;
    private View videoGroupModuleInactive;
    private View messagesModuleInactive;
    private View meetModuleInactive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customize_layout);

        AndroidBug5497Workaround.assistActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitleTextColor(ContextCompat.getColor(getContext(), R.color.toolbar_title));
        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.back_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        callModule = findViewById(R.id.call_module);
        callModuleInactive = findViewById(R.id.call_module_inactive);
        voiceGroupModule = findViewById(R.id.voice_group_module);
        voiceGroupModuleInactive = findViewById(R.id.voice_group_module_inactive);
        videoGroupModule = findViewById(R.id.video_group_module);
        videoGroupModuleInactive = findViewById(R.id.video_group_module_inactive);
        messagesModule = findViewById(R.id.messages_module);
        messagesModuleInactive = findViewById(R.id.messages_module_inactive);
        meetModule = findViewById(R.id.meet_module);
        meetModuleInactive = findViewById(R.id.meet_module_inactive);

        // Check setting
        loadSettings();

        Button callRemove = (Button) findViewById(R.id.call_remove);
        callRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeModule("Call");
            }
        });
        Button voiceRemove = (Button) findViewById(R.id.voice_group_remove);
        voiceRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeModule("Voice");
            }
        });
        Button videoRemove = (Button) findViewById(R.id.video_group_remove);
        videoRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeModule("Video");
            }
        });
        Button messagesRemove = (Button) findViewById(R.id.messages_remove);
        messagesRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeModule("Messages");
            }
        });
        Button meetRemove = (Button) findViewById(R.id.meet_remove);
        meetRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeModule("Meet");
            }
        });

        Button callAdd = (Button) findViewById(R.id.call_add);
        callAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addModule("Call");
            }
        });
        Button voiceAdd = (Button) findViewById(R.id.voice_group_add);
        voiceAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addModule("Voice");
            }
        });
        Button videoAdd = (Button) findViewById(R.id.video_group_add);
        videoAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addModule("Video");
            }
        });
        Button messagesAdd = (Button) findViewById(R.id.messages_add);
        messagesAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addModule("Messages");
            }
        });
        Button meetsAdd = (Button) findViewById(R.id.meet_add);
        meetsAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addModule("Meet");
            }
        });
    }

    private CustomizeLayoutActivity getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            ContextHelper.hideKeyboard(getContext());
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void loadSettings() {
        ArrayList<String> modules = ContextHelper.getModules(this);
        if (modules.contains("Call")) {
            callModule.setVisibility(View.VISIBLE);
            callModuleInactive.setVisibility(View.GONE);
        }
        else {
            callModule.setVisibility(View.GONE);
            callModuleInactive.setVisibility(View.VISIBLE);
        }
        if (modules.contains("Voice")) {
            voiceGroupModule.setVisibility(View.VISIBLE);
            voiceGroupModuleInactive.setVisibility(View.GONE);
        }
        else {
            voiceGroupModule.setVisibility(View.GONE);
            voiceGroupModuleInactive.setVisibility(View.VISIBLE);
        }
        if (modules.contains("Video")) {
            videoGroupModule.setVisibility(View.VISIBLE);
            videoGroupModuleInactive.setVisibility(View.GONE);
        }
        else {
            videoGroupModule.setVisibility(View.GONE);
            videoGroupModuleInactive.setVisibility(View.VISIBLE);
        }
        if (modules.contains("Messages")) {
            messagesModule.setVisibility(View.VISIBLE);
            messagesModuleInactive.setVisibility(View.GONE);
        }
        else {
            messagesModule.setVisibility(View.GONE);
            messagesModuleInactive.setVisibility(View.VISIBLE);
        }
        if (modules.contains("Meet")) {
            meetModule.setVisibility(View.VISIBLE);
            meetModuleInactive.setVisibility(View.GONE);
        }
        else {
            meetModule.setVisibility(View.GONE);
            meetModuleInactive.setVisibility(View.VISIBLE);
        }
    }

    private void removeModule(String module) {
        ArrayList<String> modules = ContextHelper.getModules(getContext());
        modules.remove(module);
        ContextHelper.setModules(getContext(), modules);
        loadSettings();
    }

    private void addModule(String module) {
        ArrayList<String> modules = ContextHelper.getModules(getContext());
        if (!modules.contains(module)) {
            modules.add(module);
        }
        ContextHelper.setModules(getContext(), modules);
        loadSettings();
    }
}
