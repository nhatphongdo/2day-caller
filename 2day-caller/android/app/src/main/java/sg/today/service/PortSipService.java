package sg.today.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class PortSipService extends Service {
    static PortSipService instance;

    public PortSipService() {
        super();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    public class MyBinder extends Binder {

        public PortSipService getService() {
            return PortSipService.this;
        }
    }

    private MyBinder myBinder = new MyBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("Portsip Service", "Create");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("Portsip Service", "Service Stopped.");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.e("Portsip Service", "Removed");
    }
}
