package sg.today;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.telecom.TelecomManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.Toast;

import com.facebook.infer.annotation.Assertions;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactInstanceManagerBuilder;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.common.LifecycleState;
import com.facebook.soloader.SoLoader;
import com.portsip.OnPortSIPEvent;
import com.portsip.PortSipEnumDefine;
import com.portsip.PortSipErrorcode;
import com.portsip.PortSipSdk;
import com.portsip.Renderer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import sg.BuildConfig;
import sg.R;
import sg.today.activity.CallActivity;
import sg.today.activity.CallingActivity;
import sg.today.activity.IncomingActivity;
import sg.today.activity.MainActivity;
import sg.today.activity.MeetActivity;
import sg.today.model.CallLog;
import sg.today.model.Contact;
import sg.today.model.Profile;
import sg.today.model.SipAccount;
import sg.today.service.ContextHelper;
import sg.today.service.DatabaseAdapter;
import sg.today.service.ForegroundCheckTask;
import sg.today.service.OnContactSelectionListener;
import sg.today.service.PortSipService;
import sg.today.util.Line;
import sg.today.util.Network;
import sg.today.util.Session;

/**
 * Created by Phong Do on 11/17/2016.
 */

public class MainApplication extends Application implements OnPortSIPEvent {

    private static MainApplication instance;

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                new com.corbt.keepawake.KCKeepAwakePackage(),
                new com.facebook.react.shell.MainReactPackage(),
                new com.oblador.vectoricons.VectorIconsPackage(),
                new com.ocetnik.timer.BackgroundTimerPackage(),
                new com.oney.WebRTCModule.WebRTCModulePackage(),
                new com.rnimmersive.RNImmersivePackage(),
                new sg.today.audiomode.AudioModePackage(),
                new sg.today.proximity.ProximityPackage(),
                new sg.today.CloseReactPackage()
            );
        }
    };

    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    private DatabaseAdapter mDatabase;

    //public TelecomManager telecomManager;
    //public ComponentName componentName;
    //public PhoneAccountHandle phoneAccountHandle;
    //public PhoneAccount phoneAccount;

    private Intent srvIntent = null;
    private PortSipService portSrv = null;
    private MyServiceConnection connection = null;
    private PortSipSdk sdk;
    boolean conference = false;
    private boolean _SIPConnecting = false;
    private boolean _SIPLogined = false;
    private boolean _SIPOnline = false;
    private MainActivity mainActivity;
    private CallActivity callActivity;
    private ArrayList<CallingActivity> callingActivities;
    private IncomingActivity incomingActivity;
    private MeetActivity meetActivity;
    private OnContactSelectionListener contactSelectionListener;
    private SurfaceView remoteSurfaceView = null;
    private SurfaceView localSurfaceView = null;
    private Network netmanager;
    private static final Line[] _CallSessions = new Line[Line.MAX_LINES];// record
    // all
    // audio-video
    // sessions
    private Line _CurrentlyLine = _CallSessions[Line.LINE_BASE];// active line
    static final String SIP_ADDRESS_PATTERN = "^(sip:)(\\+)?[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\\.-][a-z0-9]+)*)+\\.[a-z]{2,}(:[0-9]{2,5})?$";
    public static final String SESSION_CHANG = MainApplication.class.getCanonicalName() + "Session change";
    public static final String CONTACT_CHANG = MainApplication.class.getCanonicalName() + "Contact change";

    //public static final String licenseKey = "PORTSIP_TEST_LICENSE";
    public static final String licenseKey = "2Bx0yNUU2NTNDQzM0MjU1QjdEODIwNDFCMzM5NDdERjFCQkAzQzA3ODAzNTYwNTcxNjhDMzU4Mzc3QjA2NDhFNzk0RUAwRDc5MkFCOUVEMDQzNzEyMjkwNjY1QjA3MkNCMjY0Q0AyMzJEMDg3RTRDQTNFRTY2QTZBNzRBMjE1MTNGRkU5OA";

    private static int activityCounter;

    public void sendSessionChangeMessage(String message, String action) {
        Log.e("Session Changed", message);

        Intent broadIntent = new Intent(action);
        broadIntent.putExtra("description", message);
        sendBroadcast(broadIntent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        callingActivities = new ArrayList<>();

        mDatabase = new DatabaseAdapter(this);
        mDatabase.open();

        sdk = new PortSipSdk();
        srvIntent = new Intent(this, PortSipService.class);
        connection = new MyServiceConnection();
        netmanager = new Network(this, new Network.NetWorkChangeListner() {
            @Override
            public void handleNetworkChangeEvent(final boolean wifiConnect, final boolean mobileConnect) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (isOnline()) {
                            updateStatus("Lost connection");
                            for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i)// when ip network changed,we relogin ,and update call message
                            {
                                if (_CallSessions[i].getSessionState()) {
                                    sdk.hangUp(_CallSessions[i].getSessionId());
                                    _CallSessions[i].reset();
                                }
                            }
                            sdk.unRegisterServer();
                        } else {
                            if (_SIPLogined && (wifiConnect || mobileConnect)) {
                                registerSip();
                            }
                        }
                    }
                }).start();
            }
        });

        sdk.setOnPortSIPEvent(this);
        localSurfaceView = Renderer.CreateLocalRenderer(this);
        remoteSurfaceView = Renderer.CreateRenderer(this, true);

        bindService(srvIntent, connection, BIND_AUTO_CREATE);
        for (int i = 0; i < _CallSessions.length; i++) {
            _CallSessions[i] = new Line(i);
        }

        activityCounter = 0;
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityStarted(Activity activity) {
                Log.e("Activity started", activity.getLocalClassName());
                ++activityCounter;
            }

            @Override
            public void onActivityResumed(Activity activity) {
                Log.e("Activity resumed", activity.getLocalClassName());
            }

            @Override
            public void onActivityPaused(Activity activity) {
                Log.e("Activity paused", activity.getLocalClassName());
            }

            @Override
            public void onActivityStopped(Activity activity) {
                Log.e("Activity stopped", activity.getLocalClassName());
                --activityCounter;
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                Log.e("Activity destroyed", activity.getLocalClassName());
                if (activity instanceof MainActivity) {
                    // Main Activity is destroyed: app is stopped or back to login
                    String[] accounts = ContextHelper.getAccount(getApplicationContext());
                    if (!accounts[0].equalsIgnoreCase("")) {
                        // Still be online then app is stopping
                        onTerminate();
                    }
                }
            }
        });

        //telecomManager = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);
        //componentName = new ComponentName("today.sg", "sg.today.service.MyConnectionService");
        //phoneAccountHandle = new PhoneAccountHandle(componentName, "2Day");
        //PhoneAccount.Builder phoneAccountBuilder = new PhoneAccount.Builder(phoneAccountHandle, "2Day");
        //phoneAccountBuilder.setCapabilities(PhoneAccount.CAPABILITY_CALL_PROVIDER);
        //phoneAccount = phoneAccountBuilder.build();
        //telecomManager.registerPhoneAccount(phoneAccount);

        SoLoader.init(this, /* native exopackage */ false);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.e("App", "Terminate");
        if (connection != null) {
            unbindService(connection);
            connection = null;
        }
        quit();

        mDatabase.close();
    }

    public static MainApplication getInstance() {
        if (instance == null) {
            instance = new MainApplication();
        }

        return instance;
    }

    public static int getActivityCounter() {
        return activityCounter;
    }

    public SurfaceView getRemoteSurfaceView() {
        return remoteSurfaceView;
    }

    public SurfaceView getLocalSurfaceView() {
        return localSurfaceView;
    }

    public PortSipSdk getPortSIPSDK() {
        return sdk;
    }

    class MyServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PortSipService.MyBinder binder = (PortSipService.MyBinder) service;

            portSrv = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // TODO Auto-generated method stub
            portSrv = null;
        }
    }

    public Line[] getLines() {
        return _CallSessions;
    }

    public boolean isOnline() {
        return _SIPOnline;
    }

    public void setConferenceMode(boolean state) {
        conference = state;
    }

    public boolean isConference() {
        return conference;
    }

    private void setLoginState(boolean state) {
        _SIPLogined = state;
        if (state == false) {
            _SIPOnline = false;
        }
    }

    public void updateSessionVideo() {
        if (callingActivities.size() > 0) {
            for (int i = 0; i < callingActivities.size(); i++) {
                callingActivities.get(i).updateVideo();
            }
        }
    }

    private String getLocalIP(boolean ipv6) {
        return netmanager.getLocalIP(ipv6);
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void setCallActivity(CallActivity callActivity) {
        this.callActivity = callActivity;
    }

    public void setCallingActivity(CallingActivity callingActivity) {
        callingActivities.add(callingActivity);
    }

    public void setIncomingActivity(IncomingActivity incomingActivity) {
        this.incomingActivity = incomingActivity;
    }

    public OnContactSelectionListener getContactSelectionListener() {
        return contactSelectionListener;
    }

    public void setContactSelectionListener(OnContactSelectionListener listener) {
        contactSelectionListener = listener;
    }

    public void setMeetActivity(MeetActivity activity) {
        meetActivity = activity;
    }

    public MeetActivity getMeetActivity() {
        return meetActivity;
    }

    public Line findLineBySessionId(long sessionId) {
        for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i) {
            if (_CallSessions[i].getSessionId() == sessionId) {
                return _CallSessions[i];
            }
        }

        return null;
    }

    public Line findSessionByIndex(int index) {
        if (Line.LINE_BASE <= index && index < Line.MAX_LINES) {
            return _CallSessions[index];
        }

        return null;
    }

    @Nullable
    public static Line findIdleLine() {
        for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i)// get idle session
        {
            if (!_CallSessions[i].getSessionState() && !_CallSessions[i].getRecvCallState()) {
                return _CallSessions[i];
            }
        }

        return null;
    }

    public void setCurrentLine(Line line) {
        if (line == null) {
            _CurrentlyLine = _CallSessions[Line.LINE_BASE];
        } else {
            _CurrentlyLine = line;
        }

    }

    public Session getCurrentSession() {
        return _CurrentlyLine;
    }

    // register event listener
    @Override
    public void onRegisterSuccess(String reason, int code) {
        _SIPOnline = true;
        updateStatus("OK");
        Log.e("Register SIP", "Success");
    }

    @Override
    public void onRegisterFailure(String reason, int code) {
        _SIPOnline = false;
        updateStatus(reason);
        Log.e("Register SIP", "Failed: " + reason);
    }

    // call event listener
    @Override
    public void onInviteIncoming(long sessionId, final String callerDisplayName, final String caller,
                                 String calleeDisplayName, String callee,
                                 String audioCodecs, String videoCodecs, boolean existsAudio,
                                 boolean existsVideo) {

        Log.e("Incoming", "Codecs: " + videoCodecs + ", " + audioCodecs);

        setupAudio();

        Line tempSession = findIdleLine();

        if (tempSession == null) {
            // Save to log
            CallLog log = new CallLog();
            log.ContactId = 0;
            log.Name = callerDisplayName == null || callerDisplayName.equalsIgnoreCase("null") ? caller : callerDisplayName;
            log.Phone = caller;
            log.Timer = new Date();
            log.Status = "Missed";
            mDatabase.insertCallLog(log);

            sdk.rejectCall(sessionId, 480);
            return;
        } else {
            tempSession.setRecvCallState(true);
        }

        if (existsVideo) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }
        if (existsAudio) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }

        tempSession.setSessionId(sessionId);
        tempSession.setVideoState(existsVideo);
        String comingCallTips = "Call incoming: " + callerDisplayName + "<" + caller + ">";
        tempSession.setDescriptionString(comingCallTips);
        sendSessionChangeMessage(comingCallTips, SESSION_CHANG);
        setCurrentLine(tempSession);

        if (existsVideo) {
            updateSessionVideo();
        }

        Log.e("Incoming", String.valueOf(sessionId) + ", " + String.valueOf(existsVideo));
        Intent incomingIntent = new Intent(this, IncomingActivity.class);
        incomingIntent.putExtra(IncomingActivity.CALLER_DISPLAY_NAME, callerDisplayName == null || callerDisplayName.equalsIgnoreCase("null") ? caller : callerDisplayName);
        incomingIntent.putExtra(IncomingActivity.CALLER_NUMBER, caller);
        incomingIntent.putExtra(IncomingActivity.CALL_IS_VIDEO, existsVideo);
        incomingIntent.putExtra(IncomingActivity.CALL_SESSION_ID, sessionId);

        boolean foregroud = false;
        try {
            foregroud = new ForegroundCheckTask().execute(this).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (foregroud) {
            ContextHelper.startActivityAsFront(this, incomingIntent);
        } else {
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
//                try {
//                    Bundle info = new Bundle();
//                    info.putString("Caller", callerDisplayName == null || callerDisplayName.equalsIgnoreCase("null") ? caller : callerDisplayName);
//                    info.putString("CallerNumber", caller);
//                    info.putBoolean("IsVideo", existsVideo);
//                    info.putLong("SessionId", sessionId);
//                    telecomManager.addNewIncomingCall(phoneAccountHandle, info);
//                }
//                catch (Exception exc) {
//                }
//            }
//            ContextHelper.showNotification(this, incomingIntent, String.format(getString(R.string.incoming_notification),
//                    callerDisplayName == null || callerDisplayName.equalsIgnoreCase("null") ? caller : callerDisplayName, caller));
            ContextHelper.startActivityAsFront(this, incomingIntent);
        }

        //ContextHelper.showTipMessage(this, comingCallTips);
        // You should write your own code to play the wav file here for alert
        // the incoming call(incoming tone);
    }

    @Override
    public void onInviteTrying(long sessionId) {
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Call is trying...");
        sendSessionChangeMessage("Call is trying...", SESSION_CHANG);
    }

    @Override
    public void onInviteSessionProgress(long sessionId, String audioCodecs,
                                        String videoCodecs, boolean existsEarlyMedia, boolean existsAudio,
                                        boolean existsVideo) {
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        if (existsVideo) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }
        if (existsAudio) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }

        tempSession.setSessionState(true);

        tempSession.setDescriptionString("Call session progress.");
        sendSessionChangeMessage("Call session progress.", SESSION_CHANG);
        tempSession.setEarlyMeida(existsEarlyMedia);
    }

    @Override
    public void onInviteRinging(long sessionId, String statusText, int statusCode) {
        Log.e("SIP", String.format("Invite Ringing: %d, %s, %d", sessionId, statusText, statusCode));
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        if (!tempSession.hasEarlyMeida()) {
            // Hasn't the early media, you must play the local WAVE file for
            // ringing tone
            ContextHelper.playRingtone(this, false);
        }

        tempSession.setDescriptionString("Ringing...");
        sendSessionChangeMessage("Ringing...", SESSION_CHANG);
    }

    @Override
    public void onInviteAnswered(long sessionId, String callerDisplayName, String caller,
                                 String calleeDisplayName, String callee,
                                 String audioCodecs, String videoCodecs, boolean existsAudio,
                                 boolean existsVideo) {
        Log.e("SIP", String.format("Invite Answered: %d, %s, %s, %s, %s", sessionId, callerDisplayName, caller, calleeDisplayName, callee));

        ContextHelper.stopRingtone();

        setupAudio();

        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        Log.e("SIP Answered", String.valueOf(existsVideo));

        if (existsVideo) {
            sdk.sendVideo(tempSession.getSessionId(), true);
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }
        if (existsAudio) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }
        tempSession.setVideoState(existsVideo);
        tempSession.setSessionState(true);
        tempSession.setDescriptionString("call established");
        sendSessionChangeMessage("call established", SESSION_CHANG);

        if (isConference()) {
            sdk.joinToConference(tempSession.getSessionId());
            sdk.sendVideo(tempSession.getSessionId(), tempSession.getVideoState());
            tempSession.setHoldState(false);
        }

        // If this is the refer call then need set it to normal
        if (tempSession.isReferCall()) {
            tempSession.setReferCall(false, 0);
        }

        if (callingActivities.size() > 0) {
            callingActivities.get(callingActivities.size() - 1).startCallTimer(true);
        }
    }

    @Override
    public void onInviteFailure(long sessionId, String reason, int code) {
        Log.e("SIP", String.format("Invite Failure: %d, %s, %d", sessionId, reason, code));

        ContextHelper.stopRingtone();

        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("call failure" + reason);
        sendSessionChangeMessage("call failure" + reason, SESSION_CHANG);
        if (tempSession.isReferCall()) {
            // Take off the origin call from HOLD if the refer call is failure
            Line originSession = findLineBySessionId(tempSession.getOriginCallSessionId());
            if (originSession != null) {
                sdk.unHold(originSession.getSessionId());
                originSession.setHoldState(false);

                // Switch the currently line to origin call line
                setCurrentLine(originSession);

                tempSession.setDescriptionString("refer failure:" + reason + "resume orignal call");
                sendSessionChangeMessage("call failure" + reason, SESSION_CHANG);
            }
        }

        ContextHelper.showTipMessage(this, reason);

        if (callingActivities.size() > 0) {
            if (isConference()) {
                int inCalling = 0;
                for (int i = Line.LINE_BASE; i < Line.MAX_LINES; i++) {
                    if (_CallSessions[i].getSessionState()) {
                        ++inCalling;
                    }
                }

                if (inCalling == 1) {
                    callingActivities.get(callingActivities.size() - 1).stopCallTimer();
                    callingActivities.get(callingActivities.size() - 1).finish();
                    callingActivities.remove(callingActivities.get(callingActivities.size() - 1));
                    endSessionCall(tempSession);
                } else {
                    if (tempSession.getSessionState()) {
                        sdk.hangUp(tempSession.getSessionId());
                    }
                }
            } else {
                for (int i = callingActivities.size() - 1; i >= 0; i--) {
                    if (callingActivities.get(i).sessionId == sessionId) {
                        callingActivities.get(i).stopCallTimer();
                        callingActivities.get(i).finish();
                        callingActivities.remove(callingActivities.get(i));
                    }
                }
                endSessionCall(tempSession);
            }
        }
        if (incomingActivity != null) {
            incomingActivity.finish();
            incomingActivity = null;
        }

        tempSession.reset();
    }

    @Override
    public void onInviteUpdated(long sessionId, String audioCodecs,
                                String videoCodecs, boolean existsAudio, boolean existsVideo) {

        Log.e("SIP", "Invite Updated: " + audioCodecs + ", " + videoCodecs + ", " + String.valueOf(existsVideo));

        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        if (existsVideo) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }
        if (existsAudio) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }

        tempSession.setDescriptionString("Call is updated");
    }

    @Override
    public void onInviteConnected(long sessionId) {
        Log.e("SIP", String.format("Invite Connected: %d", sessionId));

        ContextHelper.stopRingtone();

        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Call is connected");
        sendSessionChangeMessage("Call is connected", SESSION_CHANG);

        updateSessionVideo();

        if (callingActivities.size() > 0) {
            for (int i = 0; i < callingActivities.size(); i++) {
                if (callingActivities.get(i).sessionId == sessionId) {
                    callingActivities.get(i).setCallStatus(getString(R.string.calling));
                }
            }
        }
    }

    @Override
    public void onInviteBeginingForward(String forwardTo) {
        sendSessionChangeMessage("An incoming call was forwarded to: " + forwardTo, SESSION_CHANG);
    }

    @Override
    public void onInviteClosed(long sessionId) {
        Log.e("SIP", String.format("Invite Closed: %d", sessionId));

        ContextHelper.stopRingtone();

        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        sdk.displayLocalVideo(false);

        if (callingActivities.size() == 0 && incomingActivity != null) {
            CallLog log = new CallLog();
            log.ContactId = 0;
            log.Name = incomingActivity.callerName;
            log.Phone = incomingActivity.callerNumber;
            log.Timer = new Date();
            log.Status = "Missed";
            mDatabase.insertCallLog(log);
        }

        updateSessionVideo();
        tempSession.setDescriptionString(": Call closed.");
        sendSessionChangeMessage(": Call closed.", SESSION_CHANG);

        if (callingActivities.size() > 0) {
            if (isConference()) {
                int inCalling = 0;
                for (int i = Line.LINE_BASE; i < Line.MAX_LINES; i++) {
                    if (_CallSessions[i].getSessionState()) {
                        ++inCalling;
                    }
                }

                if (inCalling == 1) {
                    callingActivities.get(callingActivities.size() - 1).stopCallTimer();
                    callingActivities.get(callingActivities.size() - 1).finish();
                    callingActivities.remove(callingActivities.get(callingActivities.size() - 1));
                    endSessionCall(tempSession);
                } else {
                    if (tempSession.getSessionState()) {
                        sdk.hangUp(tempSession.getSessionId());
                    }
                }
            } else {
                for (int i = callingActivities.size() - 1; i >= 0; i--) {
                    if (callingActivities.get(i).sessionId == sessionId) {
                        callingActivities.get(i).stopCallTimer();
                        callingActivities.get(i).finish();
                        callingActivities.remove(callingActivities.get(i));
                    }
                }
                endSessionCall(tempSession);
            }
        }
        if (incomingActivity != null) {
            incomingActivity.finish();
            incomingActivity = null;
        }

        tempSession.reset();
    }

    @Override
    public void onRemoteHold(long sessionId) {
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Placed on hold by remote.");
        sendSessionChangeMessage("Placed on hold by remote.", SESSION_CHANG);
    }

    @Override
    public void onRemoteUnHold(long sessionId, String audioCodecs,
                               String videoCodecs, boolean existsAudio, boolean existsVideo) {
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Take off hold by remote.");
        sendSessionChangeMessage("Take off hold by remote.", SESSION_CHANG);
    }

    @Override
    public void onRecvDtmfTone(long sessionId, int tone) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onReceivedRefer(long sessionId, final long referId, String to,
                                String from, final String referSipMessage) {
        final Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            sdk.rejectRefer(referId);
            return;
        }

        final Line referSession = findIdleLine();

        if (referSession == null)// all sessions busy
        {
            sdk.rejectRefer(referId);
            return;
        } else {
            referSession.setSessionState(true);
        }

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_NEGATIVE: {

                        sdk.rejectRefer(referId);
                        referSession.reset();
                    }
                    break;
                    case DialogInterface.BUTTON_POSITIVE: {

                        sdk.hold(tempSession.getSessionId());// hold current session
                        tempSession.setHoldState(true);

                        tempSession.setDescriptionString("Place currently call on hold on line: ");

                        long referSessionId = sdk.acceptRefer(referId, referSipMessage);
                        if (referSessionId <= 0) {
                            referSession.setDescriptionString("Failed to accept REFER on line");

                            referSession.reset();

                            // Take off hold
                            sdk.unHold(tempSession.getSessionId());
                            tempSession.setHoldState(false);
                        } else {
                            referSession.setSessionId(referSessionId);
                            referSession.setSessionState(true);
                            referSession.setReferCall(true, tempSession.getSessionId());

                            referSession.setDescriptionString("Accepted the refer, new call is trying on line ");

                            setCurrentLine(referSession);

                            tempSession.setDescriptionString("Now current line is set to: " + referSession.getLineName());
                            updateSessionVideo();
                        }
                    }
                }

            }
        };
        ContextHelper.showGlobalDialog(this, "2day", "Received REFER", "accept", listener, "reject", listener);
    }

    @Override
    public void onReferAccepted(long sessionId) {
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("the REFER was accepted.");
        sendSessionChangeMessage("the REFER was accepted.", SESSION_CHANG);
    }

    @Override
    public void onReferRejected(long sessionId, String reason, int code) {
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("the REFER was rejected.");
        sendSessionChangeMessage("the REFER was rejected.", SESSION_CHANG);
    }

    @Override
    public void onTransferTrying(long sessionId) {
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Transfer Trying.");
        sendSessionChangeMessage("Transfer Trying.", SESSION_CHANG);
    }

    @Override
    public void onTransferRinging(long sessionId) {
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Transfer Ringing.");
        sendSessionChangeMessage("Transfer Ringing.", SESSION_CHANG);
    }

    @Override
    public void onACTVTransferSuccess(long sessionId) {
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }
        tempSession.setDescriptionString("Transfer succeeded.");
    }

    @Override
    public void onACTVTransferFailure(long sessionId, String reason, int code) {
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Transfer failure");

        // reason is error reason
        // code is error code

    }

    @Override
    public void onReceivedSignaling(long sessionId, String message) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendingSignaling(long sessionId, String message) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onWaitingVoiceMessage(String messageAccount,
                                      int urgentNewMessageCount, int urgentOldMessageCount,
                                      int newMessageCount, int oldMessageCount) {
        String text = messageAccount;
        text += " has voice message.";

        Log.e("SIP", "Waiting Voice Message: " + text);

        showMessage(text);
        // You can use these parameters to check the voice message count

        // urgentNewMessageCount;
        // urgentOldMessageCount;
        // newMessageCount;
        // oldMessageCount;

    }

    @Override
    public void onWaitingFaxMessage(String messageAccount,
                                    int urgentNewMessageCount, int urgentOldMessageCount,
                                    int newMessageCount, int oldMessageCount) {
        String text = messageAccount;
        text += " has FAX message.";

        showMessage(text);
        // You can use these parameters to check the FAX message count

        // urgentNewMessageCount;
        // urgentOldMessageCount;
        // newMessageCount;
        // oldMessageCount;

    }

    @Override
    public void onPresenceRecvSubscribe(long subscribeId, String fromDisplayName, String from, String subject) {

        String fromSipUri = "sip:" + from;

        final long tempId = subscribeId;
        DialogInterface.OnClickListener onClick;
        boolean contactExist = false;

//        SipContact contactReference = null;
//        for (int i = 0; i < contacts.size(); ++i) {
//            contactReference = contacts.get(i);
//            String SipUri = contactReference.getSipAddr();
//
//            if (SipUri.equals(fromSipUri)) {
//                contactExist = true;
//                if (contactReference.isAccept()) {
//                    long nOldSubscribeID = contactReference.getSubId();
//                    sdk.presenceAcceptSubscribe(tempId);
//                    String status = "Available";
//                    sdk.presenceOnline(tempId, status);
//
//                    if (contactReference.isSubscribed() && nOldSubscribeID >= 0) {
//                        sdk.presenceSubscribeContact(fromSipUri, subject);
//                    }
//                    return;
//                } else {
//                    break;
//                }
//            }
//        }
//
//        //
//        if (!contactExist) {
//            contactReference = new SipContact();
//            contacts.add(contactReference);
//            contactReference.setSipAddr(fromSipUri);
//        }
//        final SipContact contact = contactReference;
//        onClick = new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                switch (which) {
//                    case DialogInterface.BUTTON_POSITIVE:
//                        sdk.presenceAcceptSubscribe(tempId);
//                        contact.setSubId(tempId);
//                        contact.setAccept(true);
//                        String status = "Available";
//                        sdk.presenceOnline(tempId, status);
//                        contact.setSubstatus(true);
//                        break;
//                    case DialogInterface.BUTTON_NEGATIVE:
//                        contact.setAccept(false);// reject subscribe
//                        contact.setSubId(0);
//                        contact.setSubstatus(false);// offline
//
//                        sdk.presenceRejectSubscribe(tempId);
//                        break;
//                    default:
//                        break;
//                }
//                dialog.dismiss();
//            }
//        };
//        showGlobalDialog(from, "Accept", onClick, "Reject", onClick);

    }

    @Override
    public void onPresenceOnline(String fromDisplayName, String from,
                                 String stateText) {

        Log.e("Presence Online", fromDisplayName + " " + from + " " + stateText);

        String fromSipUri = "sip:" + from;
//        SipContact contactReference;
//        for (int i = 0; i < contacts.size(); ++i) {
//            contactReference = contacts.get(i);
//            String SipUri = contactReference.getSipAddr();
//            if (SipUri.endsWith(fromSipUri)) {
//                contactReference.setSubDescription(stateText);
//                contactReference.setSubstatus(true);// online
//            }
//        }
        sendSessionChangeMessage("contact status change.", CONTACT_CHANG);
    }

    @Override
    public void onPresenceOffline(String fromDisplayName, String from) {

        Log.e("Presence Offline", fromDisplayName + " " + from);

        String fromSipUri = "sip:" + from;
//        SipContact contactReference;
//        for (int i = 0; i < contacts.size(); ++i) {
//            contactReference = contacts.get(i);
//            String SipUri = contactReference.getSipAddr();
//            if (SipUri.endsWith(fromSipUri)) {
//                contactReference.setSubstatus(false);// "Offline";
//                contactReference.setSubId(0);
//            }
//        }
        sendSessionChangeMessage("contact status change.", CONTACT_CHANG);
    }

    @Override
    public void onRecvOptions(String optionsMessage) {
        // String text = "Received an OPTIONS message: ";
        // text += optionsMessage.toString();
        // showTips(text);
    }

    @Override
    public void onRecvInfo(String infoMessage) {

        // String text = "Received a INFO message: ";
        // text += infoMessage.toString();
        // showTips(text);
    }

    @Override
    public void onRecvMessage(long sessionId, String mimeType,
                              String subMimeType, byte[] messageData, int messageDataLength) {

    }

    @Override
    public void onRecvOutOfDialogMessage(String fromDisplayName, String from,
                                         String toDisplayName, String to, String mimeType,
                                         String subMimeType, byte[] messageData, int messageDataLength) {
        String text = "Received a " + mimeType + "message(out of dialog) from ";
        text += from;

        if (mimeType.equals("text") && subMimeType.equals("plain")) {
            // String receivedMsg = GetString(messageData);
            showMessage(text);
        } else if (mimeType.equals("application")
            && subMimeType.equals("vnd.3gpp.sms")) {
            // The messageData is binary data

            showMessage(text);
        } else if (mimeType.equals("application")
            && subMimeType.equals("vnd.3gpp2.sms")) {
            // The messageData is binary data
            showMessage(text);

        }
    }

    @Override
    public void onPlayAudioFileFinished(long sessionId, String fileName) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onPlayVideoFileFinished(long sessionId) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onSendMessageSuccess(long sessionId, long messageId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendMessageFailure(long sessionId, long messageId,
                                     String reason, int code) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendOutOfDialogMessageSuccess(long messageId,
                                                String fromDisplayName, String from, String toDisplayName, String to) {
    }

    @Override
    public void onSendOutOfDialogMessageFailure(long messageId,
                                                String fromDisplayName, String from, String toDisplayName,
                                                String to, String reason, int code) {
    }

    @Override
    public void onReceivedRTPPacket(long sessionId, boolean isAudio,
                                    byte[] RTPPacket, int packetSize) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendingRTPPacket(long sessionId, boolean isAudio,
                                   byte[] RTPPacket, int packetSize) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAudioRawCallback(long sessionId, int enum_audioCallbackMode,
                                   byte[] data, int dataLength, int samplingFreqHz) {
        // TODO Auto-generated method stub
        Log.e("Audio Raw Callback", String.format("%d, %d", sessionId, dataLength));
    }

    @Override
    public void onVideoRawCallback(long sessionId, int enum_videoCallbackMode,
                                   int width, int height, byte[] data, int dataLength) {
        // TODO Auto-generated method stub
        Log.e("Video Raw Callback", String.format("%d, %d, %d, %d", sessionId, width, height, dataLength));
    }

    @Override
    public void onVideoDecodedInfoCallback(long sessionId, int width, int height,
                                           int framerate, int bitrate) {
        Log.e("Video Decoded Info", String.format("%d, %d, %d, %d", sessionId, width, height, framerate));

        Line line = findLineBySessionId(sessionId);
        if (width != 0 && height != 0 && (line.getVideoWidth() != width || line.getVideoHeight() != height)) {
            line.setVideoWidth(width);
            line.setVideoHeight(height);
            line.notifyObservers();
        }
    }

    private void showMessage(String message) {
        ContextHelper.showGlobalDialog(this, getString(R.string.app_name), message,
            null, null,
            getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                }
            });
    }

    public void quit() {
        offline();
        ((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).cancelAll();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int initializeSip() {
        Profile profile = ContextHelper.getProfile(getApplicationContext());
        if (profile.sipAccounts.length > 0) {
            SipAccount account = profile.sipAccounts[0];

            Environment.getExternalStorageDirectory();
            String LogPath = Environment.getExternalStorageDirectory().getAbsolutePath() + '/';

            //String localIP = getLocalIP(false);
            String localIP = "0.0.0.0";
            int localPort = new Random().nextInt(4940) + 5060;

            sdk.CreateCallManager(getApplicationContext());// step 1

            int result = sdk.initialize(PortSipEnumDefine.ENUM_TRANSPORT_UDP,
                PortSipEnumDefine.ENUM_LOG_LEVEL_NONE, LogPath,
                Line.MAX_LINES, "PortSIP VoIP SDK for Android",
                0, 0);// step 2

            Log.e("Initialize SIP", String.format("Initialize: %d", result));

            if (result != PortSipErrorcode.ECoreErrorNone) {
                updateStatus("init Sdk Failed");
                return result;
            }

            sdk.setSrtpPolicy(PortSipEnumDefine.ENUM_SRTPPOLICY_NONE);

            int nSetKeyRet = sdk.setLicenseKey(licenseKey);// step 3

            Log.e("Initialize SIP", String.format("License key: %d", nSetKeyRet));

            if (nSetKeyRet == PortSipErrorcode.ECoreTrialVersionLicenseKey) {
                ContextHelper.showInfo(mainActivity, getString(R.string.app_name), getString(R.string.trial_version_tips));
            } else if (nSetKeyRet == PortSipErrorcode.ECoreWrongLicenseKey) {
                ContextHelper.showInfo(mainActivity, getString(R.string.app_name), getString(R.string.wrong_licence_tips));
                return -1;
            }

            String[] parts = account.sipDomain.split(":");
            if (parts.length != 2) {
                ContextHelper.showInfo(mainActivity, getString(R.string.app_name), getString(R.string.no_sip_account));
                return -1;
            }
            int port;
            try {
                port = Integer.parseInt(parts[1]);
            } catch (NumberFormatException exc) {
                port = 5060;
            }
            result = sdk.setUser(account.sipUsername, profile.sipDisplayName, account.sipUsername, account.sipPin,
                localIP, localPort, "", parts[0], port,
                "", port, null, 5060);// step 4

            Log.e("Initialize SIP", String.format("Set user: %d", result));

            if (result != PortSipErrorcode.ECoreErrorNone) {
                updateStatus("setUser resource failed");
                return result;
            }

            addVoiceCodec();
            addVideoCodec();

            sdk.setVideoNackStatus(true);
            sdk.enableVideoDecoderCallback(true);

            int[] resolution = getResolution();
            sdk.setVideoResolution(resolution[0], resolution[1]);

            //sdk.enableVAD(true);
            //sdk.enableAEC(PortSipEnumDefine.ENUM_EC_DEFAULT);
            //sdk.enableANS(PortSipEnumDefine.ENUM_NS_DEFAULT);
            //sdk.enableAGC(true ? PortSipEnumDefine.ENUM_AGC_DEFAULT : PortSipEnumDefine.ENUM_AGC_NONE);
            //sdk.enableCNG(true);
            //sdk.enableReliableProvisional(true);

            // Use earphone
            speaker(false);

            // Use Front Camera
            sdk.setVideoDeviceId(1);
            sdk.setVideoOrientation(PortSipEnumDefine.ENUM_ROTATE_CAPTURE_FRAME_270);

            return PortSipErrorcode.ECoreErrorNone;
        }

        ContextHelper.showInfo(this, getString(R.string.app_name), getString(R.string.no_sip_account));
        return -1;
    }

    public void registerSip() {
        Profile profile = ContextHelper.getProfile(getApplicationContext());
        if (profile.sipAccounts.length > 0) {
            SipAccount account = profile.sipAccounts[0];
            int localPort = new Random().nextInt(4940) + 5060;
            String[] parts = account.sipDomain.split(":");
            int port;
            try {
                port = Integer.parseInt(parts[1]);
            } catch (NumberFormatException exc) {
                port = 5060;
            }
            sdk.setUser(account.sipUsername, profile.sipDisplayName, account.sipUsername, account.sipPin,
                getLocalIP(false), localPort, "", parts[0], port,
                account.sipStun, port, null, 5060);
            sdk.registerServer(100, 3);
        }
    }

    public void online() {
        if (_SIPConnecting == true) {
            return;
        }

        _SIPConnecting = true;

        int result = initializeSip();

        if (result == PortSipErrorcode.ECoreErrorNone) {
            result = sdk.registerServer(100, 3);

            Log.e("Initialize SIP", String.format("Register server: %d", result));

            if (result != PortSipErrorcode.ECoreErrorNone) {
                updateStatus("Register server failed");
            }
        } else {
            updateStatus("OK");
        }

        setLoginState(true);
        _SIPConnecting = false;
    }

    public void offline() {
        if (!isOnline()) {
            return;
        }
        for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i) {
            if (_CallSessions[i].getRecvCallState()) {
                sdk.rejectCall(_CallSessions[i].getSessionId(), 480);
            } else if (_CallSessions[i].getSessionState()) {
                sdk.hangUp(_CallSessions[i].getSessionId());
            }

            _CallSessions[i].reset();
        }

        setLoginState(false);
        updateStatus("OK");
        int ret = sdk.unRegisterServer();
        sdk.DeleteCallManager();

        Log.e("Unregister SIP", String.valueOf(ret));
    }

    private void updateStatus(String message) {
        if (mainActivity != null) {
            mainActivity.updateStatus(message);
        }
        if (callActivity != null) {
            callActivity.updateStatus(message);
        }
        if (callingActivities.size() > 0) {
            for (int i = 0; i < callingActivities.size(); i++) {
                callingActivities.get(i).updateStatus(message);
            }
        }
    }

    public void forwardCallToExtensionNumber(Line session, String extNumber) {
        int result = sdk.forwardCall(session.getSessionId(), extNumber);
    }

    public void call(Contact contact, boolean video, boolean keepNumber) {
        if (isOnline() == false) {
            if (mainActivity != null) {
                ContextHelper.showAlert(mainActivity, "You Are Not Connected", getString(R.string.sip_not_connected));
            } else {
                ContextHelper.showTipMessage(this, getString(R.string.sip_not_connected));
            }
            return;
        }

        boolean useSip = false;
        String number = contact.Phone;
        if ((contact.SipAccount != null && !TextUtils.isEmpty(contact.SipAccount))
            || number == null || TextUtils.isEmpty(number)) {
            number = contact.SipAccount;
            useSip = true;
        }
        if (number == null || TextUtils.isEmpty(number)) {
            ContextHelper.showTipMessage(this, getString(R.string.error_call_no_number));
            return;
        }

        float balance = ContextHelper.getSipBalance(this);
        if (video == false && ContextHelper.getLocalMode(this) == true) {
            if (!useSip && balance < 0.25) {
                ContextHelper.showTipMessage(this, getString(R.string.error_not_enough_credit));
                return;
            }
        } else if (video && !useSip) {
            ContextHelper.showTipMessage(this, getString(R.string.error_video_call_non_sip_user));
            return;
        }

        // Save last call number
        ContextHelper.setLastCallNumber(this, number);

        number = number.replace(" ", "");

        if (!keepNumber && ContextHelper.getLocalMode(this) && !number.startsWith("+") && !useSip) {
            String[] countryCodes = ContextHelper.getCountryCodes(this);
            if (countryCodes[1] != "") {
                if (number.substring(0, 1).equalsIgnoreCase("0")) {
                    number = number.substring(1, number.length());
                }
                number = countryCodes[1] + number;
            }
        }

        number = number.replace("+", "");

        Line freeLine = findIdleLine();
        if (freeLine == null) {
            ContextHelper.showTipMessage(this, getString(R.string.all_lines_busy));
            return;
        }

        // Ensure that we have been added one audio codec at least
        if (sdk.isAudioCodecEmpty()) {
            Log.e("Calling", "No audio codec");
            addVoiceCodec();
            //ContextHelper.showTipMessage(this, getString(R.string.missing_audio_codec));
            //return;
        }

        if (video && sdk.isVideoCodecEmpty()) {
            Log.e("Calling", "No video codec");
            addVideoCodec();
        }

        // Usually for 3PCC need to make call without SDP
        long sessionId = sdk.call(number, true, video);
        if (sessionId <= 0) {
            ContextHelper.showTipMessage(this, getString(R.string.call_failure));
            return;
        }

        // Save to log
        CallLog log = new CallLog();
        log.ContactId = contact.Id;
        log.Avatar = contact.Avatar;
        log.Name = contact.Name;
        log.Phone = number;
        log.Timer = new Date();
        log.Status = "Outgoing";
        mDatabase.insertCallLog(log);

        freeLine.setSessionId(sessionId);
        freeLine.setSessionState(true);
        freeLine.setVideoState(video);
        setCurrentLine(freeLine);
        updateSessionVideo();

        Intent intent = new Intent(this, CallingActivity.class);
        intent.putExtra(CallingActivity.IS_VIDEO_CALL, video);
        intent.putExtra(CallingActivity.CALLER_CONTACT_ID, contact.Id);
        intent.putExtra(CallingActivity.CALLER_DISPLAY_NAME, contact.Name);
        intent.putExtra(CallingActivity.CALLER_NUMBER, number);
        intent.putExtra(CallingActivity.RESET_TIMER, true);
        ContextHelper.startActivityAsFront(this, intent);
    }

    public void conference(ArrayList<Contact> contacts, boolean video, boolean keepNumber) {
        if (isOnline() == false) {
            if (mainActivity != null) {
                ContextHelper.showAlert(mainActivity, "You Are Not Connected", getString(R.string.sip_not_connected));
            } else {
                ContextHelper.showTipMessage(this, getString(R.string.sip_not_connected));
            }
            return;
        }

        // Ensure that we have been added one audio codec at least
        if (sdk.isAudioCodecEmpty()) {
            //ContextHelper.showTipMessage(this, getString(R.string.missing_audio_codec));
            addVoiceCodec();
        }

        setConferenceMode(true);

        int[] resolution = getResolution();
        int result = sdk.createConference(getRemoteSurfaceView(), resolution[0], resolution[1], false);

        // Save to log
        CallLog log = new CallLog();
        log.ContactId = 0;
        log.Name = "";
        log.Phone = "";
        for (int i = 0; i < contacts.size(); i++) {
            Line freeLine = findIdleLine();
            if (freeLine == null) {
                ContextHelper.showTipMessage(this, getString(R.string.all_lines_busy));
                break;
            }

            boolean useSip = false;
            String number = contacts.get(i).Phone;
            if (number == null || TextUtils.isEmpty(number) ||
                (contacts.get(i).SipAccount != null && !TextUtils.isEmpty(contacts.get(i).SipAccount))) {
                number = contacts.get(i).SipAccount;
                useSip = true;
            }
            if (number == null || TextUtils.isEmpty(number)) {
                continue;
            }

            log.Name += contacts.get(i).Name + ";";
            log.Phone += number + ";";

            number = number.replace(" ", "");

            if (!keepNumber && ContextHelper.getLocalMode(this) && !number.startsWith("+") && !useSip) {
                String[] countryCodes = ContextHelper.getCountryCodes(this);
                if (countryCodes[1] != "") {
                    if (number.substring(0, 1).equalsIgnoreCase("0")) {
                        number = number.substring(1, number.length());
                    }
                    number = countryCodes[1] + number;
                }
            }

            number = number.replace("+", "");

            // Usually for 3PCC need to make call without SDP
            Log.e("Conference: call ", number);
            long sessionId = sdk.call(number, true, video);
            if (sessionId <= 0) {
                ContextHelper.showTipMessage(this, getString(R.string.call_failure));
                continue;
            }

            freeLine.setSessionId(sessionId);
            freeLine.setSessionState(true);
            freeLine.setVideoState(video);

            if (log.ContactId == 0) {
                log.ContactId = contacts.get(i).Id;
                log.Avatar = contacts.get(i).Avatar;
                setCurrentLine(freeLine);
            }

            sdk.joinToConference(sessionId);
        }

        log.Timer = new Date();
        log.Status = "Outgoing";
        mDatabase.insertCallLog(log);

        updateSessionVideo();

        Intent intent = new Intent(this, CallingActivity.class);
        intent.putExtra(CallingActivity.IS_VIDEO_CALL, video);
        intent.putExtra(CallingActivity.CALLER_CONTACT_ID, log.ContactId);
        intent.putExtra(CallingActivity.CALLER_DISPLAY_NAME, log.Name);
        intent.putExtra(CallingActivity.CALLER_NUMBER, log.Phone);
        intent.putExtra(CallingActivity.RESET_TIMER, true);
        ContextHelper.startActivityAsFront(this, intent);
    }

    public void startConference(boolean video) {
        int[] resolution = getResolution();
        int rt = sdk.createConference(getRemoteSurfaceView(), resolution[0], resolution[1], false);
        if (rt == 0) {
            Line[] sessions = getLines();
            for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i) {
                if (sessions[i].getSessionState()) {
                    if (sessions[i].getHoldState()) {
                        sdk.unHold(sessions[i].getSessionId());
                        if (video) {
                            sdk.sendVideo(sessions[i].getSessionId(), sessions[i].getVideoState());
                        }
                    }
                    sdk.joinToConference(sessions[i].getSessionId());
                    sessions[i].setHoldState(false);
                }
            }

            setConferenceMode(true);
        } else {
            showMessage("Failed to create conference");
            setConferenceMode(false);
        }
    }

    public void addToConference(Contact contact, boolean video) {
        Line freeLine = findIdleLine();
        if (freeLine == null) {
            ContextHelper.showTipMessage(this, getString(R.string.all_lines_busy));
            return;
        }

        boolean useSip = false;
        String number = contact.Phone;
        if (number == null || TextUtils.isEmpty(number) ||
            (contact.SipAccount != null && !TextUtils.isEmpty(contact.SipAccount))) {
            number = contact.SipAccount;
            useSip = true;
        }
        if (number == null || TextUtils.isEmpty(number)) {
            Toast.makeText(this, R.string.error_call_no_number, Toast.LENGTH_LONG).show();
            return;
        }

        // Ensure that we have been added one audio codec at least
        if (sdk.isAudioCodecEmpty()) {
            Log.e("Calling", "No audio codec");
            addVoiceCodec();
            //ContextHelper.showTipMessage(this, getString(R.string.missing_audio_codec));
            //return;
        }

        if (video && sdk.isVideoCodecEmpty()) {
            Log.e("Calling", "No video codec");
            addVideoCodec();
        }

        number = number.replace(" ", "");

        if (ContextHelper.getLocalMode(this) && !number.startsWith("+") && !useSip) {
            String[] countryCodes = ContextHelper.getCountryCodes(this);
            if (countryCodes[1] != "") {
                if (number.substring(0, 1).equalsIgnoreCase("0")) {
                    number = number.substring(1, number.length());
                }
                number = countryCodes[1] + number;
            }
        }

        number = number.replace("+", "");

        // Usually for 3PCC need to make call without SDP
        long sessionId = sdk.call(number, true, video);
        if (sessionId <= 0) {
            ContextHelper.showTipMessage(this, getString(R.string.call_failure));
            return;
        }

        freeLine.setSessionId(sessionId);
        freeLine.setSessionState(true);
        freeLine.setVideoState(video);
        updateSessionVideo();
    }

    public void stopConference() {
        // Before stop the conference, MUST place all lines to hold state
        setConferenceMode(false);
        Line[] sessions = getLines();
        for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i) {
            if (sessions[i].getSessionState()) {
                if (_CurrentlyLine == null || _CurrentlyLine.getSessionId() != sessions[i].getSessionId()) {
                    sdk.removeFromConference(sessions[i].getSessionId());

                    // Hold the line
                    if (!sessions[i].getHoldState()) {
                        sdk.hold(sessions[i].getSessionId());
                        sessions[i].setHoldState(true);
                    }
                }
            }
        }
        sdk.destroyConference();
    }

    public int answerSessionCall(String callerDisplayName, String caller, Line sessionLine, boolean videoCall) {
        long sessionId = sessionLine.getSessionId();
        int rt = PortSipErrorcode.INVALID_SESSION_ID;
        if (sessionId != PortSipErrorcode.INVALID_SESSION_ID) {
            rt = sdk.answerCall(sessionLine.getSessionId(), videoCall);
            Log.e("Answer error", String.valueOf(rt));
        }
        if (rt == PortSipErrorcode.ECoreErrorNone) {
            // Save to log
            CallLog log = new CallLog();
            log.ContactId = 0;
            log.Name = callerDisplayName;
            log.Phone = caller;
            log.Timer = new Date();
            log.Status = "Incoming";
            mDatabase.insertCallLog(log);

            sessionLine.setSessionState(true);
            setCurrentLine(sessionLine);
            if (videoCall) {
                sessionLine.setVideoState(true);
            } else {
                sessionLine.setVideoState(false);
            }
            updateSessionVideo();

            if (conference) {
                sdk.joinToConference(sessionLine.getSessionId());
                sdk.sendVideo(sessionLine.getSessionId(), sessionLine.getVideoState());
            } else {
                // Hold current call
                for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i) {
                    if (_CallSessions[i].getSessionState() && _CallSessions[i].getSessionId() != sessionId) {
                        getPortSIPSDK().hold(_CallSessions[i].getSessionId());
                        _CallSessions[i].setHoldState(true);
                    }
                }
            }

            // Always activate ear speaker
            speaker(false);
            mute(sessionLine, false);
            muteVideo(sessionLine, false, false);

            Intent intent = new Intent(this, CallingActivity.class);
            intent.putExtra(CallingActivity.IS_VIDEO_CALL, videoCall);
            intent.putExtra(CallingActivity.CALLER_DISPLAY_NAME, callerDisplayName);
            intent.putExtra(CallingActivity.CALLER_NUMBER, caller);
            intent.putExtra(CallingActivity.START_TIMER, true);
            ContextHelper.startActivityAsFront(this, intent);

            //ContextHelper.showTipMessage(this, sessionLine.getLineName() + ": Call established");
        } else {
            sessionLine.reset();
            ContextHelper.showTipMessage(this, sessionLine.getLineName() + ": failed to answer call !");
        }

        return rt;
    }

    public void endSessionCall(Line sessionLine) {
        if (sessionLine == null) {
            return;
        }

        Log.e("End call", String.valueOf(sessionLine.getSessionId()));

        long sessionId = sessionLine.getSessionId();
        if (isConference()) {
            sdk.removeFromConference(sessionLine.getSessionId());
        }

        if (sessionLine.getSessionState()) {
            sdk.hangUp(sessionLine.getSessionId());
            updateSessionVideo();
            sessionLine.reset();
        } else {
            updateSessionVideo();
        }

        long activeSession = -1;
        if (isConference()) {
            stopConference();
            for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i) {
                if (_CallSessions[i].getSessionState()) {
                    sdk.hangUp(_CallSessions[i].getSessionId());
                    updateSessionVideo();
                    _CallSessions[i].reset();
                }
            }
        } else {
            // Unhold
            for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i) {
                if (_CallSessions[i].getSessionState() && _CallSessions[i].getHoldState()) {
                    getPortSIPSDK().unHold(_CallSessions[i].getSessionId());
                    _CallSessions[i].setHoldState(false);
                    setCurrentLine(_CallSessions[i]);
                    activeSession = _CallSessions[i].getSessionId();
                }
            }
        }

        // Remove calling
        if (callingActivities.size() > 0) {
            for (int i = callingActivities.size() - 1; i >= 0; i--) {
                if (callingActivities.get(i).sessionId == sessionId) {
                    callingActivities.get(i).stopCallTimer();
                    callingActivities.remove(callingActivities.get(i));
                    break;
                }
            }
            for (int i = 0; i < callingActivities.size(); i++) {
                if (callingActivities.get(i).sessionId == activeSession) {
                    Intent intent = callingActivities.get(i).getIntent();
                    intent.putExtra(CallingActivity.START_TIMER, true);
                    intent.putExtra(CallingActivity.RESET_TIMER, false);
                    intent.putExtra(CallingActivity.TIMER, callingActivities.get(i).startTime);
                    ContextHelper.startActivityAsFront(this, intent);
                    break;
                }
            }
        }
    }

    public void rejectCall(Line sessionLine) {
        if (sessionLine == null) {
            return;
        }

        sdk.rejectCall(sessionLine.getSessionId(), 480);
        updateSessionVideo();

        for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i) {
            if (_CallSessions[i].getSessionState() && _CallSessions[i].getHoldState() && _CallSessions[i].getSessionId() != sessionLine.getSessionId()) {
                getPortSIPSDK().unHold(_CallSessions[i].getSessionId());
                _CallSessions[i].setHoldState(false);
                setCurrentLine(_CallSessions[i]);
            }
        }

        sessionLine.reset();
    }

    public void sendKey(Line sessionLine, String number) {
        if (sessionLine == null) {
            return;
        }

        if (isOnline() && sessionLine.getSessionState()) {
            if (number.equals("*")) {
                sdk.sendDtmf(sessionLine.getSessionId(), PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833, 10, 160, true);
                return;
            } else if (number.equals("#")) {
                sdk.sendDtmf(sessionLine.getSessionId(), PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833, 11, 160, true);
                return;
            } else if (number.equals("A")) {
                sdk.sendDtmf(sessionLine.getSessionId(), PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833, 12, 160, true);
                return;
            } else if (number.equals("B")) {
                sdk.sendDtmf(sessionLine.getSessionId(), PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833, 13, 160, true);
                return;
            } else if (number.equals("C")) {
                sdk.sendDtmf(sessionLine.getSessionId(), PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833, 14, 160, true);
                return;
            } else if (number.equals("D")) {
                sdk.sendDtmf(sessionLine.getSessionId(), PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833, 15, 160, true);
                return;
            }
            try {
                int sum = Integer.valueOf(number); // 0~9
                sdk.sendDtmf(sessionLine.getSessionId(), PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833, sum, 160, true);
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
    }

    public void mute(Line sessionLine, boolean mute) {
        sdk.muteSession(sessionLine.getSessionId(), false, mute, false, mute);
    }

    public void muteVideo(Line sessionLine, boolean mute, boolean voiceMute) {
        sdk.muteSession(sessionLine.getSessionId(), false, voiceMute, false, mute);
    }

    public void speaker(boolean on) {
        sdk.setLoudspeakerStatus(on);
        if (on == false) {
            sdk.setSpeakerVolume(10);
        } else {
            sdk.setSpeakerVolume(255);
        }
    }

    public void updateProfile() {
        if (mainActivity != null) {
            mainActivity.setInfo();
        }
    }

    private void addVoiceCodec() {
        sdk.clearAudioCodec();
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_G722);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_G729);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_AMR);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_AMRWB);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_GSM);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_PCMA);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_PCMU);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_SPEEX);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_SPEEXWB);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_ILBC);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_ISACWB);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_ISACSWB);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_OPUS);
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_DTMF);
    }

    private void addVideoCodec() {
        sdk.clearVideoCodec();
        sdk.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_H263);
        sdk.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_H263_1998);
        sdk.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_H264);
        sdk.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_VP8);
    }

    private int[] getResolution() {
        String resolution = "2";
        int width = 352;
        int height = 288;
        if (resolution.equals("1"))//QCIF
        {
            width = 176;
            height = 144;
        } else if (resolution.equals("2"))//CIF
        {
            width = 352;
            height = 288;
        } else if (resolution.equals("3"))//VGA
        {
            width = 640;
            height = 480;
        } else if (resolution.equals("4"))//SVGA
        {
            width = 1024;
            height = 768;
        } else if (resolution.equals("5"))//XVGA
        {
            width = 640;
            height = 480;
        } else if (resolution.equals("6"))//720P
        {
            width = 1280;
            height = 720;
        } else if (resolution.equals("7"))//QVGA
        {
            width = 320;
            height = 240;
        }

        width = 480;
        height = 320;

        return new int[]{width, height};
    }

    private void setupAudio() {
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMicrophoneMute(false);
    }
}
