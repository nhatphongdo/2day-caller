package sg.today.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sg.R;
import sg.today.AndroidBug5497Workaround;
import sg.today.model.Profile;
import sg.today.model.SipAccount;
import sg.today.service.ApiServices;
import sg.today.service.ContextHelper;
import sg.today.service.JsonObjectResultInterface;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity {

    // UI references.
    private EditText mUsernameView;
    private EditText mPasswordView;
    private CheckBox mRememberView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        AndroidBug5497Workaround.assistActivity(this);

        // Set up the login form.
        mUsernameView = (EditText) findViewById(R.id.username);
        mUsernameView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    mPasswordView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        mRememberView = (CheckBox) findViewById(R.id.remember_me);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        Button mForgotPasswordButton = (Button) findViewById(R.id.forgot_password_button);
        mForgotPasswordButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ContextHelper.hideKeyboard(getContext());
                Intent mForgotPasswordIntent = new Intent(getContext(), ForgotPasswordActivity.class);
                startActivity(mForgotPasswordIntent);
            }
        });

        Button mRegisterButton = (Button) findViewById(R.id.register_button);
        mRegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ContextHelper.hideKeyboard(getContext());
                Intent mRegisterIntent = new Intent(getContext(), RegisterActivity.class);
                startActivity(mRegisterIntent);
            }
        });

        // Recall if remember
        mRememberView.setChecked(ContextHelper.getRemember(getContext()));
        if (ContextHelper.getRemember(getContext())) {
            String[] account = ContextHelper.getAccount(getContext());
            mUsernameView.setText(account[0]);
            mPasswordView.setText(account[1]);
            mEmailSignInButton.performClick();
        }
    }

    public LoginActivity getContext() {
        return this;
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        final String username = mUsernameView.getText().toString();
        final String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_username_required));
            focusView = mUsernameView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_password_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            ContextHelper.hideKeyboard(getContext());
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            try {
                ApiServices.getInstance(getContext()).Login(username, password, new JsonObjectResultInterface() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.optInt("result_code") == 1) {
                            // Successful
                            ContextHelper.setRemember(getContext(), mRememberView.isChecked());
                            ContextHelper.setAccount(getContext(), username, password);

                            String token = ContextHelper.getGcmToken(getContext());
                            Log.e("Firebase Token", token);
                            if (token != "") {
                                try {
                                    ApiServices.getInstance(getContext()).SetGcmToken(username, password, token, new JsonObjectResultInterface() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            Log.e("GCM token: ", String.valueOf(response));
                                        }

                                        @Override
                                        public void onErrorResponse(String error) {

                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            // Get account detail
                            try {
                                ApiServices.getInstance(getContext()).GetAccountDetail(username, password, new JsonObjectResultInterface() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        if (response.optInt("result_code") != 1) {
                                            showProgress(false);
                                            mPasswordView.setError(getString(R.string.error_incorrect_password));
                                            mPasswordView.requestFocus();
                                            return;
                                        }

                                        try {
                                            final Profile profile = new Profile(response.optJSONObject("data"));
                                            if (!TextUtils.isEmpty(profile.profileImage)) {
                                                ImageLoader mImageLoader = ApiServices.getInstance(getContext()).getImageLoader();
                                                mImageLoader.get(profile.profileImage, new ImageLoader.ImageListener() {
                                                    @Override
                                                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                                                        if (response.getBitmap() != null) {
                                                            profile.profileImageBitmapData = response.getBitmap();
                                                        }
                                                    }

                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {

                                                    }
                                                });
                                            }
                                            ContextHelper.setProfile(getContext(), profile);

                                            ApiServices.getInstance(getContext()).GetProfileDetail(username, password, new JsonObjectResultInterface() {
                                                @Override
                                                public void onResponse(JSONObject response) {
                                                    final Profile profile = ContextHelper.getProfile(getContext());
                                                    if (response.optInt("result_code") == 1) {
                                                        final JSONObject data = response.optJSONObject("data");
                                                        // Save account's detail
                                                        if (!TextUtils.isEmpty(data.optString("profileimage"))) {
                                                            profile.profileImage = data.optString("profileimage");
                                                            ImageLoader mImageLoader = ApiServices.getInstance(getContext()).getImageLoader();
                                                            mImageLoader.get(data.optString("profileimage"), new ImageLoader.ImageListener() {
                                                                @Override
                                                                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                                                                    if (response.getBitmap() != null) {
                                                                        profile.profileImageBitmapData = response.getBitmap();
                                                                    }
                                                                }

                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {
                                                                    error.printStackTrace();
                                                                }
                                                            });
                                                        }

                                                        Log.e("Login", data.optString("profileimage"));
                                                        profile.username = String.valueOf(data.opt("username"));
                                                        profile.email = data.optString("email");
                                                        profile.firstName = String.valueOf(data.opt("first_name"));
                                                        profile.lastName = String.valueOf(data.opt("last_name"));
                                                        profile.countryCode = String.valueOf(data.opt("countrycode"));
                                                        profile.mobileNumber = String.valueOf(data.opt("mobileno"));
                                                        Log.e("Login", String.valueOf(profile.profileImageBitmapData));

                                                        ContextHelper.setProfile(getContext(), profile);
                                                    }

                                                    // Load sip info
                                                    try {
                                                        ApiServices.getInstance(getContext()).GetSipInfo(username, password, new JsonObjectResultInterface() {
                                                            @Override
                                                            public void onResponse(JSONObject response) {
                                                                JSONArray sips = response.optJSONArray("data");
                                                                profile.sipAccounts = new SipAccount[sips.length()];
                                                                for (int i = 0; i < sips.length(); i++) {
                                                                    profile.sipAccounts[i] = new SipAccount(sips.optJSONObject(i));
                                                                    Log.e("SIP", profile.sipAccounts[i].sipUsername);
                                                                    Log.e("SIP", profile.sipAccounts[i].sipPin);
                                                                    Log.e("SIP", profile.sipAccounts[i].sipDomain);
                                                                }
                                                                ContextHelper.setProfile(getContext(), profile);

                                                                // Move to Menu
                                                                Intent mainIntent = new Intent(getContext(), MainActivity.class);
                                                                startActivity(mainIntent);
                                                                finish();
                                                            }

                                                            @Override
                                                            public void onErrorResponse(String error) {
                                                                showProgress(false);
                                                                // Show error
                                                                ContextHelper.showAlert(getContext(), getString(R.string.title_activity_login), error);
                                                            }
                                                        });
                                                    } catch (JSONException e) {
                                                        showProgress(false);
                                                        e.printStackTrace();
                                                        // Show error
                                                        ContextHelper.showAlert(
                                                            getContext(),
                                                            getString(R.string.title_activity_login),
                                                            getString(R.string.api_format_error));
                                                    }
                                                }

                                                @Override
                                                public void onErrorResponse(String error) {
                                                    showProgress(false);
                                                    // Show error
                                                    ContextHelper.showAlert(getContext(), getString(R.string.title_activity_login), error);
                                                }
                                            });
                                        } catch (JSONException e) {
                                            showProgress(false);
                                            e.printStackTrace();
                                            // Show error
                                            ContextHelper.showAlert(
                                                getContext(),
                                                getString(R.string.title_activity_login),
                                                getString(R.string.api_format_error));
                                        }
                                    }

                                    @Override
                                    public void onErrorResponse(String error) {
                                        showProgress(false);
                                        // Show error
                                        ContextHelper.showAlert(getContext(), getString(R.string.title_activity_login), error);
                                    }
                                });
                            } catch (JSONException e) {
                                showProgress(false);
                                e.printStackTrace();
                                // Show error
                                ContextHelper.showAlert(
                                    getContext(),
                                    getString(R.string.title_activity_login),
                                    getString(R.string.api_format_error));
                            }
                        } else {
                            showProgress(false);
                            Log.e("Login", response.optString("result_code"));
                            mPasswordView.setError(getString(R.string.error_incorrect_password));
                            mPasswordView.requestFocus();
                        }
                    }

                    @Override
                    public void onErrorResponse(String error) {
                        showProgress(false);
                        // Show error
                        ContextHelper.showAlert(getContext(), getString(R.string.title_activity_login), error);
                    }
                });
            } catch (JSONException e) {
                showProgress(false);
                e.printStackTrace();
                // Show error
                ContextHelper.showAlert(
                    getContext(),
                    getString(R.string.title_activity_login),
                    getString(R.string.api_format_error));
            }
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}

