package sg.today.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import sg.R;
import sg.today.MainApplication;
import sg.today.model.Contact;
import sg.today.service.ContextHelper;
import sg.today.util.Line;

public class KeypadFragment extends Fragment {

    private TextView mNumberView;
    private TextView mCountryCode;
    private TextView mCountryName;

    public KeypadFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_keypad, container, false);

        mNumberView = (TextView) view.findViewById(R.id.call_number);
        mCountryCode = (TextView) view.findViewById(R.id.number_country_code);
        mCountryName = (TextView) view.findViewById(R.id.number_country_name);

        final ImageButton mCheckCountryButton = (ImageButton) view.findViewById(R.id.country_code_button);
        mCheckCountryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean mode = ContextHelper.getLocalMode(getContext());
                if (mode == false) {
                    boolean result = ContextHelper.retrieveCountryCodes(getContext());
                    if (result) {
                        ContextHelper.setLocalMode(getContext(), true);
                        String[] countryCodes = ContextHelper.getCountryCodes(getContext());
                        mCountryCode.setText("+" + countryCodes[1]);
                        mCountryName.setText("(" + countryCodes[0] + ")");
                        mCheckCountryButton.setImageResource(R.drawable.location_icon_selected);
                    }
                } else {
                    ContextHelper.setLocalMode(getContext(), false);
                    mCountryCode.setText("");
                    mCountryName.setText("");
                    mCheckCountryButton.setImageResource(R.drawable.location_icon);
                }
            }
        });
        if (ContextHelper.getLocalMode(getContext())) {
            String[] countryCodes = ContextHelper.getCountryCodes(getContext());
            mCountryCode.setText("+" + countryCodes[1]);
            mCountryName.setText("(" + countryCodes[0] + ")");
            mCheckCountryButton.setImageResource(R.drawable.location_icon_selected);
        } else {
            mCountryCode.setText("");
            mCountryName.setText("");
            mCheckCountryButton.setImageResource(R.drawable.location_icon);
        }

        if (ContextHelper.getLocalMode(getContext())) {
            String[] countryCodes = ContextHelper.getCountryCodes(getContext());
            mCountryCode.setText(String.format("+%s", countryCodes[1]));
        } else {
            mCountryCode.setText("");
        }

        Button mDeleteButton = (Button) view.findViewById(R.id.delete_button);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteNumber();
            }
        });
        mDeleteButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mNumberView != null) {
                    mNumberView.setText("");
                }
                return false;
            }
        });

        final ImageButton mButton0 = (ImageButton) view.findViewById(R.id.number_0);
        final ImageButton mButton1 = (ImageButton) view.findViewById(R.id.number_1);
        final ImageButton mButton2 = (ImageButton) view.findViewById(R.id.number_2);
        final ImageButton mButton3 = (ImageButton) view.findViewById(R.id.number_3);
        final ImageButton mButton4 = (ImageButton) view.findViewById(R.id.number_4);
        final ImageButton mButton5 = (ImageButton) view.findViewById(R.id.number_5);
        final ImageButton mButton6 = (ImageButton) view.findViewById(R.id.number_6);
        final ImageButton mButton7 = (ImageButton) view.findViewById(R.id.number_7);
        final ImageButton mButton8 = (ImageButton) view.findViewById(R.id.number_8);
        final ImageButton mButton9 = (ImageButton) view.findViewById(R.id.number_9);
        final ImageButton mButtonStar = (ImageButton) view.findViewById(R.id.number_star);
        final ImageButton mButtonSharp = (ImageButton) view.findViewById(R.id.number_sharp);

        final ImageButton[] buttons = new ImageButton[]{
                mButton1, mButton2, mButton3,
                mButton4, mButton5, mButton6,
                mButton7, mButton8, mButton9,
                mButtonStar, mButton0, mButtonSharp
        };
        final int[] buttonsDrawable = new int[]{
                R.drawable.key_1, R.drawable.key_2, R.drawable.key_3,
                R.drawable.key_4, R.drawable.key_5, R.drawable.key_6,
                R.drawable.key_7, R.drawable.key_8, R.drawable.key_9,
                R.drawable.key_star, R.drawable.key_0, R.drawable.key_sharp,
        };

        for (int i = 0; i < buttons.length; i++) {
            final int index = i;
            buttons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addNumber(buttons[index].getTag().toString());
                }
            });
        }
        mButton0.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                addNumber("+");
                return true;
            }
        });

        ImageButton mVoiceCallButton = (ImageButton) view.findViewById(R.id.number_voice_call);
        mVoiceCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mNumberView != null && !TextUtils.isEmpty(mNumberView.getText().toString())) {

                    if (!MainApplication.getInstance().isOnline()) {
                        ContextHelper.showAlert(getContext(), "You Are Not Connected", getString(R.string.sip_not_connected));
                        return;
                    }

                    Contact contact = new Contact();
                    contact.Id = 0;
                    contact.Phone = mNumberView.getText().toString();
                    mNumberView.setText("");
                    MainApplication.getInstance().call(contact, false, false);
                }
            }
        });

        ImageButton mVideoCallButton = (ImageButton) view.findViewById(R.id.number_video_call);
        mVideoCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mNumberView != null && !TextUtils.isEmpty(mNumberView.getText().toString())) {
                    if (!MainApplication.getInstance().isOnline()) {
                        ContextHelper.showAlert(getContext(), "You Are Not Connected", getString(R.string.sip_not_connected));
                        return;
                    }

                    Contact contact = new Contact();
                    contact.Id = 0;
                    contact.Phone = mNumberView.getText().toString();
                    if (!ContextHelper.getLocalMode(getContext())) {
                        contact.SipAccount = mNumberView.getText().toString();
                    }
                    mNumberView.setText("");
                    MainApplication.getInstance().call(contact, true, false);
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void addNumber(String number) {
        if (mNumberView == null) {
            return;
        }

        MainApplication.getInstance().sendKey((Line) MainApplication.getInstance().getCurrentSession(), number);
        mNumberView.setText(mNumberView.getText() + number);
    }

    private void deleteNumber() {
        if (mNumberView == null || mNumberView.getText().length() == 0) {
            return;
        }

        mNumberView.setText(mNumberView.getText().subSequence(0, mNumberView.getText().length() - 1));
    }

}
