package sg.today.service;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import sg.R;
import sg.today.MainApplication;
import sg.today.activity.IncomingActivity;
import sg.today.activity.MainActivity;
import sg.today.model.Profile;

/**
 * Created by Phong Do on 11/28/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "Firebase Message";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        String type;
        String chatRoom;

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());
            type = remoteMessage.getData().containsKey("type") ? remoteMessage.getData().get("type") : ""; // Chat or Notification
            chatRoom = remoteMessage.getData().containsKey("room_id") ? remoteMessage.getData().get("room_id") : "";
            if (type.equalsIgnoreCase("chat")) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("ACTION", 3);
                intent.putExtra("ROOMID", chatRoom);
                if (MainApplication.getActivityCounter() > 0) {
                    // Show chat screen
                    final String[] accounts = ContextHelper.getAccount(this);
                    final Profile profile = ContextHelper.getProfile(this);
                    ContextHelper.startChat(this, chatRoom, accounts[0], profile.firstName + " " + profile.lastName, profile.profileImage);
                }
                else {
                    ContextHelper.showChatNotification(this, intent, "You have a chat request.");
                }
                return;
            }

            Intent incomingIntent = new Intent(this, MainActivity.class);
            incomingIntent.putExtra(IncomingActivity.CALLER_DISPLAY_NAME, remoteMessage.getData().containsKey("SipName") ? remoteMessage.getData().get("SipName") : "");
            incomingIntent.putExtra(IncomingActivity.CALLER_NUMBER, remoteMessage.getData().containsKey("SipNumber") ? remoteMessage.getData().get("SipNumber") : "");
            incomingIntent.putExtra(IncomingActivity.CALL_IS_VIDEO, remoteMessage.getData().containsKey("IsVideoCall") ? remoteMessage.getData().get("IsVideoCall") : "true");
            incomingIntent.putExtra(IncomingActivity.CALL_SESSION_ID, 0);
            startActivity(incomingIntent);
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
//                try {
//                    Bundle info = new Bundle();
//                    info.putString("Caller", "");
//                    info.putString("CallerNumber", "");
//                    info.putBoolean("IsVideo", true);
//                    info.putLong("SessionId", -1);
//                    //MainApplication.getInstance().telecomManager.addNewIncomingCall(MainApplication.getInstance().phoneAccountHandle, info);
//                }
//                catch (Exception exc) {
//                }
//            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]
}
