package sg.today.model;

import android.graphics.Bitmap;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Phong Do on 11/17/2016.
 */

public class Profile {
    public String username;
    public String email;
    public String countryCode;
    public String cityCode;
    public String mobileNumber;
    public String firstName;
    public String lastName;
    public int meetPlan;
    public Date meetValidFrom;
    public Date meetValidTo;
    public int telegram;
    public int moxtra;
    public String sipDisplayName;
    public String sipRingtone;
    public String sipDid;
    public String sipConnectType;
    public String profileImage;
    public Bitmap profileImageBitmapData;

    public SipAccount[] sipAccounts;

    public Profile() {
    }

    public Profile(JSONObject json) {
        if (json == null) {
            return;
        }
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            email = json.getString("email");
            countryCode = json.getString("countrycode");
            cityCode = json.getString("citycode");
            mobileNumber = json.getString("mobile");
            firstName = json.getString("firstname");
            lastName = json.getString("lastname");
            meetPlan = json.getInt("meetplan");
            meetValidFrom = TextUtils.isEmpty(json.getString("meetvalidfrom")) ? null : dateFormatter.parse(json.getString("meetvalidfrom"));
            meetValidTo = TextUtils.isEmpty(json.getString("meetvalidto")) ? null : dateFormatter.parse(json.getString("meetvalidto"));
            telegram = json.getInt("telegram");
            moxtra = json.getInt("moxtra");
            sipDisplayName = json.getString("sip_displayname");
            sipRingtone = json.getString("sip_ringtone");
            sipDid = json.isNull("sip_did") ? "" : json.getString("sip_did");
            sipConnectType = json.getString("sip_connecttype");
            profileImage = json.optString("profileimage");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
