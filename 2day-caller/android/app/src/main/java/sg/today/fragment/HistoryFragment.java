package sg.today.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;

import sg.today.MainApplication;
import sg.R;
import sg.today.adapter.CallLogListAdapter;
import sg.today.model.CallLog;
import sg.today.model.Contact;
import sg.today.service.ContextHelper;
import sg.today.service.DatabaseAdapter;

public class HistoryFragment extends Fragment {

    private DatabaseAdapter mDatabase;
    public EditText mSearchEdit;
    private SwipeMenuListView mContactsListView;
    private ArrayList<CallLog> logsList = new ArrayList<>();
    private CallLogListAdapter adapter;
    private TabLayout tabLayout;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        mDatabase = new DatabaseAdapter(getActivity());
        mDatabase.open();

        mSearchEdit = (EditText) view.findViewById(R.id.search_contact);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);

        logsList = new ArrayList<>();
        mContactsListView = (SwipeMenuListView) view.findViewById(R.id.contacts_list);
        adapter = new CallLogListAdapter(getActivity(), logsList);
        mContactsListView.setAdapter(adapter);

        final SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem delete = new SwipeMenuItem(getActivity());
                delete.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.right_menu_background)));
                delete.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                delete.setIcon(R.drawable.remove);
                delete.setId(0);
                menu.addMenuItem(delete);
            }

            @Override
            public void createLeft(SwipeMenu menu) {
                SwipeMenuItem voice = new SwipeMenuItem(getActivity());
                voice.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.left_menu_background)));
                voice.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                voice.setIcon(R.drawable.small_action_call);
                voice.setId(1);
                menu.addMenuItem(voice);
                SwipeMenuItem video = new SwipeMenuItem(getActivity());
                video.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.left_menu_background)));
                video.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                video.setIcon(R.drawable.small_action_video);
                menu.addMenuItem(video);
//                SwipeMenuItem chat = new SwipeMenuItem(getActivity());
//                chat.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.left_menu_background)));
//                chat.setWidth(ContextHelper.dpToPx(getActivity(), 100));
//                chat.setIcon(R.drawable.small_action_chat);
//                menu.addMenuItem(chat);
            }
        };

        // set creator
        mContactsListView.setMenuCreator(creator);
        mContactsListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                if (menu.getMenuItem(0).getId() == 0) {
                    if (index == 0) {
                        ContextHelper.showGlobalDialog(getActivity(),
                                getString(R.string.delete_call_log),
                                getString(R.string.delete_call_log_message),
                                getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        CallLog log = adapter.getItem(position);
                                        mDatabase.deleteCallLog(log.Id);
                                        switch (tabLayout.getSelectedTabPosition()) {
                                            case 0:
                                                adapter.setCallLogsList(mDatabase.getAllCallLogs());
                                                break;
                                            case 1:
                                                adapter.setCallLogsList(mDatabase.getCallLogsByStatus("Missed"));
                                                break;
                                            case 2:
                                                adapter.setCallLogsList(mDatabase.getCallLogsByStatus("Incoming"));
                                                break;
                                            case 3:
                                                adapter.setCallLogsList(mDatabase.getCallLogsByStatus("Outgoing"));
                                                break;
                                        }
                                        adapter.getFilter().filter(mSearchEdit.getText(), new Filter.FilterListener() {
                                            @Override
                                            public void onFilterComplete(int i) {
                                            }
                                        });
                                    }
                                },
                                getString(R.string.no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                    }
                                });
                    }
                } else {
                    CallLog log = adapter.getItem(position);
                    ArrayList<Contact> contacts = new ArrayList<>();
                    String[] names = TextUtils.isEmpty(log.Name) ? new String[0] : log.Name.split(";");
                    String[] phones = log.Phone.split(";");
                    for (int i = 0; i < phones.length; i++) {
                        Contact contact = new Contact();
                        contact.Name = i < names.length ? names[i] : "Unknown";
                        contact.Phone = phones[i];
                        contact.Avatar = log.Avatar;
                        contact.Id = log.ContactId;
                        Contact dbContact = null;
                        if (phones.length == 1) {
                            dbContact = mDatabase.getContact(log.ContactId);
                        }
                        if (dbContact == null) {
                            ArrayList<Contact> dbContacts = mDatabase.searchContacts(phones[i]);
                            if (dbContacts.size() > 0) {
                                dbContact = dbContacts.get(0);
                            }
                        }

                        if ((index > 0) && dbContact != null && !TextUtils.isEmpty(dbContact.SipAccount)) {
                            contacts.add(dbContact);
                        }
                        else if (index == 0) {
                            contacts.add(contact);
                        }
                    }
                    if (index == 0) {
                        // Call
                        if (!MainApplication.getInstance().isOnline()) {
                            ContextHelper.showAlert(getActivity(), "You Are Not Connected", getString(R.string.sip_not_connected));
                            return false;
                        }
                        if (contacts.size() == 1) {
                            MainApplication.getInstance().call(contacts.get(0), false, true);
                        }
                        else {
                            MainApplication.getInstance().conference(contacts, false, true);
                        }
                    } else if (index == 1) {
                        // Video
                        if (contacts.size() == 0) {
                            ContextHelper.showTipMessage(getActivity(), getString(R.string.error_video_call_non_sip_user));
                            return false;
                        }
                        if (!MainApplication.getInstance().isOnline()) {
                            ContextHelper.showAlert(getActivity(), "You Are Not Connected", getString(R.string.sip_not_connected));
                            return false;
                        }

                        if (contacts.size() == 1) {
                            MainApplication.getInstance().call(contacts.get(0), true, true);
                        }
                        else {
                            MainApplication.getInstance().conference(contacts, true, true);
                        }
                    } else if (index == 2) {
                        // Chat
                        if (contacts.size() == 0) {
                            ContextHelper.showTipMessage(getActivity(), getString(R.string.error_chat_non_sip_user));
                            return false;
                        }
                        ContextHelper.startChat(getActivity(), contacts);
                    }
                }
                return false;
            }
        });
        mContactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (!MainApplication.getInstance().isOnline()) {
                    ContextHelper.showAlert(getActivity(), "You Are Not Connected", getString(R.string.sip_not_connected));
                    return;
                }

                CallLog log = adapter.getItem(position);
                ArrayList<Contact> contacts = new ArrayList<>();
                String[] names = TextUtils.isEmpty(log.Name) ? new String[0] : log.Name.split(";");
                String[] phones = log.Phone.split(";");
                for (int i = 0; i < phones.length; i++) {
                    Contact contact = new Contact();
                    contact.Name = i < names.length ? names[i] : "Unknown";
                    contact.Phone = phones[i];
                    contact.Avatar = log.Avatar;
                    contact.Id = log.ContactId;
                    contacts.add(contact);
                }
                if (contacts.size() == 1) {
                    MainApplication.getInstance().call(contacts.get(0), false, true);
                }
                else {
                    MainApplication.getInstance().conference(contacts, false, true);
                }
            }
        });

        mSearchEdit.clearFocus();
        mSearchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_SEARCH) {
                    ContextHelper.hideKeyboard(getActivity());
                    return true;
                }
                return false;
            }
        });
        mSearchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter == null) {
                    return;
                }
                adapter.getFilter().filter(charSequence, new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int i) {
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        adapter.setCallLogsList(mDatabase.getAllCallLogs());
                        break;
                    case 1:
                        adapter.setCallLogsList(mDatabase.getCallLogsByStatus("Missed"));
                        break;
                    case 2:
                        adapter.setCallLogsList(mDatabase.getCallLogsByStatus("Incoming"));
                        break;
                    case 3:
                        adapter.setCallLogsList(mDatabase.getCallLogsByStatus("Outgoing"));
                        break;
                }
                adapter.getFilter().filter(mSearchEdit.getText(), new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int i) {
                    }
                });
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tabLayout.getTabAt(0).select();

        adapter.setCallLogsList(mDatabase.getAllCallLogs());
        adapter.notifyDataSetChanged();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mDatabase == null) {
            mDatabase = new DatabaseAdapter(getActivity());
            mDatabase.open();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mDatabase != null) {
            mDatabase.close();
        }
        mDatabase = null;
    }

    public void clear() {
        switch (tabLayout.getSelectedTabPosition()) {
            case 0:
                mDatabase.clearCallLogs("");
                adapter.setCallLogsList(mDatabase.getAllCallLogs());
                break;
            case 1:
                mDatabase.clearCallLogs("Missed");
                adapter.setCallLogsList(mDatabase.getCallLogsByStatus("Missed"));
                break;
            case 2:
                mDatabase.clearCallLogs("Incoming");
                adapter.setCallLogsList(mDatabase.getCallLogsByStatus("Incoming"));
                break;
            case 3:
                mDatabase.clearCallLogs("Outgoing");
                adapter.setCallLogsList(mDatabase.getCallLogsByStatus("Outgoing"));
                break;
        }
        adapter.getFilter().filter(mSearchEdit.getText(), new Filter.FilterListener() {
            @Override
            public void onFilterComplete(int i) {
            }
        });
    }
}
