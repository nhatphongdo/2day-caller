package sg.today.service;

import org.json.JSONObject;

/**
 * Created by Phong Do on 11/16/2016.
 */

public interface JsonObjectResultInterface {

    void onResponse(JSONObject response);

    void onErrorResponse(String error);

}
