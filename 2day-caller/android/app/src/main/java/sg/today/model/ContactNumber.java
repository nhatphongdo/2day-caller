package sg.today.model;

import android.database.Cursor;

/**
 * Created by Phong Do on 11/20/2016.
 */

public class ContactNumber {
    public long Id;
    public long ContactId;
    public String Number;

    public ContactNumber() {
    }

    public ContactNumber(Cursor c) {
        Id = c.getLong(0);
        ContactId = c.getLong(1);
        Number = c.getString(2);
    }

}
