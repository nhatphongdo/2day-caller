package sg.today.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.baoyz.swipemenulistview.BaseSwipeListAdapter;
import com.baoyz.swipemenulistview.ContentViewWrapper;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import sg.R;
import sg.today.model.CallLog;
import sg.today.model.Contact;
import sg.today.service.ContextHelper;

/**
 * Created by Phong Do on 11/23/2016.
 */

public class CallLogListAdapter extends BaseSwipeListAdapter implements Filterable {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<CallLog> callLogsList = new ArrayList<>();
    private ArrayList<CallLog> filterList = new ArrayList<>();

    public CallLogListAdapter(Context mContext, ArrayList<CallLog> callLogsList) {
        this.mContext = mContext;
        this.callLogsList = callLogsList;
        this.filterList = callLogsList;
        this.mInflater = LayoutInflater.from(mContext);
    }

    public void setCallLogsList(ArrayList<CallLog> callLogs) {
        this.callLogsList = callLogs;
        this.filterList = callLogs;
    }

    @Override
    public ContentViewWrapper getViewAndReusable(int position, View view, ViewGroup parent) {
        boolean reUsable = true;
        final CallLog log = getItem(position);

        if (view == null) {
            view = mInflater.inflate(R.layout.view_item_call_log, null);
            reUsable = false;
        }

        CircularImageView avatar = (CircularImageView) view.findViewById(R.id.avatar);
        TextView callName = (TextView) view.findViewById(R.id.contact_name);
        TextView callNumber = (TextView) view.findViewById(R.id.contact_number);
        TextView callTime = (TextView) view.findViewById(R.id.log_time);
        TextView callStatus = (TextView) view.findViewById(R.id.log_status);

        callName.setText(TextUtils.isEmpty(log.Name) ? log.Phone : log.Name);
        callNumber.setText(log.Phone);
        CharSequence time = DateUtils.getRelativeTimeSpanString(log.Timer.getTime(), (new Date()).getTime(), DateUtils.MINUTE_IN_MILLIS);
        callTime.setText(time.toString());
        callStatus.setText(log.Status);

        Contact contact = new Contact();
        contact.Name = log.Name;
        contact.Phone = log.Phone;
        ContextHelper.loadAvatar(mContext, contact, log.Phone, avatar);

        return new ContentViewWrapper(view, reUsable);
    }

    @Override
    public int getCount() {
        return filterList.size();
    }

    @Override
    public CallLog getItem(int i) {
        return filterList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return filterList.get(i).Id;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    results.values = callLogsList;
                    results.count = callLogsList.size();
                } else {
                    ArrayList<CallLog> list = new ArrayList<>();
                    for (int i = 0; i < callLogsList.size(); i++) {
                        if ((TextUtils.isEmpty(callLogsList.get(i).Name) && callLogsList.get(i).Phone.toLowerCase().indexOf(constraint.toString().toLowerCase()) >= 0)
                        || (!TextUtils.isEmpty(callLogsList.get(i).Name) && callLogsList.get(i).Name.toLowerCase().indexOf(constraint.toString().toLowerCase()) >= 0)) {
                            list.add(callLogsList.get(i));
                        }
                    }
                    results.values = list;
                    results.count = list.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterList = (ArrayList<CallLog>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
