package sg.today.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.portsip.PortSipErrorcode;
import com.portsip.PortSipSdk;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import sg.today.AndroidBug5497Workaround;
import sg.today.MainApplication;
import sg.R;
import sg.today.model.Contact;
import sg.today.service.ContextHelper;
import sg.today.service.DatabaseAdapter;
import sg.today.service.OnContactSelectionListener;
import sg.today.util.Line;
import sg.today.util.Session;

public class CallingActivity extends BaseActivity implements Observer, OnContactSelectionListener {

    public final static String IS_VIDEO_CALL = "IS_VIDEO_CALL";
    public final static String CALLER_DISPLAY_NAME = "CALLER_DISPLAY_NAME";
    public final static String CALLER_NUMBER = "CALLER_NUMBER";
    public final static String CALLER_CONTACT_ID = "CALLER_CONTACT_ID";
    public final static String START_TIMER = "START_TIMER";
    public final static String RESET_TIMER = "RESET_TIMER";
    public final static String BRING_FRONT = "BRING_FRONT";
    public final static String TIMER = "TIMER";

    private MainApplication mMainApplication;
    private DatabaseAdapter mDatabase;

    private View mVoiceCallView;
    private View mInCallContacts;
    private CircularImageView mContact01;
    private CircularImageView mContact02;
    private CircularImageView mContact03;
    private CircularImageView mContact04;
    private CircularImageView mContact05;
    private CircularImageView mContact06;
    private CircularImageView mContact07;
    private CircularImageView mContact08;
    private CircularImageView mContact09;
    private CircularImageView mContact10;
    private TextView mCalleeName;
    private TextView mCalleeNumber;
    private TextView mCallTime;
    private CircularImageView mCallerImage;
    private ImageButton mMuteButton;
    private ImageButton mSpeakerButton;
    private ImageButton mKeypadButton;
    private ImageButton mAddPersonButton;
    private ImageButton mEndCallButton;

    // For video call
    private View mVideoCallView;
    private LinearLayout mRemoteView;
    private TextView mVideoCallContacts;
    private TextView mVideoCallInfo;
    private TextView mVideoCallTime;
    private ImageButton mChangeCameraButton;
    private LinearLayout mVideoPerson01;
    private ImageButton mVideoCameraButton;
    private ImageButton mVideoSpeakerButton;
    private ImageButton mVideoMuteButton;
    private ImageButton mVideoKeypadButton;
    private ImageButton mVideoAddPersonButton;
    private ImageButton mVideoEndCallButton;
    private SurfaceView remoteSurfaceView = null;
    private SurfaceView localSurfaceView = null;

    // Keypad view
    private View mKeypadView;
    private TextView mKeypadCountryCode;
    private TextView mKeypadNumber;
    private Button mDeleteButton;
    private ImageButton mButton1;
    private ImageButton mButton2;
    private ImageButton mButton3;
    private ImageButton mButton4;
    private ImageButton mButton5;
    private ImageButton mButton6;
    private ImageButton mButton7;
    private ImageButton mButton8;
    private ImageButton mButton9;
    private ImageButton mButton0;
    private ImageButton mButtonSharp;
    private ImageButton mButtonStar;
    private ImageButton mButtonAddCall;
    private Button mHideButton;

    private static boolean useFrontCamera = true;
    private boolean mIsVideoCall;
    private String mCallerDisplayName;
    private String mCallerNumber;
    private long mCallerContactId;

    private ArrayList<Contact> mConferenceContacts = new ArrayList<>();

    private OrientationEventListener mListener;

    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;

    public long startTime = 0;
    final Handler timerHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            long millis = System.currentTimeMillis() - startTime;
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;

            if (mCallTime != null) {
                mCallTime.setText(String.format("%02d:%02d", minutes, seconds));
            }
            if (mVideoCallTime != null) {
                mVideoCallTime.setText(String.format("%02d:%02d", minutes, seconds));
            }
            return false;
        }
    });

    @Override
    public void OnContactSelection(ArrayList<Contact> contacts) {
        if (mIsVideoCall) {
            if (contacts.size() + mConferenceContacts.size() > 5) {
                ContextHelper.showTipMessage(getContext(), getString(R.string.maximum_people_video));
                return;
            }
        } else {
            if (contacts.size() + mConferenceContacts.size() > 20) {
                ContextHelper.showTipMessage(getContext(), getString(R.string.maximum_people_voice));
                return;
            }
        }

        // Start a conference call
        if (MainApplication.getInstance().isConference()) {
            // Already has conference, just add a call
        } else {
            MainApplication.getInstance().startConference(mIsVideoCall);
        }

        // Add call
        for (int i = 0; i < contacts.size(); i++) {
            MainApplication.getInstance().addToConference(contacts.get(i), mIsVideoCall);
            mConferenceContacts.add(contacts.get(i));
        }

        updateContactList();
    }

    private class CountTimerTask extends TimerTask {
        @Override
        public void run() {
            timerHandler.sendEmptyMessage(0);
        }
    }

    private Timer timer = new Timer();

    public long sessionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calling);

        AndroidBug5497Workaround.assistActivity(this);

        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, getLocalClassName());
        if (wakeLock != null && !wakeLock.isHeld()) {
            wakeLock.acquire();
        }

        mIsVideoCall = getIntent().getBooleanExtra(IS_VIDEO_CALL, false);
        mCallerNumber = getIntent().getStringExtra(CALLER_NUMBER);
        if (mCallerNumber.startsWith("sip:")) {
            mCallerNumber = mCallerNumber.substring(4, mCallerNumber.indexOf("@"));
        }
        mCallerDisplayName = getIntent().getStringExtra(CALLER_DISPLAY_NAME);
        if (mCallerDisplayName == null || mCallerDisplayName.length() == 0 || mCallerDisplayName.equalsIgnoreCase("null")) {
            //mCallerDisplayName = getString(R.string.unknown);
            mCallerDisplayName = mCallerNumber;
        }
        mCallerContactId = getIntent().getLongExtra(CALLER_CONTACT_ID, 0);

        mDatabase = new DatabaseAdapter(getContext());
        mDatabase.open();

        Contact contact = mDatabase.getContact(mCallerContactId);
        if (contact == null && !mCallerNumber.contains(";")) {
            ArrayList<Contact> contacts = mDatabase.searchContacts(mCallerNumber);
            if (contacts.size() > 0) {
                contact = contacts.get(0);
            }
        }

        mMainApplication = MainApplication.getInstance();
        mMainApplication.setCallingActivity(this);

        sessionId = mMainApplication.getCurrentSession().getSessionId();

        Log.e("Calling", String.valueOf(mIsVideoCall) + ", " + mCallerDisplayName + ", " + mCallerNumber + ", " + sessionId);

        mVoiceCallView = findViewById(R.id.voice_call_view);
        mInCallContacts = findViewById(R.id.call_contacts);
        mContact01 = (CircularImageView) findViewById(R.id.contact_01);
        mContact02 = (CircularImageView) findViewById(R.id.contact_02);
        mContact03 = (CircularImageView) findViewById(R.id.contact_03);
        mContact04 = (CircularImageView) findViewById(R.id.contact_04);
        mContact05 = (CircularImageView) findViewById(R.id.contact_05);
        mContact06 = (CircularImageView) findViewById(R.id.contact_06);
        mContact07 = (CircularImageView) findViewById(R.id.contact_07);
        mContact08 = (CircularImageView) findViewById(R.id.contact_08);
        mContact09 = (CircularImageView) findViewById(R.id.contact_09);
        mContact10 = (CircularImageView) findViewById(R.id.contact_10);
        mCalleeName = (TextView) findViewById(R.id.callee_name);
        mCalleeNumber = (TextView) findViewById(R.id.callee_number);
        mCallTime = (TextView) findViewById(R.id.call_time);
        mCallerImage = (CircularImageView) findViewById(R.id.caller_image);
        mMuteButton = (ImageButton) findViewById(R.id.mute_button);
        mSpeakerButton = (ImageButton) findViewById(R.id.speaker_button);
        mKeypadButton = (ImageButton) findViewById(R.id.keypad_button);
        mAddPersonButton = (ImageButton) findViewById(R.id.add_button);
        mEndCallButton = (ImageButton) findViewById(R.id.end_call_button);

        mVideoCallView = findViewById(R.id.video_call_view);
        mRemoteView = (LinearLayout) findViewById(R.id.remote_view);
        mVideoCallContacts = (TextView) findViewById(R.id.video_call_contacts);
        mVideoCallInfo = (TextView) findViewById(R.id.video_call_info);
        mVideoCallTime = (TextView) findViewById(R.id.video_call_time);
        mChangeCameraButton = (ImageButton) findViewById(R.id.change_camera_button);
        mVideoPerson01 = (LinearLayout) findViewById(R.id.video_person_01);
        mVideoCameraButton = (ImageButton) findViewById(R.id.video_camera_button);
        mVideoSpeakerButton = (ImageButton) findViewById(R.id.video_speaker_button);
        mVideoMuteButton = (ImageButton) findViewById(R.id.video_mute_button);
        mVideoKeypadButton = (ImageButton) findViewById(R.id.video_keypad_button);
        mVideoAddPersonButton = (ImageButton) findViewById(R.id.video_add_button);
        mVideoEndCallButton = (ImageButton) findViewById(R.id.video_end_button);

        mKeypadView = findViewById(R.id.keypad_view);
        mKeypadCountryCode = (TextView) findViewById(R.id.number_country_code);
        mKeypadNumber = (TextView) findViewById(R.id.call_number);
        mDeleteButton = (Button) findViewById(R.id.delete_button);
        mButton0 = (ImageButton) findViewById(R.id.number_0);
        mButton1 = (ImageButton) findViewById(R.id.number_1);
        mButton2 = (ImageButton) findViewById(R.id.number_2);
        mButton3 = (ImageButton) findViewById(R.id.number_3);
        mButton4 = (ImageButton) findViewById(R.id.number_4);
        mButton5 = (ImageButton) findViewById(R.id.number_5);
        mButton6 = (ImageButton) findViewById(R.id.number_6);
        mButton7 = (ImageButton) findViewById(R.id.number_7);
        mButton8 = (ImageButton) findViewById(R.id.number_8);
        mButton9 = (ImageButton) findViewById(R.id.number_9);
        mButtonSharp = (ImageButton) findViewById(R.id.number_sharp);
        mButtonStar = (ImageButton) findViewById(R.id.number_star);
        mButtonAddCall = (ImageButton) findViewById(R.id.add_call_button);
        mHideButton = (Button) findViewById(R.id.hide_button);

        if (mCallerNumber.contains(";")) {
            // Conference
            if (contact != null) {
                mCalleeName.setText(contact.Name);
                mCalleeNumber.setText(contact.Phone);
                mVideoCallContacts.setText(mCallerDisplayName);
                mVideoCallInfo.setText(mCallerNumber.split(";").length + " in call");
            }
        } else {
            if (contact != null) {
                mCalleeName.setText(contact.Name);
                mCalleeNumber.setText(mCallerNumber);
                mVideoCallContacts.setText(contact.Name);
                mVideoCallInfo.setText("1 in call");
            } else {
                mCalleeName.setText(mCallerDisplayName);
                mCalleeNumber.setText(mCallerNumber);
                mVideoCallContacts.setText(mCallerDisplayName);
                mVideoCallInfo.setText("1 in call");
            }
        }

        ContextHelper.loadAvatar(getContext(), contact, mCallerDisplayName, mCallerImage);

        if (mIsVideoCall) {
            mVideoCallView.setVisibility(View.VISIBLE);
            mVoiceCallView.setVisibility(View.GONE);

            localSurfaceView = mMainApplication.getLocalSurfaceView();
            remoteSurfaceView = mMainApplication.getRemoteSurfaceView();

            mRemoteView.addView(remoteSurfaceView);
            mVideoPerson01.addView(localSurfaceView);

            initCamera(mMainApplication.getPortSIPSDK());// set default camera

            updateVideo();

            mListener = new OrientationEventListener(getContext(), SensorManager.SENSOR_DELAY_NORMAL) {
                @Override
                public void onOrientationChanged(int orientation) {
                    try {
                        if ((orientation > 345 || orientation < 15) ||
                                (orientation > 75 && orientation < 105) ||
                                (orientation > 165 && orientation < 195) ||
                                (orientation > 255 && orientation < 285)) {

                            int cameraId = useFrontCamera ? 1 : 0;
                            if (orientation == ORIENTATION_UNKNOWN) return;
                            android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
                            android.hardware.Camera.getCameraInfo(cameraId, info);
                            orientation = (orientation + 45) / 90 * 90;

                            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                                orientation = (info.orientation - orientation + 360) % 360;
                            } else {  // back-facing camera
                                orientation = (info.orientation + orientation) % 360;
                            }
                            mMainApplication.getPortSIPSDK().setVideoOrientation(orientation);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
        } else {
            mVideoCallView.setVisibility(View.GONE);
            mVoiceCallView.setVisibility(View.VISIBLE);
        }

        if (mCallerNumber.contains(";")) {
            String[] names = mCallerDisplayName.split(";");
            String[] numbers = mCallerNumber.split(";");
            for (int i = 0; i < numbers.length; i++) {
                Contact person = new Contact();
                person.Phone = numbers[i];
                if (i < names.length) {
                    person.Name = names[i];
                } else {
                    person.Name = numbers[i];
                }
                mConferenceContacts.add(person);
            }
            updateContactList();
        } else {
            if (contact == null) {
                contact = new Contact();
                contact.Phone = mCallerNumber;
                contact.Name = mCallerDisplayName;
                mConferenceContacts.add(contact);
            } else {
                mConferenceContacts.add(contact);
            }
        }

        if (ContextHelper.getLocalMode(getContext())) {
            String[] countryCodes = ContextHelper.getCountryCodes(getContext());
            mKeypadCountryCode.setText(String.format("+%s", countryCodes[1]));
        } else {
            mKeypadCountryCode.setText("");
        }

        mMuteButton.setTag(1);
        mMuteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((int) mMuteButton.getTag() == 1) {
                    mMainApplication.mute(mMainApplication.findLineBySessionId(sessionId), true);
                    //mMuteButton.setText(getString(R.string.unmute));
                    //mMuteButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getContext(), R.drawable.mute_selected), null, null);
                    mMuteButton.setImageResource(R.drawable.mute_selected);
                    mMuteButton.setTag(2);
                } else {
                    mMainApplication.mute(mMainApplication.findLineBySessionId(sessionId), false);
                    //mMuteButton.setText(getString(R.string.mute));
                    //mMuteButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getContext(), R.drawable.mute), null, null);
                    mMuteButton.setImageResource(R.drawable.mute);
                    mMuteButton.setTag(1);
                }
            }
        });
        mSpeakerButton.setTag(1);
        mSpeakerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((int) mSpeakerButton.getTag() == 1) {
                    mMainApplication.speaker(true);
                    //mSpeakerButton.setText(getString(R.string.ear_phone));
                    //mSpeakerButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getContext(), R.drawable.speaker_selected), null, null);
                    mSpeakerButton.setImageResource(R.drawable.speaker_selected);
                    mSpeakerButton.setTag(2);
                } else {
                    mMainApplication.speaker(false);
                    //mSpeakerButton.setText(getString(R.string.speaker));
                    //mSpeakerButton.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getContext(), R.drawable.speaker), null, null);
                    mSpeakerButton.setImageResource(R.drawable.speaker);
                    mSpeakerButton.setTag(1);
                }
            }
        });
        mKeypadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mKeypadView.setVisibility(View.VISIBLE);
            }
        });
        mAddPersonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainApplication.getInstance().setContactSelectionListener(getContext());
                Intent contactSelectorIntent = new Intent(getContext(), ContactsSelectorActivity.class);
                contactSelectorIntent.putExtra(ContactsSelectorActivity.VIDEO_CALL, false);
                contactSelectorIntent.putExtra(ContactsSelectorActivity.ONLY_SHOW_2DAY_ACCOUNTS, false);
                ContextHelper.startActivityAsFront(getContext(), contactSelectorIntent);
            }
        });
        mEndCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMainApplication.endSessionCall(mMainApplication.findLineBySessionId(sessionId));
                finish();
            }
        });

        mChangeCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchCamera(mMainApplication.getPortSIPSDK());
            }
        });
        mVideoCameraButton.setTag(1);
        mVideoCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((int) mVideoCameraButton.getTag() == 1) {
                    mVideoCameraButton.setTag(2);
                    mVideoCameraButton.setImageResource(R.drawable.video_on);
                    mMainApplication.muteVideo(mMainApplication.findLineBySessionId(sessionId), true, (int) mVideoMuteButton.getTag() == 1 ? false : true);
                } else {
                    mVideoCameraButton.setTag(1);
                    mVideoCameraButton.setImageResource(R.drawable.video_on_selected);
                    mMainApplication.muteVideo(mMainApplication.findLineBySessionId(sessionId), false, (int) mVideoMuteButton.getTag() == 1 ? false : true);
                }
            }
        });
        mVideoSpeakerButton.setTag(1);
        mVideoSpeakerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((int) mVideoSpeakerButton.getTag() == 1) {
                    mVideoSpeakerButton.setTag(2);
                    mMainApplication.speaker(true);
                    mVideoSpeakerButton.setImageResource(R.drawable.speaker_selected);
                } else {
                    mVideoSpeakerButton.setTag(1);
                    mMainApplication.speaker(false);
                    mVideoSpeakerButton.setImageResource(R.drawable.speaker);
                }
            }
        });
        mVideoMuteButton.setTag(1);
        mVideoMuteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((int) mVideoMuteButton.getTag() == 1) {
                    mMainApplication.mute(mMainApplication.findLineBySessionId(sessionId), true);
                    mVideoMuteButton.setTag(2);
                    mVideoMuteButton.setImageResource(R.drawable.mute_selected);
                } else {
                    mMainApplication.mute(mMainApplication.findLineBySessionId(sessionId), false);
                    mVideoMuteButton.setTag(1);
                    mVideoMuteButton.setImageResource(R.drawable.mute);
                }
            }
        });
        mVideoKeypadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mKeypadView.setVisibility(View.VISIBLE);
            }
        });
        mVideoAddPersonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainApplication.getInstance().setContactSelectionListener(getContext());
                Intent contactSelectorIntent = new Intent(getContext(), ContactsSelectorActivity.class);
                contactSelectorIntent.putExtra(ContactsSelectorActivity.VIDEO_CALL, true);
                contactSelectorIntent.putExtra(ContactsSelectorActivity.ONLY_SHOW_2DAY_ACCOUNTS, true);
                ContextHelper.startActivityAsFront(getContext(), contactSelectorIntent);
            }
        });
        mVideoEndCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMainApplication.endSessionCall(mMainApplication.findLineBySessionId(sessionId));
                finish();
            }
        });

        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteNumber();
            }
        });
        mDeleteButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mKeypadNumber != null) {
                    mKeypadNumber.setText("");
                }
                return false;
            }
        });

        final ImageButton[] buttons = new ImageButton[]{
                mButton1, mButton2, mButton3,
                mButton4, mButton5, mButton6,
                mButton7, mButton8, mButton9,
                mButtonStar, mButton0, mButtonSharp
        };
        final int[] buttonsDrawable = new int[]{
                R.drawable.key_1, R.drawable.key_2, R.drawable.key_3,
                R.drawable.key_4, R.drawable.key_5, R.drawable.key_6,
                R.drawable.key_7, R.drawable.key_8, R.drawable.key_9,
                R.drawable.key_star, R.drawable.key_0, R.drawable.key_sharp,
        };
        for (int i = 0; i < buttons.length; i++) {
            final int index = i;
            buttons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addNumber(buttons[index].getTag().toString());
                }
            });
        }
        mButton0.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                addNumber("+");
                return true;
            }
        });
        mButtonAddCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIsVideoCall) {
                    if (mConferenceContacts.size() >= 5) {
                        ContextHelper.showTipMessage(getContext(), getString(R.string.maximum_people_video));
                        return;
                    }
                } else {
                    if (mConferenceContacts.size() >= 20) {
                        ContextHelper.showTipMessage(getContext(), getString(R.string.maximum_people_voice));
                        return;
                    }
                }

                // Start a conference call
                if (MainApplication.getInstance().isConference()) {
                    // Already has conference, just add a call
                } else {
                    MainApplication.getInstance().startConference(mIsVideoCall);
                }

                // Add call
                Contact contact = new Contact();
                contact.Phone = mKeypadNumber.getText().toString();
                MainApplication.getInstance().addToConference(contact, mIsVideoCall);

                // Hide keypad
                mKeypadNumber.setText("");
                mKeypadView.setVisibility(View.GONE);

                mConferenceContacts.add(contact);
                updateContactList();
            }
        });
        mHideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mKeypadView.setVisibility(View.GONE);
            }
        });

        if (getIntent().getBooleanExtra(START_TIMER, false)) {
            if (getIntent().getBooleanExtra(RESET_TIMER, false) == false) {
                startTime = getIntent().getLongExtra(TIMER, System.currentTimeMillis());
            }
            startCallTimer(getIntent().getBooleanExtra(RESET_TIMER, false));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mIsVideoCall) {
            mRemoteView.removeView(remoteSurfaceView);
            mVideoPerson01.removeView(localSurfaceView);
        }

        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
        }

        //mMainApplication.endSessionCall(mMainApplication.findLineBySessionId(sessionId));
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mIsVideoCall) {
            if (mRemoteView.getChildCount() == 0) {
                mRemoteView.addView(remoteSurfaceView);
            }
            if (mVideoPerson01.getChildCount() == 0) {
                mVideoPerson01.addView(localSurfaceView);
            }
            updateVideo();
            final Session cur = mMainApplication.getCurrentSession();
            if (cur != null) {
                cur.addObserver(this);
                remoteSurfaceView.post(new Runnable() {
                    @Override
                    public void run() {
                        cur.setVideosizeChanged();
                        cur.notifyObservers();
                    }
                });
            }
        }

        if (mListener != null && mListener.canDetectOrientation()) {
            mListener.enable();
        }

        if (getIntent().getBooleanExtra(START_TIMER, false)) {
            if (getIntent().getBooleanExtra(RESET_TIMER, false) == false) {
                startTime = getIntent().getLongExtra(TIMER, System.currentTimeMillis());
            }
            startCallTimer(getIntent().getBooleanExtra(RESET_TIMER, false));
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mIsVideoCall) {
            if (mListener != null && mListener.canDetectOrientation()) {
                mListener.disable();
            }

            Session cur = mMainApplication.getCurrentSession();
            if (cur != null) {
                cur.deleteObserver(this);
            }

            if (cur != null && cur.getSessionState() && cur.getSessionId() != PortSipErrorcode.INVALID_SESSION_ID) {
                mMainApplication.getPortSIPSDK().displayLocalVideo(false); // do not display
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mIsVideoCall) {
            mRemoteView.removeView(remoteSurfaceView);
            mVideoPerson01.removeView(localSurfaceView);
        }
    }

    private CallingActivity getContext() {
        return this;
    }

    @Override
    public void onBackPressed() {
        // Disable back button
        return;
    }

    @Override
    public void update(Observable observable, Object o) {
        final Session cur = mMainApplication.getCurrentSession();
        if (remoteSurfaceView != null && cur != null) {
            getContext().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    float rlWidht = mRemoteView.getWidth();
                    float rlHeight = mRemoteView.getHeight();
                    float height = cur.getVideoHeight();
                    float width = cur.getVideoWidth();
                    LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) remoteSurfaceView.getLayoutParams();
                    float rate = rlHeight / height < rlWidht / width ? rlHeight / height : rlWidht / width;
                    lp.height = (int) (height * rate);
                    lp.width = (int) (width * rate);
                    lp.gravity = Gravity.CENTER;

                    remoteSurfaceView.setLayoutParams(lp);
                }
            });
        }
    }

    public void updateStatus(String message) {
    }

    public void startCallTimer(boolean reset) {
        if (reset) {
            startTime = System.currentTimeMillis();
        }
        timer = new Timer();
        timer.schedule(new CountTimerTask(), 0, 1000);
    }

    public void stopCallTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
    }

    public void setCallStatus(String message) {
        mCallTime.setText(message);
    }

    private void initCamera(PortSipSdk sipSdk) {
        if (useFrontCamera) {
            sipSdk.setVideoOrientation(270);
            sipSdk.setVideoDeviceId(1);

        } else {
            sipSdk.setVideoOrientation(90);
            sipSdk.setVideoDeviceId(0);
        } // display remote video
    }

    private void switchCamera(PortSipSdk sipSdk) {
        useFrontCamera = !useFrontCamera;
        sipSdk.displayLocalVideo(false);
        if (useFrontCamera) {
            sipSdk.setVideoDeviceId(1);
            sipSdk.setVideoOrientation(270);
        } else {
            sipSdk.setVideoDeviceId(0);
            sipSdk.setVideoOrientation(90);
        }
        sipSdk.displayLocalVideo(true);
    }

    public void updateVideo() {
        if (!mIsVideoCall) {
            return;
        }

        if (mMainApplication.isConference()) {
            mMainApplication.getPortSIPSDK().setConferenceVideoWindow(remoteSurfaceView);
            return;
        }

        Session cur = mMainApplication.getCurrentSession();
        if (cur != null
                //&& cur.getSessionState()
                && cur.getSessionId() != PortSipErrorcode.INVALID_SESSION_ID
                && cur.getVideoState()) {

            mMainApplication.getPortSIPSDK().displayLocalVideo(true); // display Local video
            mMainApplication.getPortSIPSDK().setRemoteVideoWindow(cur.getSessionId(), remoteSurfaceView);
            mMainApplication.getPortSIPSDK().sendVideo(cur.getSessionId(), true);
        } else {
            mMainApplication.getPortSIPSDK().displayLocalVideo(false);
        }
    }

    private void addNumber(String number) {
        if (mKeypadNumber == null) {
            return;
        }

        MainApplication.getInstance().sendKey((Line) MainApplication.getInstance().getCurrentSession(), number);
        mKeypadNumber.setText(mKeypadNumber.getText() + number);
    }

    private void deleteNumber() {
        if (mKeypadNumber == null || mKeypadNumber.getText().length() == 0) {
            return;
        }

        mKeypadNumber.setText(mKeypadNumber.getText().subSequence(0, mKeypadNumber.getText().length() - 1));
    }

    private void updateContactList() {
        if (mConferenceContacts.size() < 2) {
            mInCallContacts.setVisibility(View.GONE);
            return;
        }

        if (mIsVideoCall) {
            String videoContacts = "";
            for (int i = 0; i < mConferenceContacts.size(); i++) {
                if (mConferenceContacts.get(i).Name == null || mConferenceContacts.get(i).Name.length() == 0 ||
                        mConferenceContacts.get(i).Name.equalsIgnoreCase("null")) {
                    videoContacts += mConferenceContacts.get(i).Phone;
                } else {
                    videoContacts += mConferenceContacts.get(i).Name;
                }

                if (i < mConferenceContacts.size() - 1) {
                    videoContacts += ", ";
                }
            }
            mVideoCallContacts.setText(videoContacts);
            mVideoCallInfo.setText(String.valueOf(mConferenceContacts.size()) + " in call");
        } else {
            mInCallContacts.setVisibility(View.VISIBLE);
            if (mConferenceContacts.size() >= 1) {
                mContact01.setVisibility(View.VISIBLE);
                ContextHelper.loadAvatar(getContext(), mConferenceContacts.get(0), mConferenceContacts.get(0).Phone, mContact01);
                //mContact01.setText(mConferenceContacts.get(0).Phone);
            } else {
                mContact01.setVisibility(View.GONE);
            }

            if (mConferenceContacts.size() >= 2) {
                mContact02.setVisibility(View.VISIBLE);
                ContextHelper.loadAvatar(getContext(), mConferenceContacts.get(1), mConferenceContacts.get(1).Phone, mContact02);
                //mContact02.setText(mConferenceContacts.get(1).Phone);
            } else {
                mContact02.setVisibility(View.GONE);
            }

            if (mConferenceContacts.size() >= 3) {
                mContact03.setVisibility(View.VISIBLE);
                ContextHelper.loadAvatar(getContext(), mConferenceContacts.get(2), mConferenceContacts.get(2).Phone, mContact03);
                //mContact03.setText(mConferenceContacts.get(2).Phone);
            } else {
                mContact03.setVisibility(View.GONE);
            }

            if (mConferenceContacts.size() >= 4) {
                mContact04.setVisibility(View.VISIBLE);
                ContextHelper.loadAvatar(getContext(), mConferenceContacts.get(3), mConferenceContacts.get(3).Phone, mContact04);
                //mContact04.setText(mConferenceContacts.get(3).Phone);
            } else {
                mContact04.setVisibility(View.GONE);
            }

            if (mConferenceContacts.size() >= 5) {
                mContact05.setVisibility(View.VISIBLE);
                ContextHelper.loadAvatar(getContext(), mConferenceContacts.get(4), mConferenceContacts.get(4).Phone, mContact05);
                //mContact05.setText(mConferenceContacts.get(4).Phone);
            } else {
                mContact05.setVisibility(View.GONE);
            }

            if (mConferenceContacts.size() >= 6) {
                mContact06.setVisibility(View.VISIBLE);
                ContextHelper.loadAvatar(getContext(), mConferenceContacts.get(5), mConferenceContacts.get(5).Phone, mContact06);
                //mContact06.setText(mConferenceContacts.get(5).Phone);
            } else {
                mContact06.setVisibility(View.GONE);
            }

            if (mConferenceContacts.size() >= 7) {
                mContact07.setVisibility(View.VISIBLE);
                ContextHelper.loadAvatar(getContext(), mConferenceContacts.get(6), mConferenceContacts.get(6).Phone, mContact07);
                //mContact07.setText(mConferenceContacts.get(6).Phone);
            } else {
                mContact07.setVisibility(View.GONE);
            }

            if (mConferenceContacts.size() >= 8) {
                mContact08.setVisibility(View.VISIBLE);
                ContextHelper.loadAvatar(getContext(), mConferenceContacts.get(7), mConferenceContacts.get(7).Phone, mContact08);
                //mContact08.setText(mConferenceContacts.get(7).Phone);
            } else {
                mContact08.setVisibility(View.GONE);
            }

            if (mConferenceContacts.size() >= 9) {
                mContact09.setVisibility(View.VISIBLE);
                ContextHelper.loadAvatar(getContext(), mConferenceContacts.get(8), mConferenceContacts.get(8).Phone, mContact09);
                //mContact09.setText(mConferenceContacts.get(8).Phone);
            } else {
                mContact09.setVisibility(View.GONE);
            }

            if (mConferenceContacts.size() >= 10) {
                mContact10.setVisibility(View.VISIBLE);
                ContextHelper.loadAvatar(getContext(), mConferenceContacts.get(9), mConferenceContacts.get(9).Phone, mContact10);
                //mContact10.setText(mConferenceContacts.get(9).Phone);
            } else {
                mContact10.setVisibility(View.GONE);
            }
        }
    }
}

