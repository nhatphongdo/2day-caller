package sg.today.service;

import java.util.ArrayList;

import sg.today.model.Contact;

/**
 * Created by Phong Do on 11/27/2016.
 */

public interface OnContactSelectionListener {

    void OnContactSelection(ArrayList<Contact> contacts);

}
