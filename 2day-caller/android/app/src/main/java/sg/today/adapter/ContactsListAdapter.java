package sg.today.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.allen.expandablelistview.BaseSwipeMenuExpandableListAdapter;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.baoyz.swipemenulistview.ContentViewWrapper;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

import sg.R;
import sg.today.model.Contact;
import sg.today.service.ContextHelper;

/**
 * Created by Phong Do on 11/21/2016.
 */

public class ContactsListAdapter extends BaseSwipeMenuExpandableListAdapter implements Filterable {

    private static final String TwoDayContactHeader = "2DAY CONTACTS";

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Contact> contactsList = new ArrayList<Contact>();
    private ArrayList<String> headers = new ArrayList<String>();
    private Hashtable<String, ArrayList<Contact>> headerList = new Hashtable<String, ArrayList<Contact>>();

    public ContactsListAdapter(Context mContext, ArrayList<Contact> contactsList) {
        this.mContext = mContext;
        this.contactsList = contactsList;
        this.mInflater = LayoutInflater.from(mContext);

        populateGroups(this.contactsList);
    }

    public void populateGroups(ArrayList<Contact> contacts) {
        headers.clear();
        headerList.clear();
        for (int i = 0; i < contacts.size(); i++) {
            if (contacts.get(i).IsDeleted) {
                continue;
            }

            if (contacts.get(i).SipAccount != null && !TextUtils.isEmpty(contacts.get(i).SipAccount)) {
                if (!headers.contains(TwoDayContactHeader)) {
                    ArrayList<Contact> childList = new ArrayList<Contact>();
                    childList.add(contacts.get(i));
                    headerList.put(TwoDayContactHeader, childList);
                    headers.add(0, TwoDayContactHeader);
                } else {
                    headerList.get(TwoDayContactHeader).add(contacts.get(i));
                }
            } else {
                if (!headers.contains(contacts.get(i).Name.substring(0, 1).toUpperCase())) {
                    ArrayList<Contact> childList = new ArrayList<Contact>();
                    childList.add(contacts.get(i));
                    headerList.put(contacts.get(i).Name.substring(0, 1).toUpperCase(), childList);
                    headers.add(contacts.get(i).Name.substring(0, 1).toUpperCase());
                } else {
                    headerList.get(contacts.get(i).Name.substring(0, 1).toUpperCase()).add(contacts.get(i));
                }
            }
        }
    }

    public void setContactsList(ArrayList<Contact> contacts) {
        this.contactsList = contacts;
        populateGroups(this.contactsList);
    }

    @Override
    public Contact getChild(int groupPosition, int childPosititon) {
        return this.headerList.get(headers.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public ContentViewWrapper getGroupViewAndReUsable(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        boolean reUsable = true;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.view_item_contact_group, null);
            reUsable = false;
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.group_name);
        lblListHeader.setText(headerTitle);

        convertView.setOnClickListener(null);

        return new ContentViewWrapper(convertView, reUsable);
    }

    @Override
    public ContentViewWrapper getChildViewAndReUsable(final int groupPosition, final int childPosition, boolean isLastChild, View view, ViewGroup parent) {
        boolean reUsable = true;
        final Contact contact = getChild(groupPosition, childPosition);

        reUsable = false;
        view = mInflater.inflate(R.layout.view_item_contact, null);

        CircularImageView avatar = (CircularImageView) view.findViewById(R.id.avatar);
        TextView contactName = (TextView) view.findViewById(R.id.contact_name);
        TextView contactNumber = (TextView) view.findViewById(R.id.contact_number);
        ImageView sipAccount = (ImageView) view.findViewById(R.id.contact_2day);

        contactName.setText(contact.Name);
        contactNumber.setText(contact.Phone);
        if (TextUtils.isEmpty(contact.SipAccount)) {
            sipAccount.setVisibility(View.INVISIBLE);
        } else {
            if (contact.Phone == null || TextUtils.isEmpty(contact.Phone)) {
            }
            contactNumber.setText(contact.SipAccount);
            sipAccount.setVisibility(View.VISIBLE);
        }

        ContextHelper.loadAvatar(mContext, contact, contact.Phone, avatar);

        return new ContentViewWrapper(view, reUsable);
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.headerList.get(headers.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.headers.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.headers.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public boolean isGroupSwipable(int groupPosition) {
        return false;
    }

    @Override
    public boolean isChildSwipable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    results.values = contactsList;
                    results.count = contactsList.size();
                } else {
                    ArrayList<Contact> list = new ArrayList<Contact>();
                    for (int i = 0; i < contactsList.size(); i++) {
                        if (contactsList.get(i).Name.toLowerCase().indexOf(constraint.toString().toLowerCase()) >= 0) {
                            list.add(contactsList.get(i));
                        }
                    }
                    results.values = list;
                    results.count = list.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                populateGroups((ArrayList<Contact>) filterResults.values);
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        Contact contact = getChild(groupPosition, childPosition);
        return contact.IsLiked ? 1 : 0;
    }

    public void removeItem(int groupPosition, int childPosition) {
        String header = headers.get(groupPosition);
        long id = headerList.get(header).get(childPosition).Id;
        headerList.get(header).remove(childPosition);
        if (headerList.get(header).size() == 0) {
            headerList.remove(header);
            headers.remove(groupPosition);
        }
        for (int i = 0; i < contactsList.size(); i++) {
            if (contactsList.get(i).Id == id) {
                contactsList.remove(i);
                break;
            }
        }
    }

    public void updateIsLiked(int groupPosition, int childPosition, boolean flag) {
        String header = headers.get(groupPosition);
        long id = headerList.get(header).get(childPosition).Id;
        headerList.get(header).get(childPosition).IsLiked = flag;
        for (int i = 0; i < contactsList.size(); i++) {
            if (contactsList.get(i).Id == id) {
                contactsList.get(i).IsLiked = flag;
                break;
            }
        }
    }
}
