package sg.today.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

import sg.R;
import sg.today.AndroidBug5497Workaround;
import sg.today.model.Profile;
import sg.today.model.SipAccount;
import sg.today.service.ApiServices;
import sg.today.service.ContextHelper;
import sg.today.service.JsonObjectResultInterface;

public class BuyCreditActivity extends BaseActivity {

    private String paypalButtonId = "J5P6TKTZ9FB6W";

    private TextView mSipBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_credit);

        AndroidBug5497Workaround.assistActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.back_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        mSipBalance = (TextView) findViewById(R.id.current_balance);

        Button purchaseButton = (Button) findViewById(R.id.purchase_button);
        purchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Profile profile = ContextHelper.getProfile(getContext());
                String url;
                url = String.format(ApiServices.BUY_CREDIT,
                        paypalButtonId,
                        "Please select plan:", "Call Credit",
                        "Email Address:", profile.email,
                        "Mobile Number:", profile.countryCode + profile.cityCode + profile.mobileNumber);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
        });
        Button checkRatesButton = (Button) findViewById(R.id.check_rates_button);
        checkRatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ApiServices.CHECK_RATES)));
            }
        });

        mSipBalance.setText("");
        ContextHelper.loadSipBalance(getContext(), mSipBalance, "$###,###.## USD");
    }

    private BuyCreditActivity getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
