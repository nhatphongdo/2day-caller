package sg.today.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;

import sg.R;
import sg.today.AndroidBug5497Workaround;
import sg.today.MainApplication;
import sg.today.model.Profile;
import sg.today.service.ApiServices;
import sg.today.service.ContextHelper;
import sg.today.service.JsonObjectResultInterface;

public class EditProfileActivity extends BaseActivity {

    private int PICK_IMAGE_REQUEST = 1;

    private CircularImageView mProfileImage;
    private TextView mClickToChange;
    private EditText mFirstName;
    private EditText mLastName;
    private EditText mCurrentPassword;
    private EditText mNewPassword;
    private EditText mRetypeNewPassword;
    private Bitmap mBitmapData;

    private View mProgressView;
    private View mFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        AndroidBug5497Workaround.assistActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.back_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        mFormView = findViewById(R.id.form);
        mProgressView = findViewById(R.id.progress);

        mProfileImage = (CircularImageView) findViewById(R.id.profile_image);
        mClickToChange = (TextView) findViewById(R.id.click_to_change);
        mFirstName = (EditText) findViewById(R.id.first_name);
        mLastName = (EditText) findViewById(R.id.last_name);
        mCurrentPassword = (EditText) findViewById(R.id.current_password);
        mNewPassword = (EditText) findViewById(R.id.new_password);
        mRetypeNewPassword = (EditText) findViewById(R.id.retype_new_password);

        mClickToChange.getBackground().setAlpha(255);
        final Profile profile = ContextHelper.getProfile(getContext());
        if (!TextUtils.isEmpty(profile.profileImage)) {
            ImageLoader mImageLoader = ApiServices.getInstance(getContext()).getImageLoader();
            mImageLoader.get(profile.profileImage, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response.getBitmap() != null) {
                        mProfileImage.setImageBitmap(response.getBitmap());
                        mClickToChange.getBackground().setAlpha(100);
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
        }
        mFirstName.setText(profile.firstName);
        mLastName.setText(profile.lastName);

        mClickToChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        Button update = (Button) findViewById(R.id.submit);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitInfo();
            }
        });

        ContextHelper.hideKeyboard(getContext());
    }

    private EditProfileActivity getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            ContextHelper.hideKeyboard(getContext());
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void selectImage() {
        Intent intent;

        if (Build.VERSION.SDK_INT < 19) {
            intent = new Intent();
            intent.setAction(Intent.ACTION_GET_CONTENT);
        } else {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
        }

        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            Profile profile = ContextHelper.getProfile(getContext());
            profile.profileImage = uri.toString();
            ContextHelper.setProfile(getContext(), profile);

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                mBitmapData = Bitmap.createScaledBitmap(bitmap, 200, 200, false);
                profile.profileImageBitmapData = mBitmapData;
                mProfileImage.setImageBitmap(mBitmapData);
                mClickToChange.getBackground().setAlpha(100);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void submitInfo() {
        mFirstName.setError(null);
        mLastName.setError(null);
        mCurrentPassword.setError(null);
        mNewPassword.setError(null);
        mRetypeNewPassword.setError(null);

        if (TextUtils.isEmpty(mFirstName.getText())) {
            mFirstName.setError(getString(R.string.error_first_name_required));
            mFirstName.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(mLastName.getText())) {
            mLastName.setError(getString(R.string.error_last_name_required));
            mLastName.requestFocus();
            return;
        }

        showProgress(true);

        final Profile profile = ContextHelper.getProfile(getContext());
        String[] account = ContextHelper.getAccount(getContext());
        String token = ContextHelper.getGcmToken(getContext());
        profile.firstName = mFirstName.getText().toString();
        profile.lastName = mLastName.getText().toString();
        try {
            if (mBitmapData != null) {
                ApiServices.getInstance(getContext()).UpdateProfileImage(account[0], account[1], mBitmapData, new JsonObjectResultInterface() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.optInt("result_code") != 1) {
                            ContextHelper.showAlert(getContext(), getString(R.string.profile), response.optString("result_message"));
                        }
                    }

                    @Override
                    public void onErrorResponse(String error) {

                    }
                });
            }

            ApiServices.getInstance(getContext()).ChangeProfile(account[0], account[1], profile, token, new JsonObjectResultInterface() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response.optInt("result_code") == 1) {
                        ContextHelper.setProfile(getContext(), profile);
                        MainApplication.getInstance().updateProfile();
                        submitPassword();
                    } else {
                        showProgress(false);
                        ContextHelper.showAlert(getContext(), getString(R.string.profile), response.optString("result_message"));
                    }
                }

                @Override
                public void onErrorResponse(String error) {
                    showProgress(false);
                    ContextHelper.showAlert(getContext(), getString(R.string.profile), error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            showProgress(false);
            ContextHelper.showAlert(getContext(), getString(R.string.profile), getString(R.string.update_profile_failed));
        }
    }

    private void submitPassword() {
        if (TextUtils.isEmpty(mCurrentPassword.getText())) {
            // If not input password, ignore
            showProgress(false);
            return;
        }

        final String[] account = ContextHelper.getAccount(getContext());
        if (mCurrentPassword.getText().toString().compareTo(account[1]) != 0) {
            showProgress(false);
            mCurrentPassword.setError(getString(R.string.error_current_password_wrong));
            mCurrentPassword.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(mNewPassword.getText())) {
            showProgress(false);
            mNewPassword.setError(getString(R.string.error_new_password_required));
            mNewPassword.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(mRetypeNewPassword.getText())) {
            showProgress(false);
            mRetypeNewPassword.setError(getString(R.string.error_retype_new_password_required));
            mRetypeNewPassword.requestFocus();
            return;
        }
        if (mRetypeNewPassword.getText().toString().compareTo(mNewPassword.getText().toString()) != 0) {
            showProgress(false);
            mRetypeNewPassword.setError(getString(R.string.error_password_not_matched));
            mRetypeNewPassword.requestFocus();
            return;
        }

        try {
            ApiServices.getInstance(getContext()).ChangePassword(account[0], mCurrentPassword.getText().toString(),
                    mNewPassword.getText().toString(), mRetypeNewPassword.getText().toString(), new JsonObjectResultInterface() {
                        @Override
                        public void onResponse(JSONObject response) {
                            showProgress(false);
                            if (response.optInt("result_code") == 1) {
                                ContextHelper.setAccount(getContext(), account[0], mCurrentPassword.getText().toString());
                                ContextHelper.showInfo(getContext(), getString(R.string.profile), getString(R.string.update_profile_successfully));
                            } else {
                                ContextHelper.showAlert(getContext(), getString(R.string.profile), response.optString("result_message"));
                            }
                        }

                        @Override
                        public void onErrorResponse(String error) {
                            showProgress(false);
                            ContextHelper.showAlert(getContext(), getString(R.string.profile), error);
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
            showProgress(false);
            ContextHelper.showAlert(getContext(), getString(R.string.profile), getString(R.string.change_password_failed));
        }
    }
}
