package sg.today.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import sg.R;
import sg.today.AndroidBug5497Workaround;

public class SplashActivity extends BaseActivity {
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        AndroidBug5497Workaround.assistActivity(this);

        mHandler.postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);
    }

    private SplashActivity getContext() {
        return this;
    }
}
