package sg.today.model;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Phong Do on 11/17/2016.
 */

public class SipAccount {
    public String sipUsername;
    public String sipDomain;
    public String sipPin;
    public Date sipValidFrom;
    public Date sipValidTo;
    public float sipBalance;
    public String sipStun;
    public String sipProxy;
    public String sipBalanceUrl;
    public String[] sipCodecs;
    public String sipDidNumber;

    public SipAccount() {
    }

    public SipAccount(JSONObject json) {
        if (json == null) {
            return;
        }
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sipUsername = json.getString("username");
            sipDomain = json.getString("domain");
            sipPin = json.getString("pin");
            sipValidFrom = TextUtils.isEmpty(json.getString("valid_from")) ? null : dateFormatter.parse(json.getString("valid_from"));
            sipValidTo = TextUtils.isEmpty(json.getString("valid_to")) ? null : dateFormatter.parse(json.getString("valid_to"));
            sipBalance = (float) json.getDouble("balance");
            sipStun = json.getString("stun");
            sipProxy = json.getString("proxy");
            sipBalanceUrl = json.getString("balance_url");
            JSONArray codecs = json.getJSONArray("codec");
            sipCodecs = new String[codecs.length()];
            for (int i = 0; i < codecs.length(); i++) {
                sipCodecs[i] = codecs.getString(i);
            }
            sipDidNumber = json.isNull("didnumber") ? "" : json.getString("didnumber");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public JSONObject toJSONObject() {
        JSONObject result = new JSONObject();
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            result.put("username", sipUsername);
            result.put("domain", sipDomain);
            result.put("pin", sipPin);
            result.put("valid_from", sipValidFrom == null ? "" : dateFormatter.format(sipValidFrom));
            result.put("valid_to", sipValidTo == null ? "" : dateFormatter.format(sipValidTo));
            result.put("balance", sipBalance);
            result.put("stun", sipStun);
            result.put("proxy", sipProxy);
            result.put("balance_url", sipBalanceUrl);
            JSONArray codecsArray = new JSONArray();
            for (int i = 0; i < sipCodecs.length; i++) {
                codecsArray.put(i, sipCodecs[i]);
            }
            result.put("codec", codecsArray);
            result.put("didnumber", sipDidNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
}
