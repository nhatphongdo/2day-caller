package sg.today.model;

import android.database.Cursor;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Phong Do on 11/20/2016.
 */

public class Contact {
    public long Id;
    public String ContactId;
    public String Name;
    public String Phone;
    public String Email;
    public String Avatar;
    public boolean IsLiked;
    public boolean IsDeleted;
    public String SipAccount;
    public boolean IsSelected;
    public ArrayList<ContactNumber> Numbers;

    public Contact() {
        Numbers = new ArrayList<>();
        IsSelected = false;
    }

    public Contact(Cursor c) {
        Id = c.getLong(0);
        ContactId = c.getString(1);
        Name = c.getString(2);
        Phone = c.getString(3);
        Email = c.getString(4);
        Avatar = c.getString(5);
        SipAccount = c.getString(6);
        IsLiked = c.getInt(7) == 1;
        IsDeleted = c.getInt(8) == 1;
        Numbers = new ArrayList<>();
        IsSelected = false;
    }

    public static class ContactComparator implements Comparator<Contact> {

        public int compare(Contact o1, Contact o2) {
            if (o1.Name == null && o2.Name != null) {
                return -1;
            }
            else if (o1.Name != null && o2.Name == null) {
                return 1;
            }
            else if (o1.Name == null && o2.Name == null) {
                return 0;
            }
            return o1.Name.toUpperCase().trim().compareTo(o2.Name.toUpperCase().trim());
        }

    }

    public static class Contact2DayComparator implements Comparator<Contact> {

        public int compare(Contact o1, Contact o2) {
            if ((o1.SipAccount == null || TextUtils.isEmpty(o1.SipAccount)) &&
                    (o2.SipAccount == null || TextUtils.isEmpty(o2.SipAccount))) {
                return o1.Name.toUpperCase().trim().compareTo(o2.Name.toUpperCase().trim());
            } else if ((o1.SipAccount == null || TextUtils.isEmpty(o1.SipAccount)) &&
                    (o2.SipAccount != null && !TextUtils.isEmpty(o2.SipAccount))) {
                return -1;
            }
            else {
                return 1;
            }
        }

    }

}
