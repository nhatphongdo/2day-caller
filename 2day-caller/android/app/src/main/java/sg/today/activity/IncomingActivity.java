package sg.today.activity;

import android.app.KeyguardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.portsip.PortSipErrorcode;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import sg.today.AndroidBug5497Workaround;
import sg.today.MainApplication;
import sg.R;
import sg.today.model.CallLog;
import sg.today.model.Contact;
import sg.today.service.ContextHelper;
import sg.today.service.DatabaseAdapter;
import sg.today.util.Line;

public class IncomingActivity extends BaseActivity {

    public final static String CALLER_DISPLAY_NAME = "CALLER_DISPLAY_NAME";
    public final static String CALLER_NUMBER = "CALLER_NUMBER";
    public final static String CALL_SESSION_ID = "CALL_SESSION_ID";
    public final static String CALL_IS_VIDEO = "CALL_IS_VIDEO";

    public String callerName;
    public String callerNumber;
    private ImageButton answerButton;
    private ImageButton answerVideoButton;
    private ImageButton rejectButton;

    private DatabaseAdapter mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incoming);

        AndroidBug5497Workaround.assistActivity(this);

        this.setVolumeControlStream(AudioManager.STREAM_RING);

        PowerManager.WakeLock wakeLock = null;
        KeyguardManager kgMgr = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        boolean locked = kgMgr.inKeyguardRestrictedInputMode();
        PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
        if (!pm.isInteractive()) {
            wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "MyWakeLock");
            wakeLock.acquire();
        }
        if (locked) {
            Window mWindow = getWindow();
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        }

        mDatabase = new DatabaseAdapter(this);
        mDatabase.open();

        callerNumber = getIntent().getStringExtra(CALLER_NUMBER);
        if (callerNumber.startsWith("sip:")) {
            callerNumber = callerNumber.substring(4, callerNumber.indexOf("@"));
        }
        callerName = getIntent().getStringExtra(CALLER_DISPLAY_NAME);
        // Try to find contact
        ArrayList<Contact> contacts = mDatabase.searchContacts(callerNumber);
        if (contacts.size() > 0) {
            callerName = contacts.get(0).Name;
        }

        if (callerName == null || callerName.length() == 0 || callerName.equalsIgnoreCase("null")) {
            callerName = callerNumber;
        }

        final String callerDisplayName = callerName;
        final String caller = callerNumber;
        long session = getIntent().getLongExtra(CALL_SESSION_ID, -1);
        final Line curSession = MainApplication.getInstance().findLineBySessionId(session);
        if (curSession == null) {
            finish();
            return;
        }
        final boolean isVideo = getIntent().getBooleanExtra(CALL_IS_VIDEO, false);

        Log.e("Incoming call", callerDisplayName + ", " + callerNumber + ", " + String.valueOf(session) + ", " + String.valueOf(isVideo));

        TextView mCalleeName = (TextView) findViewById(R.id.callee_name);
        mCalleeName.setText(callerDisplayName);
        TextView mCalleeNumber = (TextView) findViewById(R.id.callee_number);
        mCalleeNumber.setText(caller);

        CircularImageView mCallerImage = (CircularImageView) findViewById(R.id.caller_image);
        ContextHelper.loadAvatar(this, contacts.size() > 0 ? contacts.get(0) : null, callerDisplayName, mCallerImage);

        answerButton = (ImageButton) findViewById(R.id.answer_button);
        answerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContextHelper.stopRingtone();

                MainApplication.getInstance().answerSessionCall(callerDisplayName, caller, curSession, false);
                finish();
            }
        });
        answerVideoButton = (ImageButton)findViewById(R.id.answer_video_button);
        if (!isVideo) {
            answerVideoButton.setVisibility(View.GONE);
        }
        answerVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContextHelper.stopRingtone();

                MainApplication.getInstance().answerSessionCall(callerDisplayName, caller, curSession, true);
                finish();
            }
        });
        rejectButton = (ImageButton) findViewById(R.id.reject_button);
        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContextHelper.stopRingtone();

                Log.e("Incoming", "End call " + String.valueOf(curSession.getSessionId()));

                if (curSession.getSessionId() != PortSipErrorcode.INVALID_SESSION_ID) {
                    // Save to log
                    CallLog log = new CallLog();
                    log.ContactId = 0;
                    log.Name = callerDisplayName;
                    log.Phone = caller;
                    log.Timer = new Date();
                    log.Status = "Incoming";
                    mDatabase.insertCallLog(log);

                    MainApplication.getInstance().rejectCall(curSession);
                }
                else {
                    curSession.reset();
                }
                finish();
            }
        });

        MainApplication.getInstance().setIncomingActivity(this);

        // Check state
        if (String.valueOf(getIntent().getAction()) == "Answer") {
            answerButton.performClick();
            return;
        }
        if (String.valueOf(getIntent().getAction()) == "Answer Video") {
            answerVideoButton.performClick();
            return;
        }
        if (String.valueOf(getIntent().getAction()) == "Reject") {
            rejectButton.performClick();
            return;
        }

        ContextHelper.playRingtone(this, true);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mDatabase == null) {
            mDatabase = new DatabaseAdapter(this);
            mDatabase.open();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mDatabase != null) {
            mDatabase.close();
        }
        mDatabase = null;
    }

    @Override
    public void onBackPressed() {
        if (rejectButton != null) {
            rejectButton.performClick();
        }
        return;
    }
}
