package sg.today.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;

import sg.R;
import sg.today.AndroidBug5497Workaround;
import sg.today.model.Profile;
import sg.today.service.ApiServices;
import sg.today.service.ContextHelper;
import sg.today.service.JsonObjectResultInterface;


public class RegisterActivity extends BaseActivity {

    private int PICK_IMAGE_REQUEST = 1;

    private CircularImageView mProfileImage;
    private TextView mChangeImage;
    private EditText mUserNameView;
    private EditText mFirstNameView;
    private EditText mLastNameView;
    private EditText mEmailView;
    private EditText mCountryCodeView;
    private EditText mCityCodeView;
    private EditText mMobileNumberView;
    private EditText mPasswordView;
    private EditText mRetypePasswordView;
    private View mProgressView;
    private View mFormView;
    private Bitmap mBitmapData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        AndroidBug5497Workaround.assistActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitleTextColor(ContextCompat.getColor(getContext(), R.color.toolbar_title));
        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.back_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        // Set up the register form.
        mProfileImage = (CircularImageView) findViewById(R.id.profile_image);
        mChangeImage = (TextView) findViewById(R.id.click_to_change);
        mChangeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        mUserNameView = (EditText) findViewById(R.id.username);
        mUserNameView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    mFirstNameView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mFirstNameView = (EditText) findViewById(R.id.first_name);
        mFirstNameView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    mLastNameView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mLastNameView = (EditText) findViewById(R.id.last_name);
        mLastNameView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    mEmailView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mEmailView = (EditText) findViewById(R.id.email);
        mEmailView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    mCountryCodeView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mCountryCodeView = (EditText) findViewById(R.id.country_code);
        mCountryCodeView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    mCityCodeView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mCityCodeView = (EditText) findViewById(R.id.city_code);
        mCityCodeView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    mMobileNumberView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mMobileNumberView = (EditText) findViewById(R.id.mobile_number);
        mMobileNumberView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    mPasswordView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    mRetypePasswordView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mRetypePasswordView = (EditText) findViewById(R.id.retype_password);
        mRetypePasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptRegister();
                    return true;
                }
                return false;
            }
        });

        Button mRegisterButton = (Button) findViewById(R.id.register_button);
        mRegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });

        mFormView = findViewById(R.id.form);
        mProgressView = findViewById(R.id.progress);
    }

    private RegisterActivity getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            ContextHelper.hideKeyboard(getContext());
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void attemptRegister() {
        // Reset errors.
        mUserNameView.setError(null);
        mFirstNameView.setError(null);
        mLastNameView.setError(null);
        mEmailView.setError(null);
        mCountryCodeView.setError(null);
        mCityCodeView.setError(null);
        mMobileNumberView.setError(null);
        mPasswordView.setError(null);
        mRetypePasswordView.setError(null);

        final String userName = mUserNameView.getText().toString();
        final String firstName = mFirstNameView.getText().toString();
        final String lastName = mLastNameView.getText().toString();
        final String email = mEmailView.getText().toString();
        final String country = mCountryCodeView.getText().toString();
        final String city = mCityCodeView.getText().toString();
        final String mobile = mMobileNumberView.getText().toString();
        final String password = mPasswordView.getText().toString();
        final String repassword = mRetypePasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(userName)) {
            mUserNameView.setError(getString(R.string.error_user_name_required));
            focusView = mUserNameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(firstName)) {
            mFirstNameView.setError(getString(R.string.error_first_name_required));
            focusView = mFirstNameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(lastName)) {
            mLastNameView.setError(getString(R.string.error_last_name_required));
            focusView = mLastNameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_email_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (TextUtils.isEmpty(country)) {
            mCountryCodeView.setError(getString(R.string.error_country_required));
            focusView = mCountryCodeView;
            cancel = true;
        }

        if (TextUtils.isEmpty(city)) {
            mCityCodeView.setError(getString(R.string.error_city_required));
            focusView = mCityCodeView;
            cancel = true;
        }

        if (TextUtils.isEmpty(mobile)) {
            mMobileNumberView.setError(getString(R.string.error_mobile_required));
            focusView = mMobileNumberView;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_password_required));
            focusView = mPasswordView;
            cancel = true;
        }
        if (TextUtils.isEmpty(repassword)) {
            mRetypePasswordView.setError(getString(R.string.error_retype_password_required));
            focusView = mRetypePasswordView;
            cancel = true;
        }
        if (password.compareTo(repassword) != 0) {
            mRetypePasswordView.setError(getString(R.string.error_password_not_matched));
            focusView = mRetypePasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            ContextHelper.hideKeyboard(getContext());
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            try {
                ApiServices.getInstance(getContext()).Register(userName, email, country, city, mobile, password, repassword, firstName, lastName, new JsonObjectResultInterface() {
                    @Override
                    public void onResponse(JSONObject response) {
                        switch (response.optInt("result_code")) {
                            case 1:
                                // Successful
                                // Upload avatar
                                //final String message = response.optString("result_message");
                                final String message = getString(R.string.register_successfully);
                                if (mBitmapData != null) {
                                    try {
                                        ApiServices.getInstance(getContext()).UpdateProfileImage(email, password, mBitmapData, new JsonObjectResultInterface() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                if (response.optInt("result_code") == 1) {
                                                    showProgress(false);
                                                    ContextHelper.showInfo(
                                                            getContext(),
                                                            getString(R.string.title_activity_register),
                                                            message);
                                                    finish();
                                                }
                                                else {
                                                    showProgress(false);
                                                    ContextHelper.showAlert(getContext(),
                                                            getString(R.string.title_activity_register),
                                                            getString(R.string.error_upload_profile_image_failed) + " " + response.optString("result_message") + "\n" + message);
                                                    finish();
                                                }
                                            }

                                            @Override
                                            public void onErrorResponse(String error) {
                                                showProgress(false);
                                                ContextHelper.showAlert(getContext(),
                                                        getString(R.string.title_activity_register),
                                                        getString(R.string.error_upload_profile_image_failed) + "\n" + message);
                                                finish();
                                            }
                                        });
                                    }
                                    catch (Exception exc) {
                                        exc.printStackTrace();
                                        showProgress(false);
                                        ContextHelper.showAlert(getContext(),
                                                getString(R.string.title_activity_register),
                                                getString(R.string.error_upload_profile_image_failed) + "\n" + message);
                                        finish();
                                    }
                                }
                                else {
                                    showProgress(false);
                                    ContextHelper.showInfo(
                                            getContext(),
                                            getString(R.string.title_activity_register),
                                            message);
                                    finish();
                                }
                                break;
                            case 3:
                                showProgress(false);
                                mRetypePasswordView.setError(getString(R.string.error_password_not_matched));
                                mRetypePasswordView.requestFocus();
                                break;
                            default:
                                showProgress(false);
                                mUserNameView.setError(response.optString("result_message"));
                                mUserNameView.requestFocus();
                                break;
                        }
                    }

                    @Override
                    public void onErrorResponse(String error) {
                        showProgress(false);
                        // Show error
                        ContextHelper.showAlert(getContext(), getString(R.string.title_activity_register), error);
                    }
                });
            } catch (JSONException e) {
                showProgress(false);
                e.printStackTrace();
                // Show error
                ContextHelper.showAlert(
                        getContext(),
                        getString(R.string.title_activity_register),
                        getString(R.string.api_format_error));
            }
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void selectImage() {
        Intent intent;

        if (Build.VERSION.SDK_INT < 19) {
            intent = new Intent();
            intent.setAction(Intent.ACTION_GET_CONTENT);
        } else {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
        }

        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            Profile profile = ContextHelper.getProfile(getContext());
            profile.profileImage = uri.toString();
            ContextHelper.setProfile(getContext(), profile);

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                mBitmapData = Bitmap.createScaledBitmap(bitmap, 200, 200, false);
                profile.profileImageBitmapData = mBitmapData;
                mProfileImage.setImageBitmap(mBitmapData);
                mChangeImage.getBackground().setAlpha(100);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

