package sg.today.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import sg.R;
import sg.today.AndroidBug5497Workaround;
import sg.today.MainApplication;
import sg.today.model.Contact;
import sg.today.model.Profile;
import sg.today.model.SipAccount;
import sg.today.service.ApiServices;
import sg.today.service.ContextHelper;
import sg.today.service.JsonObjectResultInterface;
import sg.today.service.OnContactSelectionListener;

public class TransferCreditActivity extends BaseActivity
        implements OnContactSelectionListener {

    private EditText toEmailAddress;
    private EditText transferAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_credit);

        AndroidBug5497Workaround.assistActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.back_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        final TextView selfEmail = (TextView) findViewById(R.id.email_address);
        final TextView balance = (TextView) findViewById(R.id.balance);
        final String[] account = ContextHelper.getAccount(getContext());
        selfEmail.setText(account[0]);
        balance.setText("");
        ContextHelper.loadSipBalance(getContext(), balance, "$###,###.##");

        toEmailAddress = (EditText) findViewById(R.id.to_email_address);
        transferAmount = (EditText) findViewById(R.id.transfer_amount);

        ImageButton selectContact = (ImageButton) findViewById(R.id.select_contact);
        selectContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainApplication.getInstance().setContactSelectionListener(getContext());
                Intent contactSelectorIntent = new Intent(getContext(), ContactsSelectorActivity.class);
                contactSelectorIntent.putExtra(ContactsSelectorActivity.ONLY_SHOW_2DAY_ACCOUNTS, true);
                contactSelectorIntent.putExtra(ContactsSelectorActivity.VIDEO_CALL, true);
                startActivity(contactSelectorIntent);
            }
        });

        Button previewTransfer = (Button) findViewById(R.id.preview_transfer);
        previewTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(toEmailAddress.getText())) {
                    ContextHelper.showAlert(getContext(), getString(R.string.activity_transfer_credit), "Please insert or select Destination Email Address.");
                    toEmailAddress.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(transferAmount.getText())) {
                    ContextHelper.showAlert(getContext(), getString(R.string.activity_transfer_credit), "Please insert Transfer amount.");
                    transferAmount.requestFocus();
                    return;
                }
                double transferAmountValue = Double.valueOf(transferAmount.getText().toString());
                if (ContextHelper.getSipBalance(getContext()) < 5.0 || ContextHelper.getSipBalance(getContext()) < transferAmountValue) {
                    ContextHelper.showAlert(getContext(), getString(R.string.activity_transfer_credit), "Sorry you have insufficient balance to transfer credit.");
                    transferAmount.requestFocus();
                    return;
                }
                if (transferAmountValue < 5.0) {
                    ContextHelper.showAlert(getContext(), getString(R.string.activity_transfer_credit), "Sorry the minimum amount to transfer credit is USD 5.00.");
                    transferAmount.requestFocus();
                    return;
                }
                if (selfEmail.getText().toString().equalsIgnoreCase(toEmailAddress.getText().toString())) {
                    ContextHelper.showAlert(getContext(), getString(R.string.activity_transfer_credit), "Sorry you can't transfer credit to yourself.");
                    toEmailAddress.requestFocus();
                    return;
                }
                int percentage = 0;
                double comission = 0.0;
                double transfer = 0.0;

                if (transferAmountValue < 10) {
                    percentage = 7;
                }
                if (transferAmountValue >= 10 && transferAmountValue <= 50) {
                    percentage = 6;
                }
                if (transferAmountValue > 50) {
                    percentage = 5;
                }
                if (selfEmail.getText().toString().equalsIgnoreCase("admin@2day.sg") || toEmailAddress.getText().toString().equalsIgnoreCase("admin@2day.sg")) {
                    percentage = 0;
                }

                comission = (transferAmountValue / 100) * percentage;
                transfer = transferAmountValue - comission;

                ContextHelper.showGlobalDialog(getContext(), getString(R.string.activity_transfer_credit),
                        String.format("[Admin fee]\nUSD %.2f\n[Net amount transfer]\nUSD %.2f", comission, transfer),
                        "Proceed", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                final ProgressDialog progress = ProgressDialog.show(
                                        getContext(),
                                        getString(R.string.activity_transfer_credit),
                                        getString(R.string.waiting), true, false);

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            final String[] account = ContextHelper.getAccount(getContext());
                                            ApiServices.getInstance(getContext()).TransferCredit(account[0], account[1], toEmailAddress.getText().toString(), transferAmount.getText().toString(), new JsonObjectResultInterface() {
                                                @Override
                                                public void onResponse(JSONObject response) {
                                                    ContextHelper.showAlert(getContext(), getString(R.string.activity_transfer_credit), response.optString("result_message"));

                                                    getContext().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            progress.dismiss();
                                                        }
                                                    });
                                                    balance.setText("");
                                                    ContextHelper.loadSipBalance(getContext(), balance, "$###,###.##");
                                                }

                                                @Override
                                                public void onErrorResponse(String error) {
                                                    getContext().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            progress.dismiss();
                                                        }
                                                    });
                                                }
                                            });
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            getContext().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    progress.dismiss();
                                                }
                                            });
                                        }
                                    }
                                }).start();
                            }
                        },
                        "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
            }
        });
    }

    public TransferCreditActivity getContext() {
        return this;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void OnContactSelection(ArrayList<Contact> contacts) {
        if (contacts.size() == 0) {
            return;
        }

        Contact contact = contacts.get(0);
        if (!TextUtils.isEmpty(contact.Email)) {
            toEmailAddress.setText(contact.Email);
        }
    }
}
