package sg.today.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;

import sg.today.AndroidBug5497Workaround;
import sg.today.MainApplication;
import sg.R;
import sg.today.model.Profile;
import sg.today.model.SipAccount;
import sg.today.service.ApiServices;
import sg.today.service.ContextHelper;
import sg.today.service.JsonObjectResultInterface;

public class ProfileActivity extends BaseActivity {

    private CircularImageView mProfileImage;
    private TextView mProfileName;
    private TextView mProfileSipAccount;
    private Button mEditButton;
    private TextView mMobileNumber;
    private TextView mDidNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        AndroidBug5497Workaround.assistActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.back_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        mProfileImage = (CircularImageView) findViewById(R.id.profile_image);
        mProfileName = (TextView) findViewById(R.id.profile_name);
        mProfileSipAccount = (TextView) findViewById(R.id.profile_sip_account);
        mEditButton = (Button) findViewById(R.id.edit_button);
        mMobileNumber = (TextView) findViewById(R.id.mobile_number);
        mDidNumber = (TextView) findViewById(R.id.did_number);

        final Profile profile = ContextHelper.getProfile(getContext());
        if (!TextUtils.isEmpty(profile.profileImage)) {
            ImageLoader mImageLoader = ApiServices.getInstance(getContext()).getImageLoader();
            mImageLoader.get(profile.profileImage, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response.getBitmap() != null) {
                        mProfileImage.setImageBitmap(response.getBitmap());
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
        }
        mProfileName.setText(profile.firstName + " " + profile.lastName);
        mProfileSipAccount.setText("");
        mMobileNumber.setText(profile.countryCode + profile.cityCode + profile.mobileNumber);
        mDidNumber.setText(profile.sipAccounts.length > 0 ? profile.sipAccounts[0].sipDidNumber : profile.sipDid);

        Button buyCredit = (Button) findViewById(R.id.buy_credit_button);
        buyCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), BuyCreditActivity.class));
            }
        });

        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), EditProfileActivity.class));
            }
        });

        ContextHelper.loadSipBalance(getContext(), mProfileSipAccount, "$###,###.## Credit");

        ContextHelper.hideKeyboard(getContext());
    }

    private ProfileActivity getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onResume() {
        super.onResume();

        final Profile profile = ContextHelper.getProfile(getContext());
        if (!TextUtils.isEmpty(profile.profileImage)) {
            ImageLoader mImageLoader = ApiServices.getInstance(getContext()).getImageLoader();
            mImageLoader.get(profile.profileImage, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response.getBitmap() != null) {
                        mProfileImage.setImageBitmap(response.getBitmap());
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
        }
        mProfileName.setText(profile.firstName + " " + profile.lastName);
    }
}
