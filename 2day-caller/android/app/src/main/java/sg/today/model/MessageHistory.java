package sg.today.model;

import android.database.Cursor;
import android.text.TextUtils;

import com.clover_studio.spikachatmodule.models.Message;
import com.clover_studio.spikachatmodule.models.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Phong Do on 11/20/2016.
 */

public class MessageHistory {
    public String RoomId;
    public Message LastMessage;
    public ArrayList<User> Users;

    public MessageHistory() {
        LastMessage = new Message();
        Users = new ArrayList<>();
    }

    public MessageHistory(JSONObject jsonObject) {
        RoomId = jsonObject.optString("roomID");
        Users = new ArrayList<>();
        JSONArray users = jsonObject.optJSONArray("users");
        for (int i = 0; i < users.length(); i++) {
            User user = new User();
            user.userID = users.optJSONObject(i).optString("userID");
            user.name = users.optJSONObject(i).optString("name");
            user.avatarURL = users.optJSONObject(i).optString("avatarURL");
            user.pushToken = users.optJSONObject(i).optString("token");
            Users.add(user);
        }
        LastMessage = new Message();
        if (jsonObject.optJSONObject("lastMessage") != null) {
            LastMessage.userID = jsonObject.optJSONObject("lastMessage").optString("userID");
            LastMessage.message = jsonObject.optJSONObject("lastMessage").optString("message");
            LastMessage.created = jsonObject.optJSONObject("lastMessage").optLong("created");
        }
    }

    public static class MessageHistoryComparator implements Comparator<MessageHistory> {

        public int compare(MessageHistory o1, MessageHistory o2) {
            if (o1.LastMessage.created > o2.LastMessage.created) {
                return 1;
            }
            else if (o1.LastMessage.created < o2.LastMessage.created) {
                return -1;
            }
            else {
                return 0;
            }
        }

    }

}
