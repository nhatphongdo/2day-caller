package sg.today.fragment;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.TextView;

import com.baoyz.actionsheet.ActionSheet;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;

import sg.today.MainApplication;
import sg.R;
import sg.today.adapter.FavoriteListAdapter;
import sg.today.model.Contact;
import sg.today.service.ContextHelper;
import sg.today.service.DatabaseAdapter;

public class FavoriteFragment extends Fragment
        implements ActionSheet.ActionSheetListener {

    private DatabaseAdapter mDatabase;
    public EditText mSearchEdit;
    private SwipeMenuListView mContactsListView;
    private ArrayList<Contact> contactsList = new ArrayList<Contact>();
    private FavoriteListAdapter adapter;

    public FavoriteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);

        mDatabase = new DatabaseAdapter(getActivity());
        mDatabase.open();

        mSearchEdit = (EditText) view.findViewById(R.id.search_contact);

        contactsList = mDatabase.getFavoriteContacts();
        mContactsListView = (SwipeMenuListView) view.findViewById(R.id.contacts_list);
        adapter = new FavoriteListAdapter(getActivity(), contactsList);
        mContactsListView.setAdapter(adapter);

        final SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem delete = new SwipeMenuItem(getActivity());
                delete.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.right_favorite_background)));
                delete.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                delete.setIcon(R.drawable.white_heart);
                delete.setId(0);
                menu.addMenuItem(delete);
            }

            @Override
            public void createLeft(SwipeMenu menu) {
                SwipeMenuItem voice = new SwipeMenuItem(getActivity());
                voice.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.left_menu_background)));
                voice.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                voice.setIcon(R.drawable.small_action_call);
                voice.setId(1);
                menu.addMenuItem(voice);
                SwipeMenuItem video = new SwipeMenuItem(getActivity());
                video.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.left_menu_background)));
                video.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                video.setIcon(R.drawable.small_action_video);
                menu.addMenuItem(video);
                SwipeMenuItem chat = new SwipeMenuItem(getActivity());
                chat.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.left_menu_background)));
                chat.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                chat.setIcon(R.drawable.small_action_chat);
                menu.addMenuItem(chat);
            }
        };

        // set creator
        mContactsListView.setMenuCreator(creator);
        mContactsListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                if (menu.getMenuItem(0).getId() == 0) {
                    if (index == 0) {
                        // Favorite
                        Contact contact = adapter.getItem(position);
                        mDatabase.updateLikedContact(contact.Id, false);
                        adapter.setContactsList(mDatabase.getFavoriteContacts());
                        adapter.getFilter().filter(mSearchEdit.getText(), new Filter.FilterListener() {
                            @Override
                            public void onFilterComplete(int i) {
                            }
                        });
                    }
                } else {
                    Contact contact = adapter.getItem(position);
                    if (index == 0) {
                        // Call
                        if (!MainApplication.getInstance().isOnline()) {
                            ContextHelper.showAlert(getActivity(), "You Are Not Connected", getString(R.string.sip_not_connected));
                            return false;
                        }
                        MainApplication.getInstance().call(contact, false, false);
                    } else if (index == 1) {
                        // Video
                        if (TextUtils.isEmpty(contact.SipAccount)) {
                            ContextHelper.showTipMessage(getActivity(), getString(R.string.error_video_call_non_sip_user));
                            return false;
                        }
                        if (!MainApplication.getInstance().isOnline()) {
                            ContextHelper.showAlert(getActivity(), "You Are Not Connected", getString(R.string.sip_not_connected));
                            return false;
                        }
                        MainApplication.getInstance().call(contact, true, false);
                    } else if (index == 2) {
                        // Chat
                        if (TextUtils.isEmpty(contact.SipAccount)) {
                            ContextHelper.showTipMessage(getActivity(), getString(R.string.error_chat_non_sip_user));
                            return false;
                        }
                        ArrayList<Contact> contacts = new ArrayList<>();
                        contacts.add(contact);
                        ContextHelper.startChat(getActivity(), contacts);
                    }
                }
                return false;
            }
        });
        mContactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (!MainApplication.getInstance().isOnline()) {
                    ContextHelper.showAlert(getActivity(), "You Are Not Connected", getString(R.string.sip_not_connected));
                    return;
                }

                Contact contact = adapter.getItem(position);
                //MainApplication.getInstance().call(contact, false);
                ArrayList<String> numbers = new ArrayList<>();
                if (contact.SipAccount != null && !TextUtils.isEmpty(contact.SipAccount)) {
                    numbers.add(contact.SipAccount);
                }
                for (int i = 0; i < contact.Numbers.size(); i++) {
                    numbers.add(contact.Numbers.get(i).Number);
                }
                String[] numberArray = new String[numbers.size()];
                numberArray = numbers.toArray(numberArray);
                ActionSheet.createBuilder(getActivity(), getActivity().getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles(numberArray)
                        .setCancelableOnTouchOutside(true)
                        .setListener(getCurrentContext())
                        .setTag(String.valueOf(contact.Id))
                        .show();
            }
        });

        mSearchEdit.clearFocus();
        mSearchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_SEARCH) {
                    ContextHelper.hideKeyboard(getActivity());
                    return true;
                }
                return false;
            }
        });
        mSearchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter == null) {
                    return;
                }
                adapter.getFilter().filter(charSequence, new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int i) {
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mDatabase == null) {
            mDatabase = new DatabaseAdapter(getActivity());
            mDatabase.open();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mDatabase != null) {
            mDatabase.close();
        }
        mDatabase = null;
    }

    public FavoriteFragment getCurrentContext() {
        return this;
    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        Contact contact = mDatabase.getContact(Long.parseLong(actionSheet.getTag()));
        ArrayList<String> numbers = new ArrayList<>();
        if (contact.SipAccount != null && !TextUtils.isEmpty(contact.SipAccount)) {
            numbers.add(contact.SipAccount);
        }
        for (int i = 0; i < contact.Numbers.size(); i++) {
            numbers.add(contact.Numbers.get(i).Number);
        }
        contact.Phone = numbers.get(index);
        contact.SipAccount = numbers.get(index);
        MainApplication.getInstance().call(contact, false, false);
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancle) {
    }
}
