package sg.today.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Filter;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import sg.R;
import sg.today.AndroidBug5497Workaround;
import sg.today.MainApplication;
import sg.today.adapter.CallLogListAdapter;
import sg.today.adapter.MessagesListAdapter;
import sg.today.model.CallLog;
import sg.today.model.Contact;
import sg.today.model.MessageHistory;
import sg.today.model.Profile;
import sg.today.service.ApiServices;
import sg.today.service.ContextHelper;
import sg.today.service.JsonObjectResultInterface;
import sg.today.service.OnContactSelectionListener;

public class ChatHistoryActivity extends BaseActivity implements OnContactSelectionListener {

    private SwipeMenuListView mChatsListView;
    private ArrayList<MessageHistory> chatsList = new ArrayList<>();
    private MessagesListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_history);

        AndroidBug5497Workaround.assistActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.back_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        Button createButton = (Button) findViewById(R.id.create_chat);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.getInstance().setContactSelectionListener(getContext());
                Intent contactSelectorChatIntent = new Intent(getContext(), ContactsSelectorActivity.class);
                contactSelectorChatIntent.putExtra(ContactsSelectorActivity.ONLY_SHOW_2DAY_ACCOUNTS, true);
                contactSelectorChatIntent.putExtra(ContactsSelectorActivity.TITLE, "Add Users to Chat");
                startActivity(contactSelectorChatIntent);
            }
        });

        chatsList = new ArrayList<>();
        mChatsListView = (SwipeMenuListView) findViewById(R.id.messages_list);
        adapter = new MessagesListAdapter(getContext(), chatsList);
        mChatsListView.setAdapter(adapter);

        final SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem delete = new SwipeMenuItem(getContext());
                delete.setBackground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.right_menu_background)));
                delete.setWidth(ContextHelper.dpToPx(getContext(), 100));
                delete.setIcon(R.drawable.remove);
                delete.setId(0);
                menu.addMenuItem(delete);
            }

            @Override
            public void createLeft(SwipeMenu menu) {
            }
        };

        // set creator
        mChatsListView.setMenuCreator(creator);
        mChatsListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                if (menu.getMenuItem(0).getId() == 0) {
                    if (index == 0) {
                        ContextHelper.showGlobalDialog(getContext(),
                                getString(R.string.delete_call_log),
                                getString(R.string.delete_call_log_message),
                                getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        final ProgressDialog progress = ProgressDialog.show(
                                                getContext(),
                                                "Messages",
                                                getString(R.string.waiting), true, false);

                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    MessageHistory chat = adapter.getItem(position);
                                                    final String[] accounts = ContextHelper.getAccount(getContext());
                                                    ApiServices.getInstance(getContext()).RemoveChatHistory(accounts[0], accounts[1], chat.RoomId, new JsonObjectResultInterface() {
                                                        @Override
                                                        public void onResponse(JSONObject response) {
                                                            getContext().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    progress.dismiss();
                                                                }
                                                            });
                                                            loadChatHistory();
                                                        }

                                                        @Override
                                                        public void onErrorResponse(String error) {
                                                            getContext().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    progress.dismiss();
                                                                }
                                                            });
                                                        }
                                                    });
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                    getContext().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            progress.dismiss();
                                                        }
                                                    });
                                                }
                                            }
                                        }).start();
                                    }
                                },
                                getString(R.string.no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                    }
                                });
                    }
                }
                return false;
            }
        });

        mChatsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                MessageHistory chat = adapter.getItem(position);
                String[] accounts = ContextHelper.getAccount(getContext());
                Profile profile = ContextHelper.getProfile(getContext());
                ContextHelper.startChat(getContext(), chat.RoomId, accounts[0], profile.firstName + " " + profile.lastName, profile.profileImage);
            }
        });

        loadChatHistory();
    }

    public ChatHistoryActivity getContext() {
        return this;
    }

    @Override
    public void onResume() {
        super.onResume();

        loadChatHistory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void OnContactSelection(final ArrayList<Contact> contacts) {
        if (contacts.size() == 0) {
            return;
        }

        ContextHelper.startChat(getContext(), contacts);
    }

    private void loadChatHistory() {
        final ProgressDialog progress = ProgressDialog.show(
                getContext(),
                "Messages",
                getString(R.string.waiting), true, false);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final String[] accounts = ContextHelper.getAccount(getContext());
                    ApiServices.getInstance(getContext()).GetChatHistory(accounts[0], accounts[1], new JsonObjectResultInterface() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                ArrayList<MessageHistory> messages = new ArrayList<>();
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    messages.add(new MessageHistory(data.optJSONObject(i)));
                                }
                                adapter.setMessagesList(messages);
                                adapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            getContext().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.dismiss();
                                }
                            });
                        }

                        @Override
                        public void onErrorResponse(String error) {
                            getContext().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.dismiss();
                                }
                            });
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                    getContext().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progress.dismiss();
                        }
                    });
                }
            }
        }).start();
    }
}
