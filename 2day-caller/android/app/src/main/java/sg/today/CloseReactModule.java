package sg.today;

import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by Do Nhat Phong on 5/31/2017.
 */

public class CloseReactModule extends ReactContextBaseJavaModule {

    public CloseReactModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "CloseReactModule";
    }

    @ReactMethod
    public void close() {
        Log.e("React module", "Close React");
        if (MainApplication.getInstance().getMeetActivity() != null
            && !MainApplication.getInstance().getMeetActivity().isDestroyed()) {
            MainApplication.getInstance().getMeetActivity().finish();
        }
    }
}
