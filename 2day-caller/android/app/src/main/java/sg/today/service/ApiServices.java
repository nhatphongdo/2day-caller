package sg.today.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import sg.R;
import sg.today.model.Profile;
import sg.today.util.VolleyMultipartRequest;

/**
 * Created by Phong Do on 11/16/2016.
 */

public class ApiServices {

    private static ApiServices mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static Context mCtx;

    private static final String BASE_URL = "https://2day.com.sg/2day/api/";
    private static final String NEW_BASE_URL = "https://2day.sg:8443/portal/api/";
    //private static final String REGISTER_URL = BASE_URL + "create.php?e=%s&c=%s&ci=%s&m=%s&p=%s&pc=%s&f=%s&l=%s&mox=%d&tel=%d";
    private static final String REGISTER_URL = NEW_BASE_URL + "register?e=%s&c=%s&ci=%s&m=%s&p=%s&pc=%s&f=%s&l=%s&mox=%d&tel=%d&username=%s";
    private static final String LOGIN_URL = BASE_URL + "check.php?u=%s&p=%s";
    private static final String GET_ACCOUNT_DETAIL_URL = BASE_URL + "getaccountdetail.php?u=%s&p=%s";
    private static final String GET_PROFILE_DETAIL_URL = NEW_BASE_URL + "account/getprofiledetail?u=%s&p=%s";
    private static final String CHANGE_PASSWORD_URL = BASE_URL + "changepassword.php?u=%s&p=%s&cp=%s&np=%s&npc=%s";
    private static final String MODIFY_ACCOUNT_INFO_URL = BASE_URL + "modifyinformation.php?u=%s&p=%s&mox=%d&tel=%d&sn=%s&sr=%s&sd=%s&sc=%s&at=%s&gt=%s&f=%s&l=%s";
    //private static final String RESET_PASSWORD_URL = BASE_URL + "resetpassword.php?e=%s&c=%s&ci=%s&m=%s";
    private static final String RESET_PASSWORD_URL = NEW_BASE_URL + "resetpassword?e=%s&c=%s&ci=%s&m=%s";
    private static final String CHECK_SIP_ACCOUNT_URL = BASE_URL + "checkuserexists.php?u=%s&p=%s&l=%s";
    private static final String GET_SIP_INFO_URL = BASE_URL + "getsipinfo.php?u=%s&p=%s";
    private static final String GET_SIP_BALANCE_URL = BASE_URL + "getsipbalance.php?u=%s&p=%s";
    private static final String SET_PROFILE_IMAGE_URL = BASE_URL + "setprofileimage.php?u=%s&p=%s";
    private static final String LOGOUT_URL = BASE_URL + "logout.php";
    //private static final String UPDATE_GCM_URL = BASE_URL + "modifyinformation.php?u=%s&p=%s&gt=%s";
    //private static final String SEND_CHAT_REQUEST = BASE_URL + "sendgroupchatnotification.php?u=%s&p=%s&r=%s&i=%s&s=%s";
    private static final String UPDATE_GCM_URL = "http://chat.2day.sg/spika/v1/updatetoken";
    private static final String SEND_CHAT_REQUEST = "http://chat.2day.sg/spika/v1/sendchatrequest";
    private static final String REMOVE_CHAT_HISTORY_REQUEST = "http://chat.2day.sg/spika/v1/room/delete/%s";
    private static final String TRANSFER_CREDIT_URL = BASE_URL + "transfercredit.php?u=%s&p=%s&e=%s&a=%s";

    public static final String BUY_CREDIT = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=%s&on0=%s&os0=%s&on1=%s&os1=%s&on2=%s&os2=%s&currency_code=USD";
    public static final String UPGRADE_PLAN_SUBSCRIBE = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=Y2GPK4U2DGFAU&on0=%s&on1=%s&os0=%s&os1=%s";
    public static final String UPGRADE_PLAN_UNSUBSCRIBE = "https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=H3LGJ7T5XN63L";

    public static final String TERMS_AND_SERVICES = "https://www.2day.sg/tos.html";
    public static final String PRIVACY_POLICY = "https://www.2day.sg/privacy.html";
    public static final String DISCLAIMER = "https://www.2day.sg/disclaimer.html";
    public static final String NOTICES = "https://2day.sg/mobile/mnotices.php";
    public static final String FAQ = "https://2day.sg/mobile/mfaq.php";
    public static final String CHECK_RATES = "https://2day.sg/mobile/mcallrates.php";

    private ApiServices(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();

        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    public static synchronized ApiServices getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ApiServices(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            HurlStack hurlStack = new HurlStack() {
                @Override
                protected HttpURLConnection createConnection(URL url) throws IOException {
                    if (url.getProtocol().toLowerCase().startsWith("https")) {
                        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
                        try {
                            httpsURLConnection.setSSLSocketFactory(getSSLSocketFactory());
                            httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return httpsURLConnection;
                    } else {
                        return super.createConnection(url);
                    }
                }
            };

            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext(), hurlStack);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    private HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true; // verify always returns true, which could cause insecure network traffic due to trusting TLS/SSL server certificates for wrong hostnames
                //HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
                //return hv.verify("real host name here", session);
            }
        };
    }

    private TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
        final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
        return new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return originalTrustManager.getAcceptedIssuers();
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0) {
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkClientTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            e.printStackTrace();
                        }
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0) {
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkServerTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            e.printStackTrace();
                        }
                    }
                }
        };
    }

    private class ModifiedX509TrustManager implements X509TrustManager {
        X509TrustManager defaultTrustMgr;
        boolean accepted;

        public ModifiedX509TrustManager() {
            TrustManager managers[] = {null};
            try {
                KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
                String algo = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(algo);
                tmf.init(ks);
                managers = tmf.getTrustManagers();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < managers.length; i++) {
                if (managers[i] instanceof X509TrustManager) {
                    defaultTrustMgr = (X509TrustManager) managers[i];
                    return;
                }
            }
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
            defaultTrustMgr.checkClientTrusted(chain, authType);
        }

        public void setAccepted(boolean acc) {
            accepted = acc;
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
            // First, check with the default.
            try {
                defaultTrustMgr.checkServerTrusted(chain, authType);
                // if there is no exception, return happily.
            } catch (CertificateException ce) {
                if (accepted) {
                    // YES. They've accepted it in the setup dialog.
                    return;
                } else {
                    Log.e("Device Not trusted!", "Check Settings.");
                    throw ce;
                }
            }
        }

        public X509Certificate[] getAcceptedIssuers() {
            return defaultTrustMgr.getAcceptedIssuers();
        }

    }

    private SSLSocketFactory getSSLSocketFactory()
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, KeyManagementException {
        SSLContext sc = SSLContext.getInstance("TLS");
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                        return myTrustedAnchors;
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
        };
        sc.init(null, trustAllCerts, new SecureRandom());

        return sc.getSocketFactory();

        // Prepare for production with real certificate

//        CertificateFactory cf = CertificateFactory.getInstance("X.509");
//        InputStream caInput = mCtx.getResources().openRawResource(R.raw.my_cert); // this cert file stored in \app\src\main\res\raw folder path
//
//        Certificate ca = cf.generateCertificate(caInput);
//        caInput.close();
//
//        KeyStore keyStore = KeyStore.getInstance("BKS");
//        keyStore.load(null, null);
//        keyStore.setCertificateEntry("ca", ca);
//
//        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//        tmf.init(keyStore);
//
//        TrustManager[] wrappedTrustManagers = getWrappedTrustManagers(tmf.getTrustManagers());
//
//        SSLContext sslContext = SSLContext.getInstance("TLS");
//        sslContext.init(null, wrappedTrustManagers, null);
//
//        return sslContext.getSocketFactory();
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public boolean CheckNetworkConnectivity() {
        ConnectivityManager connMgr = (ConnectivityManager) mCtx.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            // display error
            return false;
        }
    }

    public void Register(String userName, String email, String countryCode, String cityCode, String mobileNumber, String password, String repassword, String firstName, String lastName,
                         final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format(REGISTER_URL, email, countryCode, cityCode, mobileNumber, password, repassword, firstName, lastName, 0, 0, userName),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        addToRequestQueue(request);
    }

    public void Login(String username, String password, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format(LOGIN_URL, username, md5(password)),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void ResetPassword(String email, String country, String city, String mobile, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format(RESET_PASSWORD_URL, email, country, city, mobile),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void GetAccountDetail(String username, String password, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format(GET_ACCOUNT_DETAIL_URL, username, md5(password)),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void GetProfileDetail(String username, String password, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
            Request.Method.POST,
            String.format(GET_PROFILE_DETAIL_URL, username, password),
            null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (callback != null) {
                        callback.onResponse(response);
                    }
                }
            }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void GetSipInfo(String username, String password, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format(GET_SIP_INFO_URL, username, md5(password)),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void SetGcmToken(String username, String password, String token, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        JSONObject params = new JSONObject();
        params.put("u", username);
        params.put("p", md5(password));
        params.put("t", token);
        params.put("tt", 0);

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                UPDATE_GCM_URL,
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void ChangePassword(String username, String password, String newPassword, String reNewPassword, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format(CHANGE_PASSWORD_URL, username, md5(password), password, newPassword, reNewPassword),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void ChangeProfile(String username, String password, Profile profile, String token, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format(MODIFY_ACCOUNT_INFO_URL, username, md5(password),
                        profile.moxtra, profile.telegram, profile.sipDisplayName, profile.sipRingtone,
                        profile.sipDid, profile.sipConnectType, "", token, profile.firstName, profile.lastName),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void UpdateProfileImage(String username, String password, final Bitmap image, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        VolleyMultipartRequest request = new VolleyMultipartRequest(
                Request.Method.POST,
                String.format(SET_PROFILE_IMAGE_URL, username, md5(password)),
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        String resultResponse = new String(response.data);
                        try {
                            JSONObject result = new JSONObject(resultResponse);
                            if (callback != null) {
                                callback.onResponse(result);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (callback != null) {
                            callback.onErrorResponse(error.getLocalizedMessage());
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                params.put("i", new DataPart("file_avatar.png", byteArray, "image/png"));

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void GetSipBalance(String username, String password, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format(GET_SIP_BALANCE_URL, username, md5(password)),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void CheckSipAccounts(String username, String password, List<String> accounts, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        String accountsList = "";
        for (int i = 0; i < accounts.size(); i++) {
            accountsList += accounts.get(i).replace(" ", "");
            if (i < accounts.size() - 1) {
                accountsList += ",";
            }
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format(CHECK_SIP_ACCOUNT_URL, username, md5(password), accountsList),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void SendChatNotification(String username, String password, ArrayList<String> accounts, String roomId, String message,final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        String accountsList = "";
        for (int i = 0; i < accounts.size(); i++) {
            accountsList += accounts.get(i).replace(" ", "");
            if (i < accounts.size() - 1) {
                accountsList += ",";
            }
        }

        JSONObject params = new JSONObject();
        params.put("u", username);
        params.put("p", md5(password));
        params.put("a", accountsList);
        params.put("r", roomId);
        params.put("m", message);

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                SEND_CHAT_REQUEST,
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void TransferCredit(String username, String password, String toEmail, String amount, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                String.format(TRANSFER_CREDIT_URL, username, md5(password), toEmail, amount),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void GetChatHistory(String user, String password, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                String.format("http://chat.2day.sg/spika/v1/user/rooms/%s", user),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }

    public void RemoveChatHistory(String user, String password, String roomId, final JsonObjectResultInterface callback) throws JSONException {

        if (CheckNetworkConnectivity() == false) {
            if (callback != null) {
                callback.onErrorResponse(mCtx.getResources().getString(R.string.network_disconnected));
            }
            return;
        }

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                String.format(REMOVE_CHAT_HISTORY_REQUEST, roomId),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callback != null) {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onErrorResponse(error.getLocalizedMessage());
                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(request);
    }
}
