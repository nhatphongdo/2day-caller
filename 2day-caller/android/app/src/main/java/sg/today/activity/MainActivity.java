package sg.today.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.support.v7.widget.Toolbar;
import android.telecom.TelecomManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

import sg.today.AndroidBug5497Workaround;
import sg.today.MainApplication;
import sg.today.fragment.MenuFragment;
import sg.R;
import sg.today.model.Contact;
import sg.today.model.Profile;
import sg.today.service.ApiServices;
import sg.today.service.ContactService;
import sg.today.service.ContextHelper;
import sg.today.service.DatabaseAdapter;
import sg.today.service.JsonObjectResultInterface;
import sg.today.service.OnContactSelectionListener;

public class MainActivity extends BaseActivity
        implements MenuFragment.OnMenuClickedListener, OnContactSelectionListener {

    private DatabaseAdapter mDatabase;

    private MainApplication mMainApplication;
    private int mFeature;
    private MenuFragment menuFragment;

    private CircularImageView avatar;
    private TextView name;
    private TextView account;
    private TextView email;

    private final static int PERMISSIONS_REQUEST_LOCATIONS = 1;
    private final static int PERMISSIONS_REQUEST_RECORD_AUDIO = 2;
    private final static int PERMISSIONS_REQUEST_ALL = 255;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AndroidBug5497Workaround.assistActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitleTextColor(ContextCompat.getColor(getContext(), R.color.toolbar_title));
        toolbar.setSubtitleTextColor(ContextCompat.getColor(getContext(), R.color.toolbar_title));

        mDatabase = new DatabaseAdapter(getContext());
        mDatabase.open();

        ImageButton editLayout = (ImageButton) toolbar.findViewById(R.id.edit_button);
        editLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CustomizeLayoutActivity.class);
                startActivity(intent);
            }
        });

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        //DrawerArrowDrawable menuDrawble = toggle.getDrawerArrowDrawable();
        //menuDrawble.setColor(ContextCompat.getColor(getContext(), R.color.toolbar_title));
        //toggle.setDrawerArrowDrawable(menuDrawble);

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //navigationView.setNavigationItemSelectedListener(this);

        avatar = (CircularImageView) navigationView.findViewById(R.id.avatar);
        name = (TextView) navigationView.findViewById(R.id.name);
        account = (TextView) navigationView.findViewById(R.id.account);
        email = (TextView) navigationView.findViewById(R.id.email);
        setInfo();

        Button profileButton = (Button) findViewById(R.id.nav_profile);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(getContext(), ProfileActivity.class));
            }
        });
        Button logoutButton = (Button) findViewById(R.id.logout_button);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                MainApplication.getInstance().quit();
                ContextHelper.clearAccount(getContext());
                ContextHelper.setRemember(getContext(), false);
                finish();
                startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });
        Button rebuildContacts = (Button) findViewById(R.id.rebuild_contacts_button);
        rebuildContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                new android.support.v7.app.AlertDialog.Builder(getContext())
                        .setTitle(R.string.import_contacts)
                        .setMessage("We will use your contacts' email address or mobile number to determine 2Day user. Do you want to continue to import contacts?")
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                importContacts();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();

            }
        });
        Button buyCreditButton = (Button) findViewById(R.id.buy_credit_button);
        buyCreditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(getContext(), BuyCreditActivity.class));
            }
        });
        Button transferCreditButton = (Button) findViewById(R.id.transfer_credit_button);
        transferCreditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(getContext(), TransferCreditActivity.class));
            }
        });

        Button privacyButton = (Button) findViewById(R.id.privacy_policy_button);
        privacyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ApiServices.PRIVACY_POLICY)));
            }
        });
        Button termsButton = (Button) findViewById(R.id.terms_of_use_button);
        termsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ApiServices.TERMS_AND_SERVICES)));
            }
        });
        Button disclaimerButton = (Button) findViewById(R.id.disclaimer_button);
        disclaimerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ApiServices.DISCLAIMER)));
            }
        });
        Button faqButton = (Button) findViewById(R.id.faq_button);
        faqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ApiServices.FAQ)));
            }
        });
        Button noticesButton = (Button) findViewById(R.id.notices_button);
        noticesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ApiServices.NOTICES)));
            }
        });
        Button contactUsButton = (Button) findViewById(R.id.contact_us_button);
        contactUsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"admin@2day.sg"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Enquiry from 2Day android app");
                Intent mailer = Intent.createChooser(intent, null);
                mailer.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mailer);
            }
        });

        TextView version = (TextView) findViewById(R.id.version_text);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version.setText(pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        mMainApplication = MainApplication.getInstance();
        mMainApplication.setMainActivity(getContext());
        mMainApplication.online();

        menuFragment = new MenuFragment();
        displayFragment(menuFragment, getString(R.string.title_menu), false);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.SYSTEM_ALERT_WINDOW) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getContext(), new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.SYSTEM_ALERT_WINDOW,
                    Manifest.permission.READ_PHONE_STATE
            }, PERMISSIONS_REQUEST_ALL);
        }

        if (getIntent().getIntExtra("ACTION", -1) >= 0) {
            if (getIntent().getIntExtra("ACTION", 0) == 3) {
                String roomId = getIntent().getStringExtra("ROOMID");
                if (!TextUtils.isEmpty(roomId)) {
                    final String[] accounts = ContextHelper.getAccount(getContext());
                    final Profile profile = ContextHelper.getProfile(getContext());
                    ContextHelper.startChat(getContext(), roomId, accounts[0], profile.firstName + " " + profile.lastName, profile.profileImage);
                    return;
                }
            }
            onMenuClicked(getIntent().getIntExtra("ACTION", 0));
        }
    }

    public MainActivity getContext() {
        return this;
    }

    @Override
    protected void onResume() {
        ((MainApplication) getApplicationContext()).setMainActivity(this);
        super.onResume();

        if (mDatabase == null) {
            mDatabase = new DatabaseAdapter(getContext());
            mDatabase.open();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mDatabase != null) {
            mDatabase.close();
        }
        mDatabase = null;
    }

    @Override
    protected void onPause() {
        ((MainApplication) getApplicationContext()).setMainActivity(this);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void displayFragment(Fragment fragment, String title, boolean keepState) {
        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (keepState) {
                transaction.addToBackStack(null);
            }
            transaction.replace(R.id.content_frame, fragment);
            transaction.commit();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void onMenuClicked(int index) {
        mFeature = index;
        switch (index) {
            case 0:
                MainApplication.getInstance().setConferenceMode(false);
                Intent callIntent = new Intent(getContext(), CallActivity.class);
                startActivity(callIntent);
                break;
            case 1:
                MainApplication.getInstance().setContactSelectionListener(this);
                Intent contactSelectorIntent = new Intent(getContext(), ContactsSelectorActivity.class);
                contactSelectorIntent.putExtra(ContactsSelectorActivity.ONLY_SHOW_2DAY_ACCOUNTS, false);
                contactSelectorIntent.putExtra(ContactsSelectorActivity.VIDEO_CALL, false);
                contactSelectorIntent.putExtra(ContactsSelectorActivity.TITLE, "Voice Group Participants");
                startActivity(contactSelectorIntent);
                break;
            case 2:
                MainApplication.getInstance().setContactSelectionListener(this);
                Intent videoContactSelectorIntent = new Intent(getContext(), ContactsSelectorActivity.class);
                videoContactSelectorIntent.putExtra(ContactsSelectorActivity.ONLY_SHOW_2DAY_ACCOUNTS, true);
                videoContactSelectorIntent.putExtra(ContactsSelectorActivity.VIDEO_CALL, true);
                videoContactSelectorIntent.putExtra(ContactsSelectorActivity.TITLE, "Video Group Participants");
                startActivity(videoContactSelectorIntent);
                break;
            case 3:
                startActivity(new Intent(getContext(), ChatHistoryActivity.class));
                break;
            case 4:
                startActivity(new Intent(getContext(), MeetSelectorActivity.class));
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                return;
            }
            case PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                return;
            }
            case PERMISSIONS_REQUEST_ALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                return;
            }
        }
    }

    public void updateStatus(String reason) {
        getContext().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mMainApplication.isOnline()) {
                } else {
                }
            }
        });
    }

    @Override
    public void OnContactSelection(final ArrayList<Contact> contacts) {
        if (contacts.size() == 0) {
            return;
        }

        if (mFeature == 1) {
            if (contacts.size() > 9) {
                ContextHelper.showTipMessage(getContext(), getString(R.string.maximum_people_voice));
                return;
            }

            final AlertDialog progress = new ProgressDialog.Builder(getContext())
                    .setMessage("Please wait to connect to SIP server")
                    .create();
            int time = 0;
            if (!mMainApplication.isOnline()) {
                mMainApplication.online();
                time = 2000;
                progress.show();
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (progress != null && progress.isShowing()) {
                        progress.dismiss();
                    }
                    if (!MainApplication.getInstance().isOnline()) {
                        ContextHelper.showAlert(getContext(), "You Are Not Connected", getString(R.string.sip_not_connected));
                        return;
                    }
                    MainApplication.getInstance().conference(contacts, false, false);
                }
            }, time);
        } else if (mFeature == 2) {
            if (contacts.size() > 4) {
                ContextHelper.showTipMessage(getContext(), getString(R.string.maximum_people_video));
                return;
            }

            final AlertDialog progress = new ProgressDialog.Builder(getContext())
                    .setMessage("Please wait to connect to SIP server")
                    .create();
            int time = 0;
            if (!mMainApplication.isOnline()) {
                mMainApplication.online();
                time = 2000;
                progress.show();
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (progress != null && progress.isShowing()) {
                        progress.dismiss();
                    }
                    if (!MainApplication.getInstance().isOnline()) {
                        ContextHelper.showAlert(getContext(), "You Are Not Connected", getString(R.string.sip_not_connected));
                        return;
                    }
                    MainApplication.getInstance().conference(contacts, true, false);
                }
            }, time);
        } else if (mFeature == 3) {
            ContextHelper.startChat(getContext(), contacts);
        }
    }

    public void setInfo() {
        final Profile profile = ContextHelper.getProfile(getContext());
        if (!TextUtils.isEmpty(profile.profileImage)) {
            ImageLoader mImageLoader = ApiServices.getInstance(getContext()).getImageLoader();
            mImageLoader.get(profile.profileImage, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response.getBitmap() != null) {
                        avatar.setImageBitmap(response.getBitmap());
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    ColorGenerator generator = ColorGenerator.MATERIAL;
                    int color = generator.getColor(profile.firstName);
                    TextDrawable.IBuilder builder = TextDrawable.builder()
                            .beginConfig()
                            .bold()
                            .toUpperCase()
                            .endConfig()
                            .round();
                    String[] words = (profile.firstName.length() == 0 ? "Unknown" : profile.firstName).split(" ");
                    TextDrawable drawable = builder.build(words[words.length - 1].substring(0, 1), color);
                    avatar.setImageDrawable(drawable);
                }
            });
        } else {
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getColor(profile.firstName);
            TextDrawable.IBuilder builder = TextDrawable.builder()
                    .beginConfig()
                    .bold()
                    .toUpperCase()
                    .endConfig()
                    .round();
            String[] words = (profile.firstName.length() == 0 ? "Unknown" : profile.firstName).split(" ");
            TextDrawable drawable = builder.build(words[words.length - 1].substring(0, 1), color);
            avatar.setImageDrawable(drawable);
        }
        name.setText(profile.firstName + " " + profile.lastName);
        String accountText = "+" + profile.countryCode + " " + profile.cityCode + profile.mobileNumber;
        if (profile.sipAccounts.length > 0) {
            accountText += "  |  " + (getString(R.string.extension_number) + " : " + profile.sipAccounts[0].sipUsername);
        }
        account.setText(accountText);
        if (profile.username != null && profile.username.length() > 0) {
            email.setText("Room Name: " + profile.username + "\n" + profile.email);
        }
        else {
            email.setText(profile.email);
        }

        if (menuFragment != null) {
            menuFragment.updateGreeting();
        }
    }

    private void importContacts() {
        if (mDatabase == null) {
            return;
        }

        final ProgressDialog progress = ProgressDialog.show(
                getContext(),
                getString(R.string.import_contacts),
                getString(R.string.waiting), true, false);

        new Thread(new Runnable() {
            @Override
            public void run() {
                // do the thing that takes a long time
                final ArrayList<Contact> contacts = ContactService.readContacts(getContext());
                Collections.sort(contacts, new Contact.ContactComparator());

                // Load 2Day users
                ArrayList<String> mobiles = new ArrayList<>();
                for (int i = 0; i < contacts.size(); i++) {
                    for (int j = 0; j < contacts.get(i).Numbers.size(); j++) {
                        if (!TextUtils.isEmpty(contacts.get(i).Numbers.get(j).Number)) {
                            mobiles.add(contacts.get(i).Numbers.get(j).Number.replace("+", ""));
                        }
                    }
                    if (contacts.get(i).Email != null && !TextUtils.isEmpty(contacts.get(i).Email)) {
                        mobiles.add(contacts.get(i).Email);
                    }
                }

                try {
                    ContextHelper.checkSipAccounts(getContext(), contacts, mDatabase, mobiles, 0, new JsonObjectResultInterface() {
                        @Override
                        public void onResponse(JSONObject response) {
                            getContext().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.dismiss();
                                }
                            });
                        }

                        @Override
                        public void onErrorResponse(String error) {
                            getContext().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.dismiss();
                                }
                            });
                        }
                    });
                } catch (Exception exc) {
                    exc.printStackTrace();
                    getContext().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progress.dismiss();
                        }
                    });
                }
            }
        }).start();
    }
}
