package sg.today.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import sg.R;
import sg.today.AndroidBug5497Workaround;
import sg.today.service.ApiServices;
import sg.today.service.ContextHelper;
import sg.today.service.JsonObjectResultInterface;

public class ForgotPasswordActivity extends BaseActivity {

    // UI references.
    private EditText mEmailView;
    private EditText mCountryCodeView;
    private EditText mCityCodeView;
    private EditText mMobileNumberView;
    private View mProgressView;
    private View mFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        AndroidBug5497Workaround.assistActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitleTextColor(ContextCompat.getColor(getContext(), R.color.toolbar_title));
        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.back_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        mEmailView = (EditText) findViewById(R.id.email);
        mEmailView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    mCountryCodeView.requestFocus();
                    return true;
                }
                else if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptSubmit();
                    return true;
                }
                return false;
            }
        });
//        mCountryCodeView = (EditText) findViewById(R.id.country_code);
//        mCountryCodeView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
//                if (id == EditorInfo.IME_ACTION_NEXT) {
//                    mCityCodeView.requestFocus();
//                    return true;
//                }
//                return false;
//            }
//        });
//        mCityCodeView = (EditText) findViewById(R.id.city_code);
//        mCityCodeView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
//                if (id == EditorInfo.IME_ACTION_NEXT) {
//                    mMobileNumberView.requestFocus();
//                    return true;
//                }
//                return false;
//            }
//        });
//        mMobileNumberView = (EditText) findViewById(R.id.mobile_number);
//        mMobileNumberView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
//                if (id == EditorInfo.IME_ACTION_DONE) {
//                    attemptSubmit();
//                    return true;
//                }
//                return false;
//            }
//        });

        mFormView = findViewById(R.id.form);
        mProgressView = findViewById(R.id.progress);

        Button mSubmitButton = (Button) findViewById(R.id.forgot_password_button);
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSubmit();
            }
        });
    }

    private ForgotPasswordActivity getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            ContextHelper.hideKeyboard(getContext());
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void attemptSubmit() {
        // Reset errors.
        mEmailView.setError(null);
//        mCountryCodeView.setError(null);
//        mCityCodeView.setError(null);
//        mMobileNumberView.setError(null);

        String email = mEmailView.getText().toString();
//        String country = mCountryCodeView.getText().toString();
//        String city = mCityCodeView.getText().toString();
//        String mobile = mMobileNumberView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_email_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

//        if (TextUtils.isEmpty(country)) {
//            mCountryCodeView.setError(getString(R.string.error_country_required));
//            focusView = mCountryCodeView;
//            cancel = true;
//        }
//
//        if (TextUtils.isEmpty(city)) {
//            mCityCodeView.setError(getString(R.string.error_city_required));
//            focusView = mCityCodeView;
//            cancel = true;
//        }
//
//        if (TextUtils.isEmpty(mobile)) {
//            mMobileNumberView.setError(getString(R.string.error_mobile_required));
//            focusView = mMobileNumberView;
//            cancel = true;
//        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            ContextHelper.hideKeyboard(getContext());
            // Show a progress spinner, and kick off a background task
            showProgress(true);
            try {
                ApiServices.getInstance(getContext()).ResetPassword(email, "", "", "", new JsonObjectResultInterface() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        if (response.optInt("result_code") == 1) {
                            // Successful
                            ContextHelper.showInfo(
                                    getContext(),
                                    getString(R.string.title_activity_forgot_password),
                                    response.optString("result_message"));
                        } else {
                            Log.e("Forgot Password", response.optString("result_code"));
                            mEmailView.setError(getString(R.string.error_incorrect_parameters));
                            mEmailView.requestFocus();
                        }
                    }

                    @Override
                    public void onErrorResponse(String error) {
                        showProgress(false);
                        // Show error
                        ContextHelper.showAlert(getContext(), getString(R.string.title_activity_forgot_password), error);
                    }
                });
            } catch (JSONException e) {
                showProgress(false);
                e.printStackTrace();
                // Show error
                ContextHelper.showAlert(
                        getContext(),
                        getString(R.string.title_activity_forgot_password),
                        getString(R.string.api_format_error));
            }
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
