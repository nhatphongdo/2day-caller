package sg.today.activity;

import android.os.Bundle;
import android.util.Log;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.ReactRootView;
import com.facebook.react.common.LifecycleState;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

import sg.BuildConfig;
import sg.today.MainApplication;


public class MeetActivity extends ReactActivity {

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        MainApplication.getInstance().setMeetActivity(this);
        return new ReactActivityDelegate(this, getMainComponentName()) {
            /**
             * {@inheritDoc}
             *
             * Overrides {@link ReactActivityDelegate#createRootView()} to
             * customize the {@link ReactRootView} with a background color that
             * is in accord with the JavaScript and iOS parts of the application
             * and causes less perceived visual flicker than the default
             * background color.
             */
            @Override
            protected ReactRootView createRootView() {
                ReactRootView rootView = super.createRootView();
                rootView.setBackgroundColor(0xFF111111);
                return rootView;
            }

            @Override
            protected ReactNativeHost getReactNativeHost() {
                return MainApplication.getInstance().getReactNativeHost();
            }

            @Override
            protected Bundle getLaunchOptions() {
                Bundle params = new Bundle();
                params.putString("room", getContext().getIntent().getStringExtra("Room"));
                return params;
            }
        };
    }

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "2Day";
    }

    private MeetActivity getContext()  {
        return this;
    }
}
