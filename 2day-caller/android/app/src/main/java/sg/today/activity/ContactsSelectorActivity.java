package sg.today.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.TextView;

import com.allen.expandablelistview.SwipeMenuExpandableListView;

import java.util.ArrayList;

import sg.today.AndroidBug5497Workaround;
import sg.today.MainApplication;
import sg.R;
import sg.today.adapter.ContactsSelectorListAdapter;
import sg.today.model.Contact;
import sg.today.service.ContextHelper;
import sg.today.service.DatabaseAdapter;

public class ContactsSelectorActivity extends BaseActivity {

    public static String VIDEO_CALL = "VIDEO_CALL";
    public static String ONLY_SHOW_2DAY_ACCOUNTS = "ONLY_SHOW_2DAY_ACCOUNTS";
    public static String TITLE = "TITLE";

    private DatabaseAdapter mDatabase;
    private EditText mSearchEdit;
    private SwipeMenuExpandableListView mContactsListView;
    private ArrayList<Contact> contactsList = new ArrayList<Contact>();
    private ContactsSelectorListAdapter adapter;
    private boolean mIsVideo;
    private boolean mShow2DayAccounts;
    private TextView mTitleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_selector);

        AndroidBug5497Workaround.assistActivity(this);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mDatabase = new DatabaseAdapter(getContext());
        mDatabase.open();

        mIsVideo = getIntent().getBooleanExtra(VIDEO_CALL, false);
        mShow2DayAccounts = getIntent().getBooleanExtra(ONLY_SHOW_2DAY_ACCOUNTS, false);

        mTitleView = (TextView) findViewById(R.id.title_view);
        mSearchEdit = (EditText) findViewById(R.id.search_contact);

        mTitleView.setText(getIntent().getStringExtra(TITLE) == null ? "" : getIntent().getStringExtra(TITLE));

        if (mShow2DayAccounts) {
            contactsList = mDatabase.getAllSipContacts();
        } else {
            contactsList = mDatabase.getAllContacts();
        }
        mContactsListView = (SwipeMenuExpandableListView) findViewById(R.id.contacts_list);
        adapter = new ContactsSelectorListAdapter(getContext(), contactsList);
        mContactsListView.setAdapter(adapter);
        for (int idx = 0; idx < adapter.getGroupCount(); idx++) {
            mContactsListView.expandGroup(idx);
        }

        mSearchEdit.clearFocus();
        mSearchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_SEARCH) {
                    ContextHelper.hideKeyboard(getContext());
                    return true;
                }
                return false;
            }
        });
        mSearchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter == null) {
                    return;
                }
                adapter.getFilter().filter(charSequence, new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int i) {
                        for (int idx = 0; idx < adapter.getGroupCount(); idx++) {
                            mContactsListView.expandGroup(idx);
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        Button cancelButton = (Button) findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button doneButton = (Button) findViewById(R.id.done_button);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainApplication.getInstance().getContactSelectionListener() != null) {
                    ArrayList<Contact> selectedContacts = new ArrayList<Contact>();
                    for (int i = 0; i < contactsList.size(); i++) {
                        if (contactsList.get(i).IsSelected) {
                            selectedContacts.add(contactsList.get(i));
                        }
                    }

                    MainApplication.getInstance().getContactSelectionListener().OnContactSelection(selectedContacts);
                }
                finish();
            }
        });
    }

    private ContactsSelectorActivity getContext() {
        return this;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mDatabase == null) {
            mDatabase = new DatabaseAdapter(getContext());
            mDatabase.open();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mDatabase != null) {
            mDatabase.close();
        }
        mDatabase = null;
    }

}
