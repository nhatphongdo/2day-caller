package sg.today.model;

import android.database.Cursor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Phong Do on 11/23/2016.
 */

public class CallLog {
    public long Id;
    public long ContactId;
    public String Name;
    public String Phone;
    public String Avatar;
    public Date Timer;
    public String Status;

    public CallLog() {
    }

    public CallLog(Cursor c) {
        Id = c.getLong(0);
        ContactId = c.getLong(1);
        Phone = c.getString(2);
        Name = c.getString(3);
        Avatar = c.getString(4);
        String timer = c.getString(5);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        try {
            Timer = formatter.parse(timer);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Status = c.getString(6);
    }

    public static class CallLogTimerComparator implements Comparator<CallLog> {

        public int compare(CallLog o1, CallLog o2) {
            return o1.Timer.compareTo(o2.Timer);
        }

    }
}
