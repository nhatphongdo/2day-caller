package sg.today.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.allen.expandablelistview.BaseSwipeMenuExpandableListAdapter;
import com.baoyz.swipemenulistview.BaseSwipeListAdapter;
import com.baoyz.swipemenulistview.ContentViewWrapper;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import sg.R;
import sg.today.model.Contact;
import sg.today.model.MessageHistory;
import sg.today.service.ContextHelper;

/**
 * Created by Phong Do on 11/21/2016.
 */

public class MessagesListAdapter extends BaseSwipeListAdapter implements Filterable {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<MessageHistory> messagesList = new ArrayList<>();
    private ArrayList<MessageHistory> filterList = new ArrayList<>();

    public MessagesListAdapter(Context mContext, ArrayList<MessageHistory> messagesList) {
        this.mContext = mContext;
        this.messagesList = messagesList;
        this.filterList = messagesList;
        this.mInflater = LayoutInflater.from(mContext);
    }

    public void setMessagesList(ArrayList<MessageHistory> messagesList) {
        this.messagesList = messagesList;
        this.filterList = messagesList;
    }

    @Override
    public ContentViewWrapper getViewAndReusable(int position, View view, ViewGroup parent) {
        boolean reUsable = true;
        final MessageHistory history = getItem(position);

        if (view == null) {
            view = mInflater.inflate(R.layout.view_item_chat_history, null);
            reUsable = false;
        }

        String[] accounts = ContextHelper.getAccount(mContext);
        CircularImageView avatar = (CircularImageView) view.findViewById(R.id.avatar);
        TextView contactName = (TextView) view.findViewById(R.id.contact_name);
        TextView chatMessage = (TextView) view.findViewById(R.id.chat_message);
        TextView logTime = (TextView) view.findViewById(R.id.log_time);

        String lastName = "";
        Contact contact = new Contact();
        if (history.Users.size() <= 2) {
            for (int i = 0; i < history.Users.size(); i++) {
                if (!history.Users.get(i).userID.equalsIgnoreCase(accounts[0])) {
                    contact.Name = history.Users.get(i).name;
                    contact.Avatar = history.Users.get(i).avatarURL;
                    contactName.setText(history.Users.get(i).name);
                }
                if (history.Users.get(i).userID.equalsIgnoreCase(history.LastMessage.userID)) {
                    if (history.Users.get(i).userID.equalsIgnoreCase(accounts[0])) {
                        lastName = "Me";
                    }
                    else {
                        lastName = history.Users.get(i).name;
                    }
                }
            }
        }
        else {
            contact.Name = "Group";
            String names = "";
            for (int i = 0; i < history.Users.size(); i++) {
                if (!history.Users.get(i).userID.equalsIgnoreCase(accounts[0])) {
                    names += history.Users.get(i).name + ", ";
                }
                if (history.Users.get(i).userID.equalsIgnoreCase(history.LastMessage.userID)) {
                    if (history.Users.get(i).userID.equalsIgnoreCase(accounts[0])) {
                        lastName = "Me";
                    }
                    else {
                        lastName = history.Users.get(i).name;
                    }
                }
            }

            if (names.length() > 0) {
                contactName.setText(names.substring(0, names.length() - 2));
            }
            else {
                contactName.setText(lastName);
            }
        }

        chatMessage.setText("");
        if (history.LastMessage.message != null && history.LastMessage.message.length() > 0) {
            chatMessage.setText(lastName + ": " + history.LastMessage.message);
        }

        ContextHelper.loadAvatar(mContext, contact, "Unknown", avatar);
        CharSequence time = DateUtils.getRelativeTimeSpanString(history.LastMessage.created, (new Date()).getTime(), DateUtils.MINUTE_IN_MILLIS);
        logTime.setText(time.toString());

        return new ContentViewWrapper(view, reUsable);
    }

    @Override
    public int getCount() {
        return filterList.size();
    }

    @Override
    public MessageHistory getItem(int i) {
        return filterList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    results.values = messagesList;
                    results.count = messagesList.size();
                } else {
                    ArrayList<MessageHistory> list = new ArrayList<>();
                    for (int i = 0; i < messagesList.size(); i++) {
                        if (messagesList.get(i).LastMessage.message.toLowerCase().indexOf(constraint.toString().toLowerCase()) >= 0) {
                            list.add(messagesList.get(i));
                        }
                    }
                    results.values = list;
                    results.count = list.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterList = (ArrayList<MessageHistory>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
