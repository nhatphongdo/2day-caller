package sg.today.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.provider.ContactsContract;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

import sg.today.AndroidBug5497Workaround;
import sg.today.fragment.ContactsFragment;
import sg.today.fragment.FavoriteFragment;
import sg.today.fragment.HistoryFragment;
import sg.today.fragment.KeypadFragment;
import sg.today.MainApplication;
import sg.today.NonswipeViewPager;
import sg.R;
import sg.today.model.Profile;
import sg.today.model.SipAccount;
import sg.today.service.ApiServices;
import sg.today.service.ContextHelper;
import sg.today.service.JsonObjectResultInterface;

import static android.view.View.GONE;

public class CallActivity extends BaseActivity {

    private MainApplication mMainApplication;

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private NonswipeViewPager mViewPager;
    private ImageButton mSipStatusButton;
    private ImageButton mClearButton;
    private ImageButton mAddContactButton;
    private TabLayout tabLayout;

    private FavoriteFragment favoriteFragment;
    private HistoryFragment historyFragment;
    private KeypadFragment keypadFragment;
    private ContactsFragment contactsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        AndroidBug5497Workaround.assistActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.back_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        favoriteFragment = new FavoriteFragment();
        historyFragment = new HistoryFragment();
        keypadFragment = new KeypadFragment();
        contactsFragment = new ContactsFragment();

        mSipStatusButton = (ImageButton) findViewById(R.id.sip_status);
        mSipStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMainApplication.isOnline()) {
                    // Offline
                    mMainApplication.offline();
                } else {
                    mSipStatusButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.offline));
                    mMainApplication.online();
                }
            }
        });
        mClearButton = (ImageButton) findViewById(R.id.clear_button);
        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContextHelper.showGlobalDialog(getContext(),
                        getString(R.string.delete_call_log),
                        getString(R.string.delete_call_log_message),
                        getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (historyFragment != null) {
                                    historyFragment.clear();
                                }
                            }
                        },
                        getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        });
            }
        });
        mAddContactButton = (ImageButton) findViewById(R.id.add_contact_button);
        mAddContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                // Sets the MIME type to match the Contacts Provider
                intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                intent.putExtra("finishActivityOnSaveCompleted", true);
                startActivity(intent);
            }
        });

        mMainApplication = MainApplication.getInstance();
        mMainApplication.setCallActivity(getContext());
        updateStatus("");

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (NonswipeViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.getTabAt(position).select();
                tabLayout.getTabAt(0).setIcon(ContextCompat.getDrawable(getContext(), R.drawable.favorite));
                tabLayout.getTabAt(1).setIcon(ContextCompat.getDrawable(getContext(), R.drawable.history));
                tabLayout.getTabAt(2).setIcon(ContextCompat.getDrawable(getContext(), R.drawable.keypad));
                tabLayout.getTabAt(3).setIcon(ContextCompat.getDrawable(getContext(), R.drawable.contacts));
                mClearButton.setVisibility(GONE);
                mAddContactButton.setVisibility(GONE);
                mSipStatusButton.setVisibility(GONE);
                switch (position) {
                    case 0:
                        tabLayout.getTabAt(position).setIcon(ContextCompat.getDrawable(getContext(), R.drawable.favorite_selected));
                        setTitle(R.string.favorite);
                        break;
                    case 1:
                        tabLayout.getTabAt(position).setIcon(ContextCompat.getDrawable(getContext(), R.drawable.history_selected));
                        mClearButton.setVisibility(View.VISIBLE);
                        setTitle(R.string.history);
                        break;
                    case 2:
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (historyFragment.mSearchEdit != null) {
                            imm.hideSoftInputFromWindow(historyFragment.mSearchEdit.getWindowToken(), 0);
                        }
                        if (favoriteFragment.mSearchEdit != null) {
                            imm.hideSoftInputFromWindow(favoriteFragment.mSearchEdit.getWindowToken(), 0);
                        }
                        if (contactsFragment.mSearchEdit != null) {
                            imm.hideSoftInputFromWindow(contactsFragment.mSearchEdit.getWindowToken(), 0);
                        }

                        mSipStatusButton.setVisibility(View.VISIBLE);
                        tabLayout.getTabAt(position).setIcon(ContextCompat.getDrawable(getContext(), R.drawable.keypad_selected));
                        updateBalance();
                        break;
                    case 3:
                        tabLayout.getTabAt(position).setIcon(ContextCompat.getDrawable(getContext(), R.drawable.contacts_selected));
                        mAddContactButton.setVisibility(View.VISIBLE);
                        setTitle(R.string.contacts);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tabLayout.getTabAt(2).select();

        ContextHelper.hideKeyboard(getContext());
    }

    public CallActivity getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onResume() {
        super.onResume();

        updateStatus("");
        if (!mMainApplication.isOnline()) {
            mMainApplication.online();
        }
        updateBalance();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return favoriteFragment;
                case 1:
                    return historyFragment;
                case 2:
                    return keypadFragment;
                case 3:
                    return contactsFragment;
                default:
                    return keypadFragment;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }
    }

    public void updateStatus(String reason) {
        getContext().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mMainApplication.isOnline()) {
                    mSipStatusButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.online));
                } else {
                    mSipStatusButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.offline));
                }
            }
        });
    }

    public void updateBalance() {
        final String[] account = ContextHelper.getAccount(getContext());
        final Profile profile = ContextHelper.getProfile(getContext());
        try {
            ApiServices.getInstance(getContext()).GetSipBalance(account[0], account[1], new JsonObjectResultInterface() {
                @Override
                public void onResponse(JSONObject response) {
                    final DecimalFormat formatter = new DecimalFormat("($###,###.##)");
                    if (response.optInt("result_code") == 1) {
                        ContextHelper.setSipBalance(getContext(), (float) response.optDouble("balance", 0));
                        if (tabLayout.getSelectedTabPosition() == 2) {
                            setTitle(Html.fromHtml("Ext " + (profile.sipAccounts.length > 0 ? profile.sipAccounts[0].sipUsername : "") + " " + formatter.format(ContextHelper.getSipBalance(getContext()))));
                        }
                    } else {
                        try {
                            ApiServices.getInstance(getContext()).GetSipInfo(account[0], account[1], new JsonObjectResultInterface() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    JSONArray sips = response.optJSONArray("data");
                                    profile.sipAccounts = new SipAccount[sips.length()];
                                    for (int i = 0; i < sips.length(); i++) {
                                        profile.sipAccounts[i] = new SipAccount(sips.optJSONObject(i));
                                    }
                                    ContextHelper.setProfile(getContext(), profile);
                                    ContextHelper.setSipBalance(getContext(), profile.sipAccounts[0].sipBalance);
                                    if (tabLayout.getSelectedTabPosition() == 2) {
                                        setTitle(Html.fromHtml("Ext " + (profile.sipAccounts.length > 0 ? profile.sipAccounts[0].sipUsername : "") + " " + formatter.format(ContextHelper.getSipBalance(getContext()))));
                                    }
                                }

                                @Override
                                public void onErrorResponse(String error) {
                                    if (tabLayout.getSelectedTabPosition() == 2) {
                                        setTitle(Html.fromHtml("Ext " + (profile.sipAccounts.length > 0 ? profile.sipAccounts[0].sipUsername : "") + " " + formatter.format(ContextHelper.getSipBalance(getContext()))));
                                    }
                                }
                            });
                        } catch (JSONException e) {
                        }
                    }
                }

                @Override
                public void onErrorResponse(String error) {
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
