package sg.today.fragment;


import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.TextView;

import com.allen.expandablelistview.SwipeMenuExpandableCreator;
import com.allen.expandablelistview.SwipeMenuExpandableListView;
import com.baoyz.actionsheet.ActionSheet;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import sg.today.MainApplication;
import sg.R;
import sg.today.adapter.ContactsListAdapter;
import sg.today.model.Contact;
import sg.today.model.ContactNumber;
import sg.today.service.ApiServices;
import sg.today.service.ContactService;
import sg.today.service.ContextHelper;
import sg.today.service.DatabaseAdapter;
import sg.today.service.JsonObjectResultInterface;

public class ContactsFragment extends Fragment
        implements ActionSheet.ActionSheetListener {

    private Handler mHandler;
    private ContactService mContactService;
    private DatabaseAdapter mDatabase;

    public EditText mSearchEdit;
    private SwipeMenuExpandableListView mContactsListView;
    private ArrayList<Contact> contactsList = new ArrayList<Contact>();
    private ContactsListAdapter adapter;

    public ContactsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);
        getActivity().setTheme(R.style.ActionSheetStyleiOS7);

        mDatabase = new DatabaseAdapter(getActivity());
        mDatabase.open();

        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                //importContacts();
            }
        };
        mContactService = new ContactService(mHandler);
        getActivity().getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, mContactService);

        mSearchEdit = (EditText) view.findViewById(R.id.search_contact);

        contactsList = mDatabase.getAllContacts();

        if (contactsList.size() == 0) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.import_contacts)
                    .setMessage("We will use your contacts' email address or mobile number to determine 2Day user. Do you want to continue to import contacts?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            importContacts();
                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        }
        mContactsListView = (SwipeMenuExpandableListView) view.findViewById(R.id.contacts_list);
        adapter = new ContactsListAdapter(getActivity(), contactsList);
        mContactsListView.setAdapter(adapter);
        for (int idx = 0; idx < adapter.getGroupCount(); idx++) {
            mContactsListView.expandGroup(idx);
        }

        final SwipeMenuExpandableCreator creator = new SwipeMenuExpandableCreator() {
            @Override
            public void createGroup(SwipeMenu menu) {
            }

            @Override
            public void createChild(SwipeMenu menu) {
                if (menu.getViewType() == 0) {
                    SwipeMenuItem favorite = new SwipeMenuItem(getActivity());
                    favorite.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.right_menu_background)));
                    favorite.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                    favorite.setIcon(R.drawable.empty_heart);
                    favorite.setId(0);
                    menu.addMenuItem(favorite);
                } else {
                    SwipeMenuItem favorite = new SwipeMenuItem(getActivity());
                    favorite.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.right_menu_background)));
                    favorite.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                    favorite.setIcon(R.drawable.white_heart);
                    favorite.setId(0);
                    menu.addMenuItem(favorite);
                }
                SwipeMenuItem edit = new SwipeMenuItem(getActivity());
                edit.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.right_menu_background)));
                edit.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                edit.setIcon(R.drawable.edit);
                menu.addMenuItem(edit);
                SwipeMenuItem delete = new SwipeMenuItem(getActivity());
                delete.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.right_menu_background)));
                delete.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                delete.setIcon(R.drawable.remove);
                menu.addMenuItem(delete);
            }

            @Override
            public void createLeftChild(SwipeMenu menu) {
                SwipeMenuItem voice = new SwipeMenuItem(getActivity());
                voice.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.left_menu_background)));
                voice.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                voice.setIcon(R.drawable.small_action_call);
                voice.setId(1);
                menu.addMenuItem(voice);
                SwipeMenuItem video = new SwipeMenuItem(getActivity());
                video.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.left_menu_background)));
                video.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                video.setIcon(R.drawable.small_action_video);
                menu.addMenuItem(video);
                SwipeMenuItem chat = new SwipeMenuItem(getActivity());
                chat.setBackground(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.left_menu_background)));
                chat.setWidth(ContextHelper.dpToPx(getActivity(), 100));
                chat.setIcon(R.drawable.small_action_chat);
                menu.addMenuItem(chat);
            }
        };

        // set creator
        mContactsListView.setMenuCreator(creator);
        mContactsListView.setOnMenuItemClickListener(new SwipeMenuExpandableListView.OnMenuItemClickListenerForExpandable() {
            @Override
            public boolean onMenuItemClick(final int groupPosition, final int childPosition, SwipeMenu menu, int index) {
                if (menu.getMenuItem(0).getId() == 0) {
                    if (index == 0) {
                        // Favorite
                        Contact contact = adapter.getChild(groupPosition, childPosition);
                        mDatabase.updateLikedContact(contact.Id, !contact.IsLiked);
                        adapter.setContactsList(mDatabase.getAllContacts());
                        adapter.getFilter().filter(mSearchEdit.getText(), new Filter.FilterListener() {
                            @Override
                            public void onFilterComplete(int i) {
                                for (int idx = 0; idx < adapter.getGroupCount(); idx++) {
                                    mContactsListView.expandGroup(idx);
                                }
                            }
                        });
                    } else if (index == 1) {
                        // Edit
                        Contact contact = adapter.getChild(groupPosition, childPosition);
                        // The Cursor that contains the Contact row
                        ContentResolver cr = getActivity().getContentResolver();
                        Cursor mCursor = cr.query(ContactsContract.Contacts.CONTENT_URI,
                                null,
                                ContactsContract.Contacts._ID + " = ?",
                                new String[]{contact.ContactId},
                                null);
                        // The index of the lookup key column in the cursor
                        int mLookupKeyIndex;
                        // The index of the contact's _ID value
                        int mIdIndex;
                        // The lookup key from the Cursor
                        String mCurrentLookupKey;
                        // The _ID value from the Cursor
                        long mCurrentId;
                        // A content URI pointing to the contact
                        Uri mSelectedContactUri;

                        if (mCursor.moveToFirst()) {
                            // Gets the lookup key column index
                            mLookupKeyIndex = mCursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY);
                            // Gets the lookup key value
                            mCurrentLookupKey = mCursor.getString(mLookupKeyIndex);
                            // Gets the _ID column index
                            mIdIndex = mCursor.getColumnIndex(ContactsContract.Contacts._ID);
                            mCurrentId = mCursor.getLong(mIdIndex);
                            mSelectedContactUri = ContactsContract.Contacts.getLookupUri(mCurrentId, mCurrentLookupKey);

                            // Creates a new Intent to edit a contact
                            Intent editIntent = new Intent(Intent.ACTION_EDIT);
                            editIntent.setDataAndType(mSelectedContactUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                            editIntent.putExtra("finishActivityOnSaveCompleted", true);
                            startActivity(editIntent);
                        }

                    } else if (index == 2) {
                        // Remove
                        // Ask
                        ContextHelper.showGlobalDialog(getActivity(),
                                getString(R.string.delete_contact),
                                getString(R.string.delete_contact_message),
                                getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Contact contact = adapter.getChild(groupPosition, childPosition);
                                        mDatabase.deleteContact(contact.Id);
                                        adapter.removeItem(groupPosition, childPosition);
                                        adapter.notifyDataSetChanged();
                                    }
                                },
                                getString(R.string.no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                    }
                                });
                    }
                } else {
                    Contact contact = adapter.getChild(groupPosition, childPosition);
                    if (index == 0) {
                        // Call
                        if (!MainApplication.getInstance().isOnline()) {
                            ContextHelper.showAlert(getActivity(), "You Are Not Connected", getString(R.string.sip_not_connected));
                            return false;
                        }
                        MainApplication.getInstance().call(contact, false, false);
                    } else if (index == 1) {
                        // Video
                        if (TextUtils.isEmpty(contact.SipAccount)) {
                            ContextHelper.showTipMessage(getActivity(), getString(R.string.error_video_call_non_sip_user));
                            return false;
                        }
                        if (!MainApplication.getInstance().isOnline()) {
                            ContextHelper.showAlert(getActivity(), "You Are Not Connected", getString(R.string.sip_not_connected));
                            return false;
                        }
                        MainApplication.getInstance().call(contact, true, false);
                    } else if (index == 2) {
                        // Chat
                        if (TextUtils.isEmpty(contact.SipAccount)) {
                            ContextHelper.showTipMessage(getActivity(), getString(R.string.error_chat_non_sip_user));
                            return false;
                        }
                        ArrayList<Contact> contacts = new ArrayList<>();
                        contacts.add(contact);
                        ContextHelper.startChat(getActivity(), contacts);
                    }
                }
                return false;
            }
        });
        mContactsListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long l) {
                if (!MainApplication.getInstance().isOnline()) {
                    ContextHelper.showAlert(getActivity(), "You Are Not Connected", getString(R.string.sip_not_connected));
                    return false;
                }
                Contact contact = adapter.getChild(groupPosition, childPosition);
                //MainApplication.getInstance().call(contact, false);
                ArrayList<String> numbers = new ArrayList<>();
                if (contact.SipAccount != null && !TextUtils.isEmpty(contact.SipAccount)) {
                    numbers.add(contact.SipAccount);
                }
                for (int i = 0; i < contact.Numbers.size(); i++) {
                    numbers.add(contact.Numbers.get(i).Number);
                }
                String[] numberArray = new String[numbers.size()];
                numberArray = numbers.toArray(numberArray);
                ActionSheet.createBuilder(getActivity(), getActivity().getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles(numberArray)
                        .setCancelableOnTouchOutside(true)
                        .setListener(getCurrentContext())
                        .setTag(String.valueOf(contact.Id))
                        .show();
                return false;
            }
        });

        mSearchEdit.clearFocus();
        mSearchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_SEARCH) {
                    ContextHelper.hideKeyboard(getActivity());
                    return true;
                }
                return false;
            }
        });
        mSearchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter == null) {
                    return;
                }
                adapter.getFilter().filter(charSequence, new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int i) {
                        for (int idx = 0; idx < adapter.getGroupCount(); idx++) {
                            mContactsListView.expandGroup(idx);
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mDatabase == null) {
            mDatabase = new DatabaseAdapter(getActivity());
            mDatabase.open();
        }

        if (mHandler == null) {
            mHandler = new Handler(Looper.getMainLooper()) {
                @Override
                public void handleMessage(Message message) {
                    importContacts();
                }
            };
        }

        if (mContactService == null) {
            mContactService = new ContactService(mHandler);
            getActivity().getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, mContactService);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mContactService != null) {
            getActivity().getContentResolver().unregisterContentObserver(mContactService);
        }

        if (mDatabase != null) {
            mDatabase.close();
        }
        mDatabase = null;
    }

    public ContactsFragment getCurrentContext() {
        return this;
    }

    private void importContacts() {
        if (mDatabase == null) {
            return;
        }

        final ProgressDialog progress = ProgressDialog.show(
                getActivity(),
                getString(R.string.import_contacts),
                getString(R.string.waiting), true, false);

        new Thread(new Runnable() {
            @Override
            public void run() {
                // do the thing that takes a long time
                final ArrayList<Contact> contacts = ContactService.readContacts(getActivity());
                Collections.sort(contacts, new Contact.ContactComparator());

                // Load 2Day users
                ArrayList<String> mobiles = new ArrayList<>();
                for (int i = 0; i < contacts.size(); i++) {
                    for (int j = 0; j < contacts.get(i).Numbers.size(); j++) {
                        if (!TextUtils.isEmpty(contacts.get(i).Numbers.get(j).Number)) {
                            mobiles.add(contacts.get(i).Numbers.get(j).Number.replace("+", ""));
                        }
                    }
                    if (contacts.get(i).Email != null && !TextUtils.isEmpty(contacts.get(i).Email)) {
                        mobiles.add(contacts.get(i).Email);
                    }
                }

                try {
                    ContextHelper.checkSipAccounts(getContext(), contacts, mDatabase, mobiles, 0, new JsonObjectResultInterface() {
                        @Override
                        public void onResponse(JSONObject response) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.dismiss();
                                    adapter.setContactsList(mDatabase.getAllContacts());
                                    adapter.getFilter().filter(mSearchEdit.getText(), new Filter.FilterListener() {
                                        @Override
                                        public void onFilterComplete(int i) {
                                            for (int idx = 0; idx < adapter.getGroupCount(); idx++) {
                                                mContactsListView.expandGroup(idx);
                                            }
                                        }
                                    });
                                }
                            });
                        }

                        @Override
                        public void onErrorResponse(String error) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.dismiss();
                                }
                            });
                        }
                    });
                }
                catch (Exception exc) {
                    exc.printStackTrace();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progress.dismiss();
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        Contact contact = mDatabase.getContact(Long.parseLong(actionSheet.getTag()));
        ArrayList<String> numbers = new ArrayList<>();
        if (contact.SipAccount != null && !TextUtils.isEmpty(contact.SipAccount)) {
            numbers.add(contact.SipAccount);
        }
        for (int i = 0; i < contact.Numbers.size(); i++) {
            numbers.add(contact.Numbers.get(i).Number);
        }
        contact.Phone = numbers.get(index);
        contact.SipAccount = numbers.get(index);
        MainApplication.getInstance().call(contact, false, false);
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancle) {
    }
}
