require(["jquery.js", "react.js", "react-dom.js", "socket.io.js", "literallycanvas.js"], function (jQuery, react, reactDOM, socket, canvas) {
    // disable scrolling on touch devices so we can actually draw
    $(document).bind('touchmove', function(e) {
        if (e.target === document.documentElement) {
            return e.preventDefault();
        }
    });

    window.io = socket;

    $(document).ready(function () {
        canvas.initCanvas(document.getElementById("canvas-container"));
    })
});
