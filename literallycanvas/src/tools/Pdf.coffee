PDFJS = require '../../node_modules/pdfjs-dist/build/pdf.combined'

{ Tool } = require './base' 
{ createShape } = require '../core/shapes'

PDFJS.disableWorker = true

module.exports = class Pdf extends Tool

    name: 'Pdf'
    iconName: 'pdf'
    usesSimpleAPI: false

    renderPage = (renderCanvas, context, pdf, pageNumber) ->
        pdf.getPage pageNumber
        .then (page) ->
            space = 10
            scale = 1
            viewport = page.getViewport scale 

            # Resize canvas
            newWidth = Math.max renderCanvas.width, viewport.width
            newHeight = if renderCanvas.height == 0 then viewport.height else renderCanvas.height + viewport.height + space

            # Move canvas
            oldWidth = renderCanvas.width
            oldHeight = renderCanvas.height
            if oldWidth > 0 and oldHeight > 0
                imageData = context.getImageData 0, 0, oldWidth, oldHeight
            renderCanvas.width = newWidth
            renderCanvas.height = newHeight
            if imageData
                context.putImageData imageData, (newWidth - oldWidth) / 2, 0

            # Render PDF page into canvas context
            pageCanvas = document.createElement 'canvas'
            pageContext = pageCanvas.getContext '2d'
            pageCanvas.width = viewport.width
            pageCanvas.height = viewport.height
            renderContext = {
                canvasContext: pageContext,
                viewport: viewport
            }

            renderTask = page.render renderContext
            renderTask.then () ->
                context.drawImage pageCanvas, (newWidth - viewport.width) / 2, newHeight - viewport.height

                ++pageNumber
                self.currentImage = document.createElement 'img'
                self.currentImage.onload = () ->
                    canvasWidth = parseInt document.getElementsByClassName('lc-drawing')[0].getElementsByTagName("canvas")[0].style.width
                    self.currentShape = LC.createShape 'Image', {
                        image: self.currentImage
                        scale: 1
                        x: (canvasWidth - renderCanvas.width) / 2
                        y: 0
                    }
                    
                    self.canvas.repaintLayer 'main'
                    self.canvas.drawShapeInProgress self.currentShape

                    if pageNumber > pdf.numPages
                        document.getElementById("waiting-modal").style.display = "none"
                        self.canvas.saveShape self.currentShape
                    else
                        renderPage renderCanvas, context, pdf, pageNumber

                self.currentImage.src = renderCanvas.toDataURL()

    didBecomeActive: (lc) ->
        self.canvas = lc

        fileInput = () ->
            openFileInputName = "pdfOpenFileInput"
            return document.getElementById openFileInputName if document.getElementById openFileInputName

            fileInputElement = document.createElement 'input'
            fileInputElement.id = openFileInputName
            fileInputElement.style.visibility = 'hidden'
            fileInputElement.style.position = 'fixed'
            fileInputElement.style.top = 0
            fileInputElement.style.left = 0
            fileInputElement.setAttribute 'type', 'file'
            fileInputElement.oncontextmenu = (e) -> e.preventDefault
            fileInputElement.value = null
            document.body.appendChild fileInputElement

            $(fileInputElement).change(() ->
                return if not fileInputElement.files[0]?
                return if fileInputElement.files[0].type.toLowerCase().indexOf('application/pdf') < 0

                document.getElementById("waiting-modal").style.display = "block"
        
                fileReader = new FileReader()
                fileReader.onload = (event) ->
                    loadingTask = PDFJS.getDocument {data: atob event.target.result.substr "data:application/pdf;base64,".length}
                    loadingTask.promise.then(
                        (pdf) ->
                            # Prepare canvas
                            renderCanvas = document.createElement 'canvas'
                            context = renderCanvas.getContext '2d'
                            renderCanvas.height = 0
                            renderCanvas.width = 0

                            pageNumber = 1
                            renderPage renderCanvas, context, pdf, pageNumber
                            
                        (reason) ->
                            # PDF loading error
                            console.error(reason)
                            document.getElementById("waiting-modal").style.display = "none"
                    )
                
                fileReader.readAsDataURL fileInputElement.files[0]
            )

            fileInputElement

        fileInput().click()
