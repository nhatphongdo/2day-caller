{ Tool } = require './base' 
{ createShape } = require '../core/shapes'

module.exports = class Image extends Tool

    name: 'Image'
    iconName: 'image'
    usesSimpleAPI: false

    didBecomeActive: (lc) ->
        self.canvas = lc
        self.currentImage = null

        onPointerUp = (pt) ->
            self.currentShape = LC.createShape 'Image', {
                image: self.currentImage
                scale: 1
                x: pt.x
                y: pt.y
            }
            
            self.canvas.repaintLayer 'main'
            self.canvas.saveShape self.currentShape

        fileInput = () ->
            openFileInputName = "canvasOpenFileInput"
            return document.getElementById openFileInputName if document.getElementById openFileInputName

            fileInputElement = document.createElement 'input'
            fileInputElement.id = openFileInputName
            fileInputElement.style.visibility = 'hidden'
            fileInputElement.style.position = 'fixed'
            fileInputElement.style.top = 0
            fileInputElement.style.left = 0
            fileInputElement.setAttribute 'type', 'file'
            fileInputElement.oncontextmenu = (e) -> e.preventDefault
            fileInputElement.value = null
            document.body.appendChild fileInputElement

            $(fileInputElement).change(() ->
                return if not fileInputElement.files[0]?
                return if fileInputElement.files[0].type.toLowerCase().indexOf('image') < 0

                document.getElementById("waiting-modal").style.display = "block"
        
                fileReader = new FileReader()
                fileReader.onload = (event) ->
                    img = document.createElement 'img'
                    img.onload = () ->
                        # Resize image
                        resizeCanvas = document.createElement "canvas"
                        width = img.width
                        height = img.height
                        scale = Math.min 600.0 / width, 600.0 / height
                        resizeCanvas.width = scale * width
                        resizeCanvas.height = scale * height

                        # Scale and draw the source image to the canvas
                        resizeCanvas.getContext "2d"
                            .drawImage img, 0, 0, resizeCanvas.width, resizeCanvas.height

                        # Convert the canvas to a data URL in PNG format
                        self.currentImage = document.createElement 'img'
                        self.currentImage.onload = () ->
                            # lc.on() returns a function that unsubscribes us. capture it.
                            self.unsubscribeFuncs = [
                                lc.on 'lc-pointerup', onPointerUp
                            ]

                            document.getElementById("waiting-modal").style.display = "none"
                        self.currentImage.src = resizeCanvas.toDataURL()

                    img.src = event.target.result
                
                fileReader.readAsDataURL fileInputElement.files[0]
            )

            fileInputElement

        fileInput().click()

    willBecomeInactive: (lc) ->
        # call all the unsubscribe functions
        if self.unsubscribeFuncs?
            self.unsubscribeFuncs.map (f) -> f()
