{ Tool } = require './base' 
{ createShape } = require '../core/shapes'
filesaver = require '../../node_modules/file-saver/FileSaver.min.js'

module.exports = class Save extends Tool

    name: 'Save'
    iconName: 'save'
    usesSimpleAPI: false

    getParameterByName = (name, url) ->
        url = window.location.href if not url?
        name = name.replace /[\[\]]/g, "\\$&"
        regex = new RegExp "[?&]" + name + "(=([^&#]*)|&|#|$)"
        results = regex.exec url
        return null if not results?
        return '' if not results[2]
        decodeURIComponent results[2].replace /\+/g, " "

    didBecomeActive: (lc) ->
        self.canvas = lc
        image = lc.getImage()
        imageName = getParameterByName("room") || "collaboration"
        image.toBlob (blob) ->
            try
                filesaver.saveAs blob, imageName + ".png"
            catch exc
                console.log exc
                alert "Cannot save file."
