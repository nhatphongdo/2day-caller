const React = require('./React-shim');
const ReactDOM = require('./ReactDOM-shim');
const LiterallyCanvasModel = require('../core/LiterallyCanvas');
const LiterallyCanvasReactComponent = require('./LiterallyCanvas');
const CanvasReactComponent = React.createFactory(require('./react-component'));

function init(el, opts) {
  const originalClassName = el.className
  const lc = new LiterallyCanvasModel(opts)
  ReactDOM.render(<LiterallyCanvasReactComponent lc={lc} />, el);
  lc.teardown = function() {
    lc._teardown();
    for (var i=0; i<el.children.length; i++) {
      el.removeChild(el.children[i]);
    }
    el.className = originalClassName;
  };
  return lc;
}

function initCanvas(el) {
  ReactDOM.render(<CanvasReactComponent />, el);
}

module.exports = { init, initCanvas }
