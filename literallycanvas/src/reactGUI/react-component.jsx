const React = require('./React-shim');
const ReactDOM = require('./ReactDOM-shim');
const LiterallyCanvasReactComponent = require('./LiterallyCanvas');

const CanvasApp = React.createClass({
    getInitialState() {
        return {
            isSetUp: true,
            svgText: "",
            lcOptions: {
                toolbarPosition: 'top',
                tools: [
                    require('../tools/Pencil'),
                    require('../tools/Eraser'),
                    require('../tools/Line'),
                    require('../tools/Rectangle'),
                    require('../tools/Ellipse'),
                    require('../tools/Text'),
                    require('../tools/Polygon'),
                    require('../tools/Pan'),
                    require('../tools/Eyedropper'),
                    require('../tools/Image'),
                    require('../tools/Pdf'),
                    require('../tools/Save')
                ],
                //snapshot: JSON.parse(localStorage.getItem('drawing')),
                onInit: this.onInit,
                imageURLPrefix: "img"
            }
        };
    },

    onInit: function(lc) {
        this.lc = lc;

        $('.lc-drawing').on("mousewheel", function(e) {
            var delta = e.originalEvent.deltaY;
            if (e.shiftKey) {
                // Move horizontal
                lc.pan(delta, 0);
            }
            else {
                lc.pan(0, delta);
            }
        });

        lc.on('drawingChange', this.drawingChange);
        //lc.on('pan', this.save);
        //lc.on('zoom', this.save);
        lc.on('snapshotLoad', this.updateSize);

        lc.on('shapeSave', this.saveAndSync);
        lc.on('clear', this.saveAndSync);
        lc.on('undo', this.saveAndSync);
        lc.on('redo', this.saveAndSync);
        lc.on('backgroundColorChange', this.saveAndSync);

        // Start socket listerner
        self = this;
        self.isLoading = false;
        this.socket = window.io("http://chat.2day.sg/canvas");
        this.socket.on('connect', function(){
            this.on('socket error', function(msg) {
                console.log(msg);
            });
            this.on('joined', function(msg) {
                if (msg.snapshot) {
                    self.isLoading = true;
                    lc.loadSnapshot(JSON.parse(msg.snapshot));
                    localStorage.setItem('drawing', msg.snapshot);
                }
            });
            this.on('new update', function(msg) {
                if (msg.snapshot) {
                    self.isLoading = true;
                    lc.loadSnapshot(JSON.parse(msg.snapshot));
                    localStorage.setItem('drawing', msg.snapshot);
                }
            });
        });
        this.socket.emit("join", {
            roomID: this.getParameterByName("room") || ""
        });
    },

    save() {
        var snapshot = JSON.stringify(this.lc.getSnapshot());
        var svg = this.lc.getSVGString();
        localStorage.setItem('drawing', snapshot);
        this.setState({svgText: svg});
    },

    saveAndSync() {
        if (self.isLoading == true) {
            return;
        }

        var snapshot = this.lc.getSnapshot();
        var canvasWidth = $('.lc-drawing').width();
        var canvasHeight = $('.lc-drawing').height();
        var size = this.lc.getDefaultImageRect();
        if (canvasWidth < size.width) {
            canvasWidth = size.width;
        }
        if (canvasHeight < size.height) {
            canvasHeight = size.height;
        }
        snapshot.imageSize.width = canvasWidth * 1.2;
        snapshot.imageSize.height = canvasHeight * 1.2;

        var snapshotStr = JSON.stringify(snapshot);
        var svg = this.lc.getSVGString();
        localStorage.setItem('drawing', snapshotStr);
        this.setState({svgText: svg});
        // Synchronize to group
        this.socket.emit('update canvas', {
            roomID: this.getParameterByName("room") || "",
            snapshot: snapshotStr
        });
    },

    updateSize() {
        var canvasWidth = $('.lc-drawing').width();
        var canvasHeight = $('.lc-drawing').height();
        var size = this.lc.getDefaultImageRect();
        if (canvasWidth < size.width) {
            canvasWidth = size.width;
        }
        if (canvasHeight < size.height) {
            canvasHeight = size.height;
        }
        this.lc.setImageSize(canvasWidth * 1.2, canvasHeight * 1.2);

        self.isLoading = false;
    },

    drawingChange() {
        this.updateSize();
        this.save();
    },

    render() {
        return <div style={{position: "relative"}}>
            {<LiterallyCanvasReactComponent {...this.state.lcOptions} />}
            <div id="waiting-modal" style={{
                background: "#000",
                opacity: 0.5,
                position: "absolute",
                left: 0,
                top: 0,
                width: "100%",
                height: "100%",
                cursor: "wait",
                zIndex: 9999,
                display: "none"
            }}></div>
        </div>;
    },
    

    getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
});

module.exports = CanvasApp
