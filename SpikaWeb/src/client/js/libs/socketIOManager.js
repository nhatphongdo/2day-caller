var socket = require('socket.io-client');
var Backbone = require('backbone');
var _ = require('lodash');
var CONST = require('../consts');
var Config = require('../init');
var ErrorDialog = require('../Views/Modals/ErrorDialog/ErrorDialog');

(function (global) {
    "use strict;"

    var socketIOManager = {

        io: null,

        init: function () {

            this.io = socket.connect(Config.socketUrl);


            this.io.on('socketerror', function (param) {

                if (param.code) {

                    ErrorDialog.show('Error', CONST.ERROR_CODES[param.code]);

                } else {
                    ErrorDialog.show('Error', 'Unknown Error');
                }


            });

            this.io.on('newUser', function (param) {
                Backbone.trigger(CONST.EVENT_ON_LOGIN_NOTIFY, param);

                // Post message for CORS case
                window.parent.postMessage({
                    event: 'newUser',
                    param: param
                }, "*");

                // call listener
                try {
                    if (!_.isEmpty(window.parent.SpikaAdapter) &&
                        !_.isEmpty(window.parent.SpikaAdapter.listener)) {

                        var listener = window.parent.SpikaAdapter.listener;

                        if (_.isFunction(listener.onNewUser))
                            listener.onNewMessage(param);

                    }
                } catch (error) {
                    console.log(error);
                }
            });

            this.io.on('userLeft', function (param) {

                Backbone.trigger(CONST.EVENT_ON_LOGOUT_NOTIFY, param);

                // Post message for CORS case
                window.parent.postMessage({
                    event: 'userLeft',
                    param: param
                }, "*");

                // call listener
                try {
                    if (!_.isEmpty(window.parent.SpikaAdapter) &&
                        !_.isEmpty(window.parent.SpikaAdapter.listener)) {

                        var listener = window.parent.SpikaAdapter.listener;

                        if (_.isFunction(listener.onUserLeft))
                            listener.onUserLeft(param);

                    }
                } catch (error) {
                    console.log(error);
                }
            });

            this.io.on('newMessage', function (param) {

                Backbone.trigger(CONST.EVENT_ON_MESSAGE, param);

                // Post message for CORS case
                window.parent.postMessage({
                    event: 'newMessage',
                    param: param
                }, "*");

                // call listener
                try {
                    if (!_.isEmpty(window.parent.SpikaAdapter) &&
                        !_.isEmpty(window.parent.SpikaAdapter.listener)) {

                        var listener = window.parent.SpikaAdapter.listener;

                        if (_.isFunction(listener.onNewMessage))
                            listener.onNewMessage(param);

                    }
                } catch (error) {
                    console.log(error);
                }
            });

            this.io.on('sendTyping', function (param) {

                Backbone.trigger(CONST.EVENT_ON_TYPING, param);

                // Post message for CORS case
                window.parent.postMessage({
                    event: 'sendTyping',
                    param: param
                }, "*");

                // call listener
                try {
                    if (!_.isEmpty(window.parent.SpikaAdapter) &&
                        !_.isEmpty(window.parent.SpikaAdapter.listener)) {

                        var listener = window.parent.SpikaAdapter.listener;

                        if (_.isFunction(listener.OnUserTyping))
                            listener.OnUserTyping(param);

                    }
                } catch (error) {
                    console.log(error);
                }
            });

            this.io.on('login', function (param) {

                Backbone.trigger(CONST.EVENT_ON_LOGIN, param);

                // Post message for CORS case
                window.parent.postMessage({
                    event: 'login',
                    param: param
                }, "*");

                // call listener
                try {
                    if (!_.isEmpty(window.parent.SpikaAdapter) &&
                        !_.isEmpty(window.parent.SpikaAdapter.listener)) {

                        var listener = window.parent.SpikaAdapter.listener;

                        if (_.isFunction(listener.onLogin))
                            listener.onLogin(param);

                    }
                } catch (error) {
                    console.log(error);
                }
            });

            this.io.on('logout', function (param) {
                Backbone.trigger(CONST.EVENT_ON_LOGOUT, param);

                // Post message for CORS case
                window.parent.postMessage({
                    event: 'logout',
                    param: param
                }, "*");

                // call listener
                try {
                    if (!_.isEmpty(window.parent.SpikaAdapter) &&
                        !_.isEmpty(window.parent.SpikaAdapter.listener)) {

                        var listener = window.parent.SpikaAdapter.listener;

                        if (_.isFunction(listener.onLogout))
                            listener.onLogout(param);

                    }
                } catch (error) {
                    console.log(error);
                }
            });

            this.io.on('messageUpdated', function (param) {
                Backbone.trigger(CONST.EVENT_ON_MESSAGE_UPDATED, param);

                // Post message for CORS case
                window.parent.postMessage({
                    event: 'messageUpdated',
                    param: param
                }, "*");

                // call listener
                try {
                    if (!_.isEmpty(window.parent.SpikaAdapter) &&
                        !_.isEmpty(window.parent.SpikaAdapter.listener)) {

                        var listener = window.parent.SpikaAdapter.listener;

                        if (_.isFunction(listener.OnMessageChanges))
                            listener.OnMessageChanges(param);

                    }
                } catch (error) {
                    console.log(error);
                }
            });


        },

        emit: function (command, params) {

            var command = arguments[0];
            this.io.emit(command, params);

        }

    };

    // Exports ----------------------------------------------
    module["exports"] = socketIOManager;

})((this || 0).self || global);