var _ = require('lodash');

var CanvasUsersManager = {
    
    rooms:{},
    addUser: function(roomID,socketID){
        
        var user = {
            roomID: roomID,
            socketID: socketID
        };
         
        if(_.isUndefined(this.rooms[roomID])){
            this.rooms[roomID] = {};
        }

        if(_.isEmpty(this.rooms[roomID])){
            this.rooms[roomID] = {
                snapshot: "",
                users:{}
            };
        }
                        
        if(_.isUndefined(this.rooms[roomID].users[socketID]))
            this.rooms[roomID].users[socketID] = user;
        
        this.rooms[roomID].users[socketID] = user;
                
    },
    removeUser: function(roomID,socketID){
                
        delete this.rooms[roomID].users[socketID];
                
    },
    getUsers: function(roomID){
        
        if(!this.rooms[roomID])
            this.rooms[roomID] = {};
            
        var users = this.rooms[roomID].users;
                
        // change to array
        var usersAry = [];
        
        _.forEach(users, function(row, key) {
                                
            usersAry.push(row);
            
        });
            
        return usersAry;
        
    },
    updateSnapshot: function(roomID,snapshot) {
        if(!this.rooms[roomID])
            this.rooms[roomID] = {snapshot: ""};
        
        this.rooms[roomID].snapshot = snapshot;
    },
    getSnapshot: function(roomID) {
        if(!this.rooms[roomID])
            return "";
        
        return this.rooms[roomID].snapshot;
    }
}

module["exports"] = CanvasUsersManager;