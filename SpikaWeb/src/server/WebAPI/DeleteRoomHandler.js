var express = require('express');
var router = express.Router();
var bodyParser = require("body-parser");
var path = require('path');
var _ = require('lodash');

var RequestHandlerBase = require("./RequestHandlerBase");
var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");

var async = require('async');
var formidable = require('formidable');
var fs = require('fs-extra');
var path = require('path');
var mime = require('mime');

var MessageModel = require("../Models/MessageModel");

var DeleteRoomHandler = function(){
    
}

_.extend(DeleteRoomHandler.prototype,RequestHandlerBase.prototype);

DeleteRoomHandler.prototype.attach = function(route){
        
    var self = this;

    route.get('/:roomID',function(request,response){
        
        var roomID = request.params.roomID;
        if(_.isEmpty(roomID)){
            self.successResponse(response, Const.responsecodeParamError);
            return;
        }

        MessageModel.removeRoom(roomID, function (err) {
            if (err) {
                self.errorResponse(response, Const.httpCodeSeverError);
            }
            else {
                self.successResponse(response,Const.httpCodeSucceed,{});
            }
        });
        
    });

}

new DeleteRoomHandler().attach(router);
module["exports"] = router;
