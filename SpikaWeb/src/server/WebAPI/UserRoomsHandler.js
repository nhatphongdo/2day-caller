var express = require('express');
var router = express.Router();
var bodyParser = require("body-parser");
var path = require('path');
var _ = require('lodash');

var RequestHandlerBase = require("./RequestHandlerBase");
var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");
var async = require('async');
var formidable = require('formidable');
var fs = require('fs-extra');
var path = require('path');
var mime = require('mime');
var SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');
var MessageModel = require("../Models/MessageModel");
var tokenChecker = require('../lib/Auth');

var UserRoomsHandler = function(){
    
}

_.extend(UserRoomsHandler.prototype,RequestHandlerBase.prototype);

UserRoomsHandler.prototype.attach = function(router){
        
    var self = this;

    /**
     * @api {get} /user/rooms/:userID  Get List of Users in room
     * @apiName Get Rooms List of specific user
     * @apiGroup WebAPI
     * @apiDescription Get list of rooms of specific user

     * @apiParam {String} userID ID of user
     *
     *     
     * @apiSuccessExample Success-Response:
{
  "code": 1,
  "data": [
    "test",
    "test"
  ]
}
    */
    router.get('/:userID',function(request,response){
      
        var userID = request.params.userID;
        if(_.isEmpty(userID)){
          self.successResponse(response,Const.resCodeRoomListNoUserID);
          return;
        }

        MessageModel.findRooms(userID,function(err, rooms) {
          async.map(rooms, function(roomId, callback) {
            MessageModel.findUsersOfRoom(roomId,function(err, users) {
              if (err) {
                console.log(err)
              }
              MessageModel.findLastMessage(roomId,function (msgErr,data) {
                if (msgErr) {
                  console.log(msgErr);
                }
                callback(msgErr, {
                  roomID: roomId,
                  users: users,
                  lastMessage: data[0]
                })
              });
            });
          }
          , function(err, results) {

            results = _.orderBy(results, [function(m) { return m.lastMessage ? m.lastMessage.created : 0; }], ['desc']);

            self.successResponse(response,
              Const.responsecodeSucceed,
              Utils.stripPrivacyParamsFromArray(results));
          });

        });
    });


}

new UserRoomsHandler().attach(router);
module["exports"] = router;
