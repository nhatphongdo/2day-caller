var express = require('express');
var router = express.Router();
var bodyParser = require("body-parser");
var path = require('path');
var _ = require('lodash');
var Request = require('request');

var RequestHandlerBase = require("./RequestHandlerBase");
var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");
var Config = require("../init");
var async = require('async');
var formidable = require('formidable');
var fs = require('fs-extra');
var path = require('path');
var mime = require('mime');
var SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');

var StickerListHandler = function () {

}

_.extend(StickerListHandler.prototype, RequestHandlerBase.prototype);

StickerListHandler.prototype.attach = function (router) {

	var self = this;

	/**
     * @api {get} /stickers Sticker List
     * @apiName Sticker List
     * @apiGroup WebAPI
     * @apiDescription Return list of stickers

     * @apiSuccessExample Success-Response:

{
	"code": 1,
	"data": {
		"stickers": [{
			"mainPic": "http://spika.chat/api/v2/sticker/b9jrzYulNunQrfFIgy9Xiv2w3SVB1Zpy",
			"list": [{
				"fullPic": "http://spika.chat/api/v2/sticker/8FzKKTcwl8YMJWIizBluXnlGBO3Zwpj2",
				"smallPic": "http://spika.chat/api/v2/sticker/b9jrzYulNunQrfFIgy9Xiv2w3SVB1Zpy"
			}, {
				"fullPic": "http://spika.chat/api/v2/sticker/19hxo8g5KhFgQLrzbziWSmmoCV7aCykg",
				"smallPic": "http://spika.chat/api/v2/sticker/3aqSVUCxP5fvI2PraJ9DoumHYu9oSKGg"
			}, {
				"fullPic": "http://spika.chat/api/v2/sticker/QjT3MDOiecxMFTqCHGH9Q4WIrmmiaoqP",
				"smallPic": "http://spika.chat/api/v2/sticker/QoZ20LPd7JRtPSsEfhCx86I3lqs0aOWw"
			}, {
				"fullPic": "http://spika.chat/api/v2/sticker/Qof2yrFvW5BstguLA7zQOG2VV5XC07TZ",
				"smallPic": "http://spika.chat/api/v2/sticker/c48qmefZEzjxGK8Bv3YaKOzKqMxPHU6y"
			}, {
				"fullPic": "http://spika.chat/api/v2/sticker/Oax2l7xCGeZ0SjR4WZuFsslnN4375Mlj",
				"smallPic": "http://spika.chat/api/v2/sticker/rUDFHI3BJDJOCFmnKhm90xzCPXWX4M2i"
			}, {

				"fullPic": "http://spika.chat/api/v2/sticker/vv8tszaB5qZ9uHuWeX2lZBjuiku5c8n9",
				"smallPic": "http://spika.chat/api/v2/sticker/m5UpjDqxDrcn43dBfrEWfDAbTAkLO24e"
			}, {
				"fullPic": "http://spika.chat/api/v2/sticker/kPoQiYSOaFIzg47zPBiNQoZaukXZYB0o",
				"smallPic": "http://spika.chat/api/v2/sticker/kYJDCQqyThkwmf6zuagkN0k6bkwzwPyu"
			}]
		}, {
			"mainPic": "http://spika.chat/api/v2/sticker/pQMwOMxxkdECAlfqJbOvOoITiUaL6ZJT",
			"list": [{
				"fullPic": "http://spika.chat/api/v2/sticker/T4gIS2iC6gbOv75889oE31GbnCEJ4OgR",
				"smallPic": "http://spika.chat/api/v2/sticker/pQMwOMxxkdECAlfqJbOvOoITiUaL6ZJT"
			}, {
				"fullPic": "http://spika.chat/api/v2/sticker/7eZzmei9Pwut6nsosFYmgJ05N7olP9Mo",
				"smallPic": "http://spika.chat/api/v2/sticker/aAckl7IhnJ28U8U7bnDSALGl7EvhGgND"
			}, {
				"fullPic": "http://spika.chat/api/v2/sticker/gS52BAxfOJZAP7CN9HLyFzXcjhF6Vgxa",
				"smallPic": "http://spika.chat/api/v2/sticker/tc4OG0zCt8pTxlhmJkxUNvET8mmLEjlx"
			}, {

				"fullPic": "http://spika.chat/api/v2/sticker/oSp6ltfWHkC5Kb7SjgIVSuUR72W1DdRo",
				"smallPic": "http://spika.chat/api/v2/sticker/vPYb8BuoDjjDkgfFIeNNDiNfH5Sd5PtB"
			}, {
				"fullPic": "http://spika.chat/api/v2/sticker/9SOVxUFQOTZvSy2rw1xA51c5Z2aAZRr9",
				"smallPic": "http://spika.chat/api/v2/sticker/N9yYXMJF7E5y8PSyiVQCYXmUSDuQfCZg"
			}, {
				"fullPic": "http://spika.chat/api/v2/sticker/Kga2eWk4xJv1lmC1YnnKGhoJM3e9hjhI",
				"smallPic": "http://spika.chat/api/v2/sticker/tnbe3Tksib3invpGFno9wVslSUnOVZml"
			}]
		}]
	}
}
    */

	router.get('', function (request, response) {

		/*
		        Request(Config.stickerAPI, function (error, responseAPI, body) {
		            
		            if (!error && responseAPI.statusCode == 200) {
		                
		                var obj = JSON.parse(body);
		                
		                if(obj && obj.data && obj.data.stickers){
		                    
		                    var list = obj.data.stickers;
		                    
		                    var mappedList = _.map(list,function(group){
		                        
		                        var stickers = {
		                            
		                            mainPic : Config.stickerBaseURL + group.mainTitlePic
		                            
		                              
		                        };
		                        
		                        var list = _.map(group.list,function(row){
		                            
		                            return {
		                                fullPic:Config.stickerBaseURL + row.fullPic,
		                                smallPic:Config.stickerBaseURL + row.smallPic
		                            }
		                             
		                        });
		                        
		                        stickers.list = list;
		                        
		                        return stickers;
		                            
		                    });
		                    
		                    self.successResponse(response,Const.responsecodeSucceed,{
		                        stickers:mappedList
		                    });
		                    
		                }else{
		                    
		                    self.successResponse(response,Const.resCodeStickerListFailed);
		                    
		                }

		            }else{
		                self.successResponse(response,Const.resCodeStickerListFailed);
		            }
		            
		        });
		*/

		var baseImage = "http://" + Config.host + ":" + Config.port + Config.urlPrefix + "/emojis/";
		var baseSmallImage = "http://" + Config.host + ":" + Config.port + Config.urlPrefix + "/emojis_small/";

		self.successResponse(response, Const.responsecodeSucceed, {
			stickers: [{
				"name": "Faces",
				"list": [{
						"fullPic": baseImage + "face/Alien_Emoji.png",
						"smallPic": baseSmallImage + "face/Alien_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Angry_Devil_Emoji.png",
						"smallPic": baseSmallImage + "face/Angry_Devil_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Angry_Emoji.png",
						"smallPic": baseSmallImage + "face/Angry_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Anguished_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Anguished_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Astonished_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Astonished_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Blow_Kiss_Emoji.png",
						"smallPic": baseSmallImage + "face/Blow_Kiss_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Cold_Sweat_Emoji.png",
						"smallPic": baseSmallImage + "face/Cold_Sweat_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Confounded_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Confounded_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Confused_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Confused_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Crying_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Crying_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Disappointed_but_Relieved_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Disappointed_but_Relieved_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Disappointed_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Disappointed_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Dizzy_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Dizzy_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Emoji_Face_without_Mouth.png",
						"smallPic": baseSmallImage + "face/Emoji_Face_without_Mouth.png"
					},
					{
						"fullPic": baseImage + "face/Expressionless_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Expressionless_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Face_with_Cold_Sweat_Emoji.png",
						"smallPic": baseSmallImage + "face/Face_with_Cold_Sweat_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Face_With_Head-Bandage_Emoji.png",
						"smallPic": baseSmallImage + "face/Face_With_Head-Bandage_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Face_With_Rolling_Eyes_Emoji.png",
						"smallPic": baseSmallImage + "face/Face_With_Rolling_Eyes_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Face_With_Thermometer_Emoji.png",
						"smallPic": baseSmallImage + "face/Face_With_Thermometer_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Fearful_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Fearful_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Flushed_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Flushed_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Frowning_Face_with_Open_Mouth_Emoji.png",
						"smallPic": baseSmallImage + "face/Frowning_Face_with_Open_Mouth_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Ghost_Emoji.png",
						"smallPic": baseSmallImage + "face/Ghost_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Grinmacing_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Grinmacing_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Grinning_Emoji_with_Smiling_Eyes.png",
						"smallPic": baseSmallImage + "face/Grinning_Emoji_with_Smiling_Eyes.png"
					},
					{
						"fullPic": baseImage + "face/Heart_Eyes_Emoji.png",
						"smallPic": baseSmallImage + "face/Heart_Eyes_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Hugging_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Hugging_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Hungry_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Hungry_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Hushed_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Hushed_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Kissing_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Kissing_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Kissing_Face_with_Smiling_Eyes_Emoji.png",
						"smallPic": baseSmallImage + "face/Kissing_Face_with_Smiling_Eyes_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Kiss_Emoji_with_Closed_Eyes.png",
						"smallPic": baseSmallImage + "face/Kiss_Emoji_with_Closed_Eyes.png"
					},
					{
						"fullPic": baseImage + "face/Loudly_Crying_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Loudly_Crying_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Money_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Money_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Nerd_with_Glasses_Emoji.png",
						"smallPic": baseSmallImage + "face/Nerd_with_Glasses_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Neutral_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Neutral_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/OMG_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/OMG_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Persevering_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Persevering_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Poop_Emoji.png",
						"smallPic": baseSmallImage + "face/Poop_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Relieved_Emoji.png",
						"smallPic": baseSmallImage + "face/Relieved_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Sad_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Sad_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Shyly_Smiling_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Shyly_Smiling_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Sick_Emoji.png",
						"smallPic": baseSmallImage + "face/Sick_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Sleeping_Emoji.png",
						"smallPic": baseSmallImage + "face/Sleeping_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Sleeping_with_Snoring_Emoji.png",
						"smallPic": baseSmallImage + "face/Sleeping_with_Snoring_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Slightly_Smiling_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Slightly_Smiling_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Smiling_Devil_Emoji.png",
						"smallPic": baseSmallImage + "face/Smiling_Devil_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Smiling_Emoji_with_Eyes_Opened.png",
						"smallPic": baseSmallImage + "face/Smiling_Emoji_with_Eyes_Opened.png"
					},
					{
						"fullPic": baseImage + "face/Smiling_Emoji_with_Smiling_Eyes.png",
						"smallPic": baseSmallImage + "face/Smiling_Emoji_with_Smiling_Eyes.png"
					},
					{
						"fullPic": baseImage + "face/Smiling_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Smiling_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Smiling_Face_Emoji_with_Blushed_Cheeks.png",
						"smallPic": baseSmallImage + "face/Smiling_Face_Emoji_with_Blushed_Cheeks.png"
					},
					{
						"fullPic": baseImage + "face/Smiling_Face_with_Halo.png",
						"smallPic": baseSmallImage + "face/Smiling_Face_with_Halo.png"
					},
					{
						"fullPic": baseImage + "face/Smiling_Face_with_Tightly_Closed_eyes.png",
						"smallPic": baseSmallImage + "face/Smiling_Face_with_Tightly_Closed_eyes.png"
					},
					{
						"fullPic": baseImage + "face/Smiling_with_Sweat_Emoji.png",
						"smallPic": baseSmallImage + "face/Smiling_with_Sweat_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Smirk_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Smirk_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Sunglasses_Emoji.png",
						"smallPic": baseSmallImage + "face/Sunglasses_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Surprised_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Surprised_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Tears_of_Joy_Emoji.png",
						"smallPic": baseSmallImage + "face/Tears_of_Joy_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Thinking_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Thinking_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Tired_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Tired_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Tongue_Out_Emoji.png",
						"smallPic": baseSmallImage + "face/Tongue_Out_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Tongue_Out_Emoji_with_Tightly_Closed_Eyes.png",
						"smallPic": baseSmallImage + "face/Tongue_Out_Emoji_with_Tightly_Closed_Eyes.png"
					},
					{
						"fullPic": baseImage + "face/Tongue_Out_Emoji_with_Winking_Eye.png",
						"smallPic": baseSmallImage + "face/Tongue_Out_Emoji_with_Winking_Eye.png"
					},
					{
						"fullPic": baseImage + "face/Unamused_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Unamused_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Upside-Down_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Upside-Down_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Very_Angry_Emoji.png",
						"smallPic": baseSmallImage + "face/Very_Angry_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Very_Mad_Emoji.png",
						"smallPic": baseSmallImage + "face/Very_Mad_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Very_sad_emoji_icon_png.png",
						"smallPic": baseSmallImage + "face/Very_sad_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "face/Weary_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Weary_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Wink_Emoji.png",
						"smallPic": baseSmallImage + "face/Wink_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Worried_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Worried_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "face/Zipper-Mouth_Face_Emoji.png",
						"smallPic": baseSmallImage + "face/Zipper-Mouth_Face_Emoji.png"
					}
				],
				"mainPic": baseSmallImage + "face/Smiling_Emoji_with_Eyes_Opened.png"
			}, {
				"name": "People",
				"list": [{
						"fullPic": baseImage + "people/Beating_Pink_Heart_Emoji.png",
						"smallPic": baseSmallImage + "people/Beating_Pink_Heart_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Bride_With_Veil_Woman_Emoji.png",
						"smallPic": baseSmallImage + "people/Bride_With_Veil_Woman_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Broken_Red_Heart_Emoji.png",
						"smallPic": baseSmallImage + "people/Broken_Red_Heart_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Clapping_Hands_Emoji.png",
						"smallPic": baseSmallImage + "people/Clapping_Hands_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Dancer_With_Red_Dress_Emoji.png",
						"smallPic": baseSmallImage + "people/Dancer_With_Red_Dress_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Eyes_Emoji.png",
						"smallPic": baseSmallImage + "people/Eyes_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Fist_Hand_Emoji.png",
						"smallPic": baseSmallImage + "people/Fist_Hand_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Flexing_Muscles_Emoji.png",
						"smallPic": baseSmallImage + "people/Flexing_Muscles_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Growing_Pink_Heart_Emoji.png",
						"smallPic": baseSmallImage + "people/Growing_Pink_Heart_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/High_Five_Emoji.png",
						"smallPic": baseSmallImage + "people/High_Five_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Man_Bowing_Emoji.png",
						"smallPic": baseSmallImage + "people/Man_Bowing_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Man_Rowing_Emoji.png",
						"smallPic": baseSmallImage + "people/Man_Rowing_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Man_Running_Emoji.png",
						"smallPic": baseSmallImage + "people/Man_Running_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Man_Walking_Emoji.png",
						"smallPic": baseSmallImage + "people/Man_Walking_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Middle_Finger_Emoji.png",
						"smallPic": baseSmallImage + "people/Middle_Finger_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Nose_Emoji.png",
						"smallPic": baseSmallImage + "people/Nose_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Ok_Hand_Sign_Emoji.png",
						"smallPic": baseSmallImage + "people/Ok_Hand_Sign_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/One_Ear_Emoji.png",
						"smallPic": baseSmallImage + "people/One_Ear_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Part_Between_Middle_and_Ring_Fingers_Emoji.png",
						"smallPic": baseSmallImage + "people/Part_Between_Middle_and_Ring_Fingers_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Pink_Heart_With_Arrow_Emoji.png",
						"smallPic": baseSmallImage + "people/Pink_Heart_With_Arrow_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Pink_Heart_With_Ribbon_Emoji.png",
						"smallPic": baseSmallImage + "people/Pink_Heart_With_Ribbon_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Pink_Nail_Polish_Emoji.png",
						"smallPic": baseSmallImage + "people/Pink_Nail_Polish_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Praying_Emoji.png",
						"smallPic": baseSmallImage + "people/Praying_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Raised_Fist_Emoji.png",
						"smallPic": baseSmallImage + "people/Raised_Fist_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Raised_Hand_Emoji.png",
						"smallPic": baseSmallImage + "people/Raised_Hand_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Revolving_Pink_Hearts_Emoji.png",
						"smallPic": baseSmallImage + "people/Revolving_Pink_Hearts_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Sign_of_the_Horns_Emoji.png",
						"smallPic": baseSmallImage + "people/Sign_of_the_Horns_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Sparkling_Pink_Heart_Emoji.png",
						"smallPic": baseSmallImage + "people/Sparkling_Pink_Heart_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Thumbs_Up_Hand_Sign_Emoji.png",
						"smallPic": baseSmallImage + "people/Thumbs_Up_Hand_Sign_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Tongue_Emoji.png",
						"smallPic": baseSmallImage + "people/Tongue_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Two_Pink_Hearts_Emoji.png",
						"smallPic": baseSmallImage + "people/Two_Pink_Hearts_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Unknown_Man_Emoji.png",
						"smallPic": baseSmallImage + "people/Unknown_Man_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Up_Pointing_Hand_Emoji.png",
						"smallPic": baseSmallImage + "people/Up_Pointing_Hand_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Victory_Hand_Emoji.png",
						"smallPic": baseSmallImage + "people/Victory_Hand_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Virus_Emoji.png",
						"smallPic": baseSmallImage + "people/Virus_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Waving_Hand_Sign_Emoji.png",
						"smallPic": baseSmallImage + "people/Waving_Hand_Sign_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/White_Baby_Angel_Emoji.png",
						"smallPic": baseSmallImage + "people/White_Baby_Angel_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/White_Baby_Emoji.png",
						"smallPic": baseSmallImage + "people/White_Baby_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/White_Grandma_Emoji.png",
						"smallPic": baseSmallImage + "people/White_Grandma_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/White_Grandpa_Emoji.png",
						"smallPic": baseSmallImage + "people/White_Grandpa_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/White_Open_Hands_Sign_Emoji.png",
						"smallPic": baseSmallImage + "people/White_Open_Hands_Sign_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/White_Santa_Claus_Emoji.png",
						"smallPic": baseSmallImage + "people/White_Santa_Claus_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/White_Thumbs_Down_Sign_Emoji.png",
						"smallPic": baseSmallImage + "people/White_Thumbs_Down_Sign_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/White_Up_Pointing_Backhand_Index_Emoji.png",
						"smallPic": baseSmallImage + "people/White_Up_Pointing_Backhand_Index_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Woman_Face_Massage_Emoji.png",
						"smallPic": baseSmallImage + "people/Woman_Face_Massage_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Woman_Frowning_Emoji.png",
						"smallPic": baseSmallImage + "people/Woman_Frowning_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Woman_Getting_Haircut_Emoji.png",
						"smallPic": baseSmallImage + "people/Woman_Getting_Haircut_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Woman_Hand_Gesture_Emoji.png",
						"smallPic": baseSmallImage + "people/Woman_Hand_Gesture_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Woman_Saying_Hello_Emoji.png",
						"smallPic": baseSmallImage + "people/Woman_Saying_Hello_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Woman_Saying_No_Emoji.png",
						"smallPic": baseSmallImage + "people/Woman_Saying_No_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Woman_Saying_Yes_Emoji.png",
						"smallPic": baseSmallImage + "people/Woman_Saying_Yes_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Woman_Wondering_Face_Emoji.png",
						"smallPic": baseSmallImage + "people/Woman_Wondering_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "people/Women_With_Bunny_Emoji.png",
						"smallPic": baseSmallImage + "people/Women_With_Bunny_Emoji.png"
					}
				],
				"mainPic": baseSmallImage + "people/Thumbs_Up_Hand_Sign_Emoji.png"
			}, {
				"name": "Foods",
				"list": [{
						"fullPic": baseImage + "food/Banana_Emoji.png",
						"smallPic": baseSmallImage + "food/Banana_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Beer_Emoji.png",
						"smallPic": baseSmallImage + "food/Beer_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Birthday_Cake_Emoji.png",
						"smallPic": baseSmallImage + "food/Birthday_Cake_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Cheese_Burger_Emoji.png",
						"smallPic": baseSmallImage + "food/Cheese_Burger_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Cherry_Emoji.png",
						"smallPic": baseSmallImage + "food/Cherry_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Chocolate_Bar_Emoji.png",
						"smallPic": baseSmallImage + "food/Chocolate_Bar_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Chocolate_Chip_Emoji.png",
						"smallPic": baseSmallImage + "food/Chocolate_Chip_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Cooking_Egg_Emoji.png",
						"smallPic": baseSmallImage + "food/Cooking_Egg_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Dango_Emoji.png",
						"smallPic": baseSmallImage + "food/Dango_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Donut_Emoji.png",
						"smallPic": baseSmallImage + "food/Donut_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Eggplant_Emoji.png",
						"smallPic": baseSmallImage + "food/Eggplant_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/French_Fries_Emoji.png",
						"smallPic": baseSmallImage + "food/French_Fries_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Grape_Emoji.png",
						"smallPic": baseSmallImage + "food/Grape_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Green_Apple_Emoji.png",
						"smallPic": baseSmallImage + "food/Green_Apple_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Honey_Pot_Emoji.png",
						"smallPic": baseSmallImage + "food/Honey_Pot_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Hot_Coffee_Emoji.png",
						"smallPic": baseSmallImage + "food/Hot_Coffee_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Ice_Cream_Emoji.png",
						"smallPic": baseSmallImage + "food/Ice_Cream_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Lemon_Emoji.png",
						"smallPic": baseSmallImage + "food/Lemon_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Lollipop_Candy_Emoji.png",
						"smallPic": baseSmallImage + "food/Lollipop_Candy_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Margarita_Cocktail_Emoji.png",
						"smallPic": baseSmallImage + "food/Margarita_Cocktail_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Meat_on_Bone_Emoji.png",
						"smallPic": baseSmallImage + "food/Meat_on_Bone_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Oden_Emoji.png",
						"smallPic": baseSmallImage + "food/Oden_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Orange_Emoji.png",
						"smallPic": baseSmallImage + "food/Orange_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Peach_Emoji.png",
						"smallPic": baseSmallImage + "food/Peach_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Poultry_Leg_Emoji.png",
						"smallPic": baseSmallImage + "food/Poultry_Leg_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Red_Apple_Emoji.png",
						"smallPic": baseSmallImage + "food/Red_Apple_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Red_Wine_Emoji.png",
						"smallPic": baseSmallImage + "food/Red_Wine_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Roasted_Sweet_Potato_Emoji.png",
						"smallPic": baseSmallImage + "food/Roasted_Sweet_Potato_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Sake_Bottle_and_Cup_Emoji.png",
						"smallPic": baseSmallImage + "food/Sake_Bottle_and_Cup_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Shaved_Ice_Emoji.png",
						"smallPic": baseSmallImage + "food/Shaved_Ice_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Slice_Of_Pizza_Emoji.png",
						"smallPic": baseSmallImage + "food/Slice_Of_Pizza_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Soup_Without_Handle_Emoji.png",
						"smallPic": baseSmallImage + "food/Soup_Without_Handle_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Strawberry_Cake_Emoji.png",
						"smallPic": baseSmallImage + "food/Strawberry_Cake_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Sushi_Emoji.png",
						"smallPic": baseSmallImage + "food/Sushi_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Taco_Emoji.png",
						"smallPic": baseSmallImage + "food/Taco_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Vanila_Ice_Cream_Emoji.png",
						"smallPic": baseSmallImage + "food/Vanila_Ice_Cream_Emoji.png"
					},
					{
						"fullPic": baseImage + "food/Watermelon_Emoji.png",
						"smallPic": baseSmallImage + "food/Watermelon_Emoji.png"
					}
				],
				"mainPic": baseSmallImage + "food/Banana_Emoji.png"
			}, {
				"name": "Animals",
				"list": [{
						"fullPic": baseImage + "animal/ant_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/ant_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Baby_Chick_Emoji.png",
						"smallPic": baseSmallImage + "animal/Baby_Chick_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Baby_chick_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/Baby_chick_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Bear_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/Bear_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Boar_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/Boar_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Camel_Emoji_PNG.png",
						"smallPic": baseSmallImage + "animal/Camel_Emoji_PNG.png"
					},
					{
						"fullPic": baseImage + "animal/Caterpie_Bug_Emoji.png",
						"smallPic": baseSmallImage + "animal/Caterpie_Bug_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/CAT_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/CAT_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/chicken_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/chicken_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Cow_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/Cow_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Dog_Emoji.png",
						"smallPic": baseSmallImage + "animal/Dog_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Dolphin_Emoji.png",
						"smallPic": baseSmallImage + "animal/Dolphin_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Elephant_Emoji.png",
						"smallPic": baseSmallImage + "animal/Elephant_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Emoji_Chestnut_PNG.png",
						"smallPic": baseSmallImage + "animal/Emoji_Chestnut_PNG.png"
					},
					{
						"fullPic": baseImage + "animal/Emoji_Dog_PNG.png",
						"smallPic": baseSmallImage + "animal/Emoji_Dog_PNG.png"
					},
					{
						"fullPic": baseImage + "animal/Emoji_Spiral_Shell_PNG.png",
						"smallPic": baseSmallImage + "animal/Emoji_Spiral_Shell_PNG.png"
					},
					{
						"fullPic": baseImage + "animal/Fish_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/Fish_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/FROG_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/FROG_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Grey_Bird_Emoji_PNG.png",
						"smallPic": baseSmallImage + "animal/Grey_Bird_Emoji_PNG.png"
					},
					{
						"fullPic": baseImage + "animal/Hatching_Chick_Emoji.png",
						"smallPic": baseSmallImage + "animal/Hatching_Chick_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Hear_No_Evil_Monkey_Emoji.png",
						"smallPic": baseSmallImage + "animal/Hear_No_Evil_Monkey_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/honeybee_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/honeybee_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Horse_emoji_icon_png (1).png",
						"smallPic": baseSmallImage + "animal/Horse_emoji_icon_png (1).png"
					},
					{
						"fullPic": baseImage + "animal/Horse_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/Horse_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/koala_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/koala_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Lady_beetle_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/Lady_beetle_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Monkey_Emoji.png",
						"smallPic": baseSmallImage + "animal/Monkey_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Monkey_Face_Emoji.png",
						"smallPic": baseSmallImage + "animal/Monkey_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Mushroom_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/Mushroom_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Octopus_Emoji.png",
						"smallPic": baseSmallImage + "animal/Octopus_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Panda_Face_Emoji.png",
						"smallPic": baseSmallImage + "animal/Panda_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Penguin_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/Penguin_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Pig_Emoji.png",
						"smallPic": baseSmallImage + "animal/Pig_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Pig_Nose_Emoji.png",
						"smallPic": baseSmallImage + "animal/Pig_Nose_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Poodle_Dog_Emoji.png",
						"smallPic": baseSmallImage + "animal/Poodle_Dog_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Rabbit_Face_Emoji.png",
						"smallPic": baseSmallImage + "animal/Rabbit_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/See_No_Evil_Monkey_Emoji.png",
						"smallPic": baseSmallImage + "animal/See_No_Evil_Monkey_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Sheep_Emoji_Icon.png",
						"smallPic": baseSmallImage + "animal/Sheep_Emoji_Icon.png"
					},
					{
						"fullPic": baseImage + "animal/Snake_Emoji.png",
						"smallPic": baseSmallImage + "animal/Snake_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Speak_No_Evil_Monkey_Emoji.png",
						"smallPic": baseSmallImage + "animal/Speak_No_Evil_Monkey_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Spouting_Whale_Emoji.png",
						"smallPic": baseSmallImage + "animal/Spouting_Whale_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/tiger_emoji_icon_png.png",
						"smallPic": baseSmallImage + "animal/tiger_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "animal/Tropical_Fish_Emoji.png",
						"smallPic": baseSmallImage + "animal/Tropical_Fish_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Turtle_Emoji.png",
						"smallPic": baseSmallImage + "animal/Turtle_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Unicorn_Emoji.png",
						"smallPic": baseSmallImage + "animal/Unicorn_Emoji.png"
					},
					{
						"fullPic": baseImage + "animal/Wold_Face_Emoji_Icon_in_PNG.png",
						"smallPic": baseSmallImage + "animal/Wold_Face_Emoji_Icon_in_PNG.png"
					}
				],
				"mainPic": baseSmallImage + "animal/Dog_Emoji.png"
			}, {
				"name": "Nature",
				"list": [{
						"fullPic": baseImage + "nature/Bouquet_Emoji_PNG.png",
						"smallPic": baseSmallImage + "nature/Bouquet_Emoji_PNG.png"
					},
					{
						"fullPic": baseImage + "nature/Cactus_Emoji.png",
						"smallPic": baseSmallImage + "nature/Cactus_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/Cherry_Blossom_Emoji.png",
						"smallPic": baseSmallImage + "nature/Cherry_Blossom_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/Cyclone_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/Cyclone_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Dark_Blue_Moon_Emoji.png",
						"smallPic": baseSmallImage + "nature/Dark_Blue_Moon_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/Dark_moon_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/Dark_moon_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Earth_Globe_Americas_Emoji.png",
						"smallPic": baseSmallImage + "nature/Earth_Globe_Americas_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Blossom_Yello.png",
						"smallPic": baseSmallImage + "nature/Emoji_Blossom_Yello.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Cloud_PNG.png",
						"smallPic": baseSmallImage + "nature/Emoji_Cloud_PNG.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Earth_Globe_Asia.png",
						"smallPic": baseSmallImage + "nature/Emoji_Earth_Globe_Asia.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Earth_Globe_Europe_Africa.png",
						"smallPic": baseSmallImage + "nature/Emoji_Earth_Globe_Europe_Africa.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Fallen_Leaf.png",
						"smallPic": baseSmallImage + "nature/Emoji_Fallen_Leaf.png"
					},
					{
						"fullPic": baseImage + "nature/EMOJI_FIRST_QUARTER_MOON_WITH_FACE.png",
						"smallPic": baseSmallImage + "nature/EMOJI_FIRST_QUARTER_MOON_WITH_FACE.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Foggy_PNG.png",
						"smallPic": baseSmallImage + "nature/Emoji_Foggy_PNG.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Herb_Image.png",
						"smallPic": baseSmallImage + "nature/Emoji_Herb_Image.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Leaf_Falling_PNG.png",
						"smallPic": baseSmallImage + "nature/Emoji_Leaf_Falling_PNG.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Maple_Leaf.png",
						"smallPic": baseSmallImage + "nature/Emoji_Maple_Leaf.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Milky_Way_PNG.png",
						"smallPic": baseSmallImage + "nature/Emoji_Milky_Way_PNG.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Of_Rice_Image.png",
						"smallPic": baseSmallImage + "nature/Emoji_Of_Rice_Image.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Snowflake_Download.png",
						"smallPic": baseSmallImage + "nature/Emoji_Snowflake_Download.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Sunflower_Free.png",
						"smallPic": baseSmallImage + "nature/Emoji_Sunflower_Free.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Sun_Behind_Cloud_Emoji.png",
						"smallPic": baseSmallImage + "nature/Emoji_Sun_Behind_Cloud_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/Emoji_Volcano_image.png",
						"smallPic": baseSmallImage + "nature/Emoji_Volcano_image.png"
					},
					{
						"fullPic": baseImage + "nature/Fire_Emoji.png",
						"smallPic": baseSmallImage + "nature/Fire_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/First_quarter_moon_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/First_quarter_moon_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Full_moon_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/Full_moon_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Globe_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/Globe_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Hibiscus_Emoji_Flower_PNG.png",
						"smallPic": baseSmallImage + "nature/Hibiscus_Emoji_Flower_PNG.png"
					},
					{
						"fullPic": baseImage + "nature/Last_quarter_moon_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/Last_quarter_moon_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Moon_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/Moon_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Palm_Tree_Emoji.png",
						"smallPic": baseSmallImage + "nature/Palm_Tree_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/Rainbow_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/Rainbow_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Rose_Emoji_Download_PNG.png",
						"smallPic": baseSmallImage + "nature/Rose_Emoji_Download_PNG.png"
					},
					{
						"fullPic": baseImage + "nature/Seedling_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/Seedling_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Snowman_Emoji.png",
						"smallPic": baseSmallImage + "nature/Snowman_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/Star_Emoji.png",
						"smallPic": baseSmallImage + "nature/Star_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/The_Sun_Emoji.png",
						"smallPic": baseSmallImage + "nature/The_Sun_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/The_Sun_Face_Emoji.png",
						"smallPic": baseSmallImage + "nature/The_Sun_Face_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/Umbrella_Rain_Drops_Emoji.png",
						"smallPic": baseSmallImage + "nature/Umbrella_Rain_Drops_Emoji.png"
					},
					{
						"fullPic": baseImage + "nature/voltage_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/voltage_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Waning_crescent_moon_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/Waning_crescent_moon_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Water_wave_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/Water_wave_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Waxing_crescent_moon_emoji_icon_png.png",
						"smallPic": baseSmallImage + "nature/Waxing_crescent_moon_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "nature/Yellow_Moon_Emoji.png",
						"smallPic": baseSmallImage + "nature/Yellow_Moon_Emoji.png"
					}
				],
				"mainPic": baseSmallImage + "nature/Bouquet_Emoji_PNG.png"
			}, {
				"name": "Things",
				"list": [{
						"fullPic": baseImage + "thing/100_Emoji.png",
						"smallPic": baseSmallImage + "thing/100_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Airplane_Emoji.png",
						"smallPic": baseSmallImage + "thing/Airplane_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Athletic_Shoe_Emoji.png",
						"smallPic": baseSmallImage + "thing/Athletic_Shoe_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Bicycle_Emoji.png",
						"smallPic": baseSmallImage + "thing/Bicycle_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Bikini_Emoji.png",
						"smallPic": baseSmallImage + "thing/Bikini_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Briefcase_Emoji.png",
						"smallPic": baseSmallImage + "thing/Briefcase_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Business_Shirt_With_Tie_Emoji.png",
						"smallPic": baseSmallImage + "thing/Business_Shirt_With_Tie_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Coin_Purse_Emoji.png",
						"smallPic": baseSmallImage + "thing/Coin_Purse_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Crystal_Magic_Ball.png",
						"smallPic": baseSmallImage + "thing/Crystal_Magic_Ball.png"
					},
					{
						"fullPic": baseImage + "thing/Delivery_Truck_Emoji.png",
						"smallPic": baseSmallImage + "thing/Delivery_Truck_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Diamond_Emoji.png",
						"smallPic": baseSmallImage + "thing/Diamond_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Diamond_Ring_Emoji.png",
						"smallPic": baseSmallImage + "thing/Diamond_Ring_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Footprints_Emoji.png",
						"smallPic": baseSmallImage + "thing/Footprints_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Gift_Present_Emoji.png",
						"smallPic": baseSmallImage + "thing/Gift_Present_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Glasses_Emoji.png",
						"smallPic": baseSmallImage + "thing/Glasses_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Graduation_Cap_Emoji.png",
						"smallPic": baseSmallImage + "thing/Graduation_Cap_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Green_Dress_Emoji.png",
						"smallPic": baseSmallImage + "thing/Green_Dress_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Handbag_Emoji.png",
						"smallPic": baseSmallImage + "thing/Handbag_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/HIgh_Heel_Shoe_Emoji.png",
						"smallPic": baseSmallImage + "thing/HIgh_Heel_Shoe_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/House_Emoji.png",
						"smallPic": baseSmallImage + "thing/House_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/House_Emoji_With_Tree.png",
						"smallPic": baseSmallImage + "thing/House_Emoji_With_Tree.png"
					},
					{
						"fullPic": baseImage + "thing/Jeans_Emoji.png",
						"smallPic": baseSmallImage + "thing/Jeans_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Kimono_Emoji.png",
						"smallPic": baseSmallImage + "thing/Kimono_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Kiss_Mark_Emoji.png",
						"smallPic": baseSmallImage + "thing/Kiss_Mark_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Lipstick_Emoji.png",
						"smallPic": baseSmallImage + "thing/Lipstick_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Lips_Emoji.png",
						"smallPic": baseSmallImage + "thing/Lips_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Love_Letter_Emoji.png",
						"smallPic": baseSmallImage + "thing/Love_Letter_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Mans_Shoe_Emoji.png",
						"smallPic": baseSmallImage + "thing/Mans_Shoe_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Money_Bag_Emoji.png",
						"smallPic": baseSmallImage + "thing/Money_Bag_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Money_with_Wings_Emoji.png",
						"smallPic": baseSmallImage + "thing/Money_with_Wings_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Musical_Keyboard_Emoji.png",
						"smallPic": baseSmallImage + "thing/Musical_Keyboard_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Musical_Notes_Emoji.png",
						"smallPic": baseSmallImage + "thing/Musical_Notes_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Notebook_Emoji.png",
						"smallPic": baseSmallImage + "thing/Notebook_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Open_Book_Emoji.png",
						"smallPic": baseSmallImage + "thing/Open_Book_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Package_Box_Emoji.png",
						"smallPic": baseSmallImage + "thing/Package_Box_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Paw_emoji_icon_png.png",
						"smallPic": baseSmallImage + "thing/Paw_emoji_icon_png.png"
					},
					{
						"fullPic": baseImage + "thing/Pencil_Emoji.png",
						"smallPic": baseSmallImage + "thing/Pencil_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Picnic_Hat_Emoji.png",
						"smallPic": baseSmallImage + "thing/Picnic_Hat_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Pouch_Emoji.png",
						"smallPic": baseSmallImage + "thing/Pouch_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Queen_s_Crown_Emoji.png",
						"smallPic": baseSmallImage + "thing/Queen_s_Crown_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Red_Flag_Emoji.png",
						"smallPic": baseSmallImage + "thing/Red_Flag_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Red_Pin_Emoji.png",
						"smallPic": baseSmallImage + "thing/Red_Pin_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Rocket_Emoji.png",
						"smallPic": baseSmallImage + "thing/Rocket_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Running_Shirt_With_Sash_Emoji.png",
						"smallPic": baseSmallImage + "thing/Running_Shirt_With_Sash_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Sail_Boat_Emoji.png",
						"smallPic": baseSmallImage + "thing/Sail_Boat_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Speech_Bubble_Emoji.png",
						"smallPic": baseSmallImage + "thing/Speech_Bubble_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Sweat_Water_Emoji.png",
						"smallPic": baseSmallImage + "thing/Sweat_Water_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/T-Shirt_Emoji.png",
						"smallPic": baseSmallImage + "thing/T-Shirt_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Thought_Speech_Bubble_Emoji.png",
						"smallPic": baseSmallImage + "thing/Thought_Speech_Bubble_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Top_Magic_Hat_Emoji.png",
						"smallPic": baseSmallImage + "thing/Top_Magic_Hat_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Umbrella_Emoji.png",
						"smallPic": baseSmallImage + "thing/Umbrella_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Womans_Boots_Emoji.png",
						"smallPic": baseSmallImage + "thing/Womans_Boots_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Womans_Clothes_Emoji.png",
						"smallPic": baseSmallImage + "thing/Womans_Clothes_Emoji.png"
					},
					{
						"fullPic": baseImage + "thing/Womans_Sandal_Emoji.png",
						"smallPic": baseSmallImage + "thing/Womans_Sandal_Emoji.png"
					}
				],
				"mainPic": baseSmallImage + "thing/Gift_Present_Emoji.png"
			}]
		});

	});

}

new StickerListHandler().attach(router);
module["exports"] = router;