var express = require('express');
var router = express.Router();
var async = require('async');
var formidable = require('formidable');
var fs = require('fs-extra');
var path = require('path');
var mime = require('mime');
var bodyParser = require("body-parser");
var path = require('path');
var _ = require('lodash');

var RequestHandlerBase = require("./RequestHandlerBase");
var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');
var UserModel = require("../Models/UserModel");
var PushNotificationModel = require("../Models/PushNotificationModel");
var Settings = require("../lib/Settings");
var Const = require("../const");

var SendChatRequestHandler = function(){
    
}

_.extend(SendChatRequestHandler.prototype,RequestHandlerBase.prototype);

SendChatRequestHandler.prototype.attach = function(router){
        
    var self = this;

    router.post('/',function(request,response){
                
        var userId = request.body.u.toLowerCase();
        var password = request.body.p;
        var accounts = request.body.a.toLowerCase().split(',');
        var roomId = request.body.r;
        var message = request.body.m;

        // Save to db
        UserModel.findUsersbyId(accounts, function(err, users) {
            if (err) {
                console.log(err);
                self.successResponse(response, Const.responsecodeParamError);
                return;
            }

            async.series([
                function(seriesCallback) {
                    async.each(accounts, function(userId, callback) {
                        var user = _.find(users, function(u) { return u.userID == userId; });
                        if (userId && !user) {
                            var newUser = new DatabaseManager.userModel({
                                userID: userId,
                                name: userId,
                                created: Utils.now()
                            });

                            newUser.save(function(newErr, u){
                            
                                if(newErr){
                                    console.log(newErr);
                                }

                                var newMessage = new DatabaseManager.messageModel({
                                    user: u._id,
                                    userID: u.userID,
                                    roomID: roomId,
                                    message: '',
                                    type: Const.messageUserAdded,
                                    created: Utils.now()                   
                                });
                                            
                                newMessage.save(function(msgErr,message){
                                
                                    if(msgErr) {
                                        console.log(msgErr);
                                    }

                                    callback(msgErr);
                                });

                            });
                        }
                        else if (user) {
                            var newMessage = new DatabaseManager.messageModel({
                                user:user._id,
                                userID: user.userID,
                                roomID: roomId,
                                message: '',
                                type: Const.messageUserAdded,
                                created: Utils.now()                   
                            });
                                        
                            newMessage.save(function(msgErr,message){
                            
                                if(msgErr) {
                                    console.log(msgErr);
                                }

                                callback(msgErr);
                            });
                        }
                        else {
                            callback(null);
                        }
                    }, function(err) {
                        if (err) {
                            console.log(err);
                        }

                        seriesCallback(err, null);
                    });
                },
                function(seriesCallback) {
                    async.each(users, function(user, callback) {
                        if (user.userID == userId) {
                            callback(null);
                            return;
                        }

                        var newNotification = new DatabaseManager.pushNotificationModel({
                            token: user.token_type == 0 ? user.gcm_token : user.apns_token,
                            type: user.token_type,
                            message: message,
                            data: roomId,
                            dispatched: 0,
                            created: Utils.now()
                        });

                        newNotification.save(function(err,notification){
                        
                            if(err){
                                console.log(err);
                            }

                            notification.send(function(notiErr, noti) {
                                if (notiErr) {
                                    console.log(notiErr);
                                }

                                callback(notiErr);
                            });

                        });

                    }, function(err) {
                        if (err) {
                            console.log(err);
                        }

                        seriesCallback(err, null);
                    });
                }
            ], function(err, result) {
                self.successResponse(response,Const.responsecodeSucceed,{
                });
            })
        });
        
    });

}

new SendChatRequestHandler().attach(router);
module["exports"] = router;
