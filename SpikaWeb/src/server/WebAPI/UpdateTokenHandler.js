var express = require('express');
var router = express.Router();
var async = require('async');
var formidable = require('formidable');
var fs = require('fs-extra');
var path = require('path');
var mime = require('mime');
var bodyParser = require("body-parser");
var path = require('path');
var _ = require('lodash');

var RequestHandlerBase = require("./RequestHandlerBase");
var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');
var UserModel = require("../Models/UserModel");
var Settings = require("../lib/Settings");
var Const = require("../const");

var UpdateTokenHandler = function(){
    
}

_.extend(UpdateTokenHandler.prototype,RequestHandlerBase.prototype);

UpdateTokenHandler.prototype.attach = function(router){
        
    var self = this;

    router.post('/',function(request,response){
                
        var userId = request.body.u.toLowerCase();
        var password = request.body.p;
        var token = request.body.t;
        var token_type = request.body.tt;

        UserModel.findUserbyId(userId, function(err, user) {
            if (err) {
                self.errorResponse(
                    response,
                    Const.httpCodeSucceed,
                    Const.responsecodeParamError,
                    Utils.localizeString("Update Failed"),
                    false
                );
                return;
            }

            if(user == null){
            
                // save to database
                var newUser = new DatabaseManager.userModel({
                    userID: userId,
                    name: userId,
                    token_type: token_type,
                    gcm_token: token_type == 0 ? token : "",
                    apns_token: token_type == 1 ? token : "",
                    created: Utils.now()
                });

                newUser.save(function(newErr,user){
                
                    if(newErr){
                        console.log(newErr);
                        self.errorResponse(
                            response,
                            Const.httpCodeSucceed,
                            Const.responsecodeParamError,
                            Utils.localizeString("Update Failed"),
                            false
                        );
                    }else{
                        self.successResponse(response,Const.responsecodeSucceed,{
                        });
                    }      
            
                });

            } else {
                user.update({
                    token_type: token_type,
                    gcm_token: token_type == 0 ? token : "",
                    apns_token: token_type == 1 ? token : ""
                },{},function(updateErr,userResult){
                
                    if(updateErr){
                        console.log(updateErr);
                        self.errorResponse(
                            response,
                            Const.httpCodeSucceed,
                            Const.responsecodeParamError,
                            Utils.localizeString("Update Failed"),
                            false
                        );
                    }else{
                        self.successResponse(response,Const.responsecodeSucceed,{
                        });
                    }                
            
                });
            }
        });
        
    });

}

new UpdateTokenHandler().attach(router);
module["exports"] = router;
