var mongoose = require('mongoose');
var _ = require('lodash');
var Const = require('../const.js');
var async = require('async');
var Util = require('../lib/Utils');
var Settings = require("../lib/Settings");
var apn = require('apn');
var FCM = require('fcm-push');
var fcm = new FCM('AAAAUk25E44:APA91bEfin_UfdLoXR8FCIBZWaBSKLN4g8cKBSSzusB0F8OzTk4qJi57rKH9og2-f-deLOLIRCJc2RE2ZiFGGLJhB-m1_PgCGmS1IhqshGuKOoVu42Wyis33JjG08hWXDbIOBg3bMvSSNAgbCSewhm2iyB7KVLrqgw');

var options = {
//   token: {
//     key: '/home/ubuntu/Spika/APNsAuthKey_26M8F66PH7.p8',
//     keyId: "26M8F66PH7",
//     teamId: "7CQNUBJ79R"
//   },
   cert: '/home/ubuntu/Spika/voip_cert.pem',
   key: '/home/ubuntu/Spika/voip_key.pem',
  production: true
};

var apnProvider = new apn.Provider(options);

var PushNotificationModel = function(){
    
};

PushNotificationModel.prototype.model = null;

PushNotificationModel.prototype.init = function(){

    // Defining a schema
    var pushNotificationSchema = new mongoose.Schema({
        token: String,
        type: Number,
        message: String,
        data: String,
        dispatched: Number,
        created: Number
    });
 
    // add instance methods
    pushNotificationSchema.methods.dispatch = function (callBack) {
        
        var self = this;

        this.update({
            dispatched: 1
        },{},function(err,result){
        
            if(callBack)
                callBack(err,self);              
        
        });

    }

    pushNotificationSchema.methods.send = function (callBack) {

        if (this.dispatched == 1) {
            callBack(null, this);
            return;
        }
        
        var self = this;

        if (this.type == 0) {
            // GCM
            var message = {
                to: this.token,
                data: {
                    type: 'chat',
                    room_id: this.data
                }
            };
            fcm.send(message, function(err, response){
                if (err) {
                    console.log(err);
                    if(callBack)
                        callBack(err,self);
                    return;
                }

                self.update({
                    dispatched: 1
                },{},function(updateErr,result){
                    if (updateErr) {
                        console.log(updateErr);
                    }
                    
                    if(callBack)
                        callBack(updateErr,self);
                });
            });
        }
        else if (this.type == 1) {
            // Apple
            var note = new apn.Notification();
            note.payload = {'type': 'chat', 'room_id': this.data};
            apnProvider.send(note, this.token).then(function(result) {
                console.log(result);
                self.update({
                    dispatched: 1
                },{},function(err,result){
                    if (err) {
                        console.log(err);
                    }
                    
                    if(callBack)
                        callBack(err,self);    
                });
            });
        }
        else {
            self.update({
                    dispatched: 1
                },{},function(err,result){
                    if (err) {
                        console.log(err);
                    }
                    
                    if(callBack)
                        callBack({ error: 'Invalid type'}, self);
                });
        }
    }

    this.model = mongoose.model(Settings.options.dbCollectionPrefix + "push_notifications", pushNotificationSchema);
    return this.model;
		        
}

PushNotificationModel.prototype.findNotDispatched = function(callBack){

    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);

    this.model.find({ dispatched: 0, created: { $gt: yesterday.getTime() } }).sort({'created': 'asc'}).exec(function (err, data) {

        if (err) 
            console.error(err);
        
        if(callBack)
            callBack(err,data);
        
    });
            
}

module["exports"] = new PushNotificationModel();