var _ = require('lodash');

var UsersManager = require("../lib/CanvasUsersManager");
var Utils = require("../lib/Utils");
var SocketHandlerBase = require("./SocketHandlerBase");

var JoinCanvasActionHandler = function(){
    
}

_.extend(JoinCanvasActionHandler.prototype,SocketHandlerBase.prototype);

JoinCanvasActionHandler.prototype.attach = function(io,socket){
        
    var self = this;

    socket.on('join', function(param){
                    
        if(Utils.isEmpty(param.roomID)){  
            socket.emit('socket error', {code:Const.resCodeSocketLoginNoRoomID});               
            return;
        }

        if(!Utils.isEmpty(param.password)){                 
        }
        
        socket.join(param.roomID);
        UsersManager.addUser(param.roomID,socket.id);
        io.of("/canvas").in(param.roomID).emit('new user', {
            socketID: socket.id
        });

        var snapshot = UsersManager.getSnapshot(param.roomID);
        socket.emit('joined', {
            snapshot: snapshot
        })
    });

}


module["exports"] = new JoinCanvasActionHandler();