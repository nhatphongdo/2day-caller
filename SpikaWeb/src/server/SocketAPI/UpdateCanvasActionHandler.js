var _ = require('lodash');

var UsersManager = require("../lib/CanvasUsersManager");
var Utils = require("../lib/Utils");
var SocketHandlerBase = require("./SocketHandlerBase");

var UpdateCanvasActionHandler = function(){
    
}

_.extend(UpdateCanvasActionHandler.prototype,SocketHandlerBase.prototype);

UpdateCanvasActionHandler.prototype.attach = function(io,socket){
        
    var self = this;

    socket.on('update canvas', function(param){
                    
        if(Utils.isEmpty(param.roomID)){  
            socket.emit('socket error', {code:Const.resCodeSocketLoginNoRoomID});               
            return;
        }

        if(Utils.isEmpty(param.snapshot)){
            socket.emit('socket error', {message:"Missing Snapshot"});               
            return;
        }
        
        UsersManager.updateSnapshot(param.roomID, param.snapshot);
        io.of("/canvas").in(param.roomID).emit('new update', {
            snapshot: param.snapshot
        });
       
    });

}


module["exports"] = new UpdateCanvasActionHandler();